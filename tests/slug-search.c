#include "../src/lb-utils.h"

#define assert_true(test) assert_impl ((test), #test)
#define assert_false(test) assert_impl (!(test), #test)

#define assert_strequal(expected, test) \
	assert_strequal_impl((expected), (test), #test)

static int
assert_impl (gboolean     success,
	     const gchar *test)
{
	g_print ("%-74s %s\n", test, success ? "GOOD" : "FAIL");
	return (success ? 0 : 1);
}

static int
assert_strequal_impl (const gchar *expected,
		      gchar       *result,
		      const gchar *test)
{
	gboolean success;
	int failures;

	success = (expected ? result && g_str_equal (expected, result) : !result);
	failures = assert_impl (success, test);

	if (failures) {
		g_print (" - expected: %s\n", expected);
		g_print (" - found:    %s\n", result);
	}

	g_free (result);
	return failures;
}

int
main (int argc, char **argv)
{
	int failures = 0;

	failures += assert_true (lb_string_has_substring ("foobar", "foo"));
	failures += assert_true (lb_string_has_substring ("foobar", ":foo"));
	failures += assert_true (lb_string_has_substring ("foobar", "f-oo"));
	failures += assert_true (lb_string_has_substring ("foobar", "foo!"));

	failures += assert_true (lb_string_has_substring ("f:oobar", "ooba"));
	failures += assert_true (lb_string_has_substring ("fo:o::bar", "ooba"));
	failures += assert_true (lb_string_has_substring ("fooba:r", "ooba"));

	failures += assert_true (lb_string_has_substring ("FooBar!", "bar"));
	failures += assert_true (lb_string_has_substring ("foobar", "OO"));

	failures += assert_true (lb_string_has_substring ("f**fo*foobar", "foo"));
	failures += assert_true (lb_string_has_substring ("ffofoobar", "foo"));

	failures += assert_false (lb_string_has_substring ("foobar", "nix"));
	failures += assert_false (lb_string_has_substring ("foobar", ":nix:"));
	failures += assert_false (lb_string_has_substring ("foobar", "obx"));

	failures += assert_false (lb_string_has_substring ("foobar", ""));
	failures += assert_false (lb_string_has_substring ("foobar", "/"));
	failures += assert_false (lb_string_has_substring ("foobar", "/x"));

	failures += assert_true (lb_string_has_substring ("Berliner Wei\303\237e",
							  "wei\303\237"));
	failures += assert_false (lb_string_has_substring ("Berliner Wei\303\237e",
							   "mit Schu\303\237"));

	failures += assert_strequal ("<u>foo</u>bar",
			             lb_string_markup_substring
				    ("foobar", "foo", "u"));
	failures += assert_strequal ("foo &amp; <i>bar</i> is foobar",
			             lb_string_markup_substring
				    ("foo & bar is foobar", "bar", "i"));
	failures += assert_strequal ("f<b>o&lt;o&gt;b</b>ar",
			             lb_string_markup_substring
				    ("fo<o>bar", "oob", "b"));

	failures += assert_strequal ("<b>Evo</b>lution E-Mail",
			             lb_string_markup_substring
				    ("Evolution E-Mail", "evo", "b"));
	failures += assert_strequal ("<b>Evo</b>lution E-Mail",
			             lb_string_markup_substring
				    ("Evolution E-Mail", "e-vo", "b"));
	failures += assert_strequal ("Evolution <b>E-Mai</b>l",
			             lb_string_markup_substring
				    ("Evolution E-Mail", "emai", "b"));
	failures += assert_strequal ("Evolution <b>E-Mai</b>l",
			             lb_string_markup_substring
				    ("Evolution E-Mail", "e-mai", "b"));

	return failures;
}
