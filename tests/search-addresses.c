/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>

#include <bonobo/bonobo-main.h>
#include <libebook/e-book.h>

static EBookQuery *
create_query (const gchar *query_str)
{
	EBookQuery *query;
	EBookQuery *queries[10];

	queries[0] = e_book_query_field_test (E_CONTACT_GIVEN_NAME,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[1] = e_book_query_field_test (E_CONTACT_FAMILY_NAME,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[2] = e_book_query_field_test (E_CONTACT_NICKNAME,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[3] = e_book_query_field_test (E_CONTACT_EMAIL,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);

	/* Should we do the query here or leave that to an IM aware 
	 * application
	 */
	queries[4] = e_book_query_field_test (E_CONTACT_IM_AIM,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[5] = e_book_query_field_test (E_CONTACT_IM_GROUPWISE,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[6] = e_book_query_field_test (E_CONTACT_IM_JABBER,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[7] = e_book_query_field_test (E_CONTACT_IM_YAHOO,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[8] = e_book_query_field_test (E_CONTACT_IM_MSN,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[9] = e_book_query_field_test (E_CONTACT_IM_ICQ,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);

	query = e_book_query_or (10, queries, TRUE);
	
	return query;
}

static void
print_results (EBook *book, const gchar *query_str)
{
	GList      *contacts, *l;
	gboolean    result;
	EBookQuery *query;

	query = create_query (query_str);

	g_print ("\n");

	result = e_book_get_contacts (book, query, &contacts, NULL);
	e_book_query_unref (query);

	if (!result) {
		g_print ("Error fetching list\n");
		return;
	}

	for (l = contacts; l; l = l->next) {
		EContact    *contact = E_CONTACT (l->data);
		const gchar *name;
		GList       *emails, *e;

		name = e_contact_get_const (contact, E_CONTACT_FULL_NAME);
		g_print ("%s\n---------------------\n", name);

		emails = e_contact_get (contact, E_CONTACT_EMAIL);
		for (e = emails; e; e = e->next) {
			printf ("%s\n", (gchar *) e->data);
		}
		g_list_foreach (emails, (GFunc)g_free, NULL);
		g_list_free (emails);

		g_print ("\n");

		g_object_unref (contact);
	}

	g_list_free (contacts);
}

int 
main (int argc, char **argv)
{
	EBook      *book;

	if (argc < 2) {
		g_print ("USAGE: %s query\n", argv[0]);
		exit (0);
	}

	if (bonobo_init (&argc, argv) == FALSE) {
		g_error ("Could not initialize Bonobo");
	}
	
	book = e_book_new_system_addressbook (NULL);
	
	if (!e_book_open (book, TRUE, NULL)) {
		g_warning ("Failed to open addressbook");
		return -1;
	}

	print_results (book, argv[1]);

	g_object_unref (book);

	return 0;
}
