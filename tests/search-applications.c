/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>

#define GMENU_I_KNOW_THIS_IS_UNSTABLE
#include <gnome-menus/gmenu-tree.h>

static void
applications_add_directory (GSList **applications, GMenuTreeDirectory *dir)
{
	GSList *items;
	GSList *l;
	
	const char *name = gmenu_tree_directory_get_name (dir);
	g_warning (name);

	items = gmenu_tree_directory_get_contents (dir);

	for (l = items; l; l = l->next) {
		GMenuTreeDirectory *subitem;

		subitem = (GMenuTreeDirectory *) l->data;
		if (gmenu_tree_item_get_type (subitem) == GMENU_TREE_ITEM_DIRECTORY)
		{
			applications_add_directory (applications, subitem);
			gmenu_tree_item_unref (subitem);
		
		} else if(gmenu_tree_item_get_type (subitem) == GMENU_TREE_ITEM_ENTRY)
		   *applications = g_slist_append (*applications, subitem);
		
		}

	g_slist_free (items);
}

static GSList *
applications_get_list (void)
{
	GMenuTree          *tree;
	GMenuTreeDirectory *root;
	GSList            *applications;

	tree = gmenu_tree_lookup ("applications.menu", GMENU_TREE_FLAGS_NONE);
	if (!tree) {
		g_warning ("Failed to look up applications.menu");
		return NULL;
	}

	root = gmenu_tree_get_root_directory (tree);
	if (!root) {
		g_warning ("Menu tree empty");
		gmenu_tree_unref (tree);
		return NULL;
	}

	applications = NULL;

	applications_add_directory (&applications, root);

	gmenu_tree_item_unref (root);
	gmenu_tree_unref (tree);

	return applications;
}

int
main (int argc, char **argv)
{
	GSList *applications;
	GSList *l;

	applications = applications_get_list ();

	for (l = applications; l; l = l->next) {
		GMenuTreeEntry *entry;

		entry = (GMenuTreeEntry *) l->data;
		
		g_print ("%s - %s\n",
			 gmenu_tree_entry_get_name (entry),
			 gmenu_tree_entry_get_icon (entry));
		gmenu_tree_item_unref (entry);
	}

	g_slist_free (applications);

	return 0;
}
