/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>

static void
print_actions_for_file (GFileInfo *info)
{
	GList *applications, *list;
	
	applications = g_app_info_get_all_for_type (g_file_info_get_content_type (info));

	for (list = applications; list != NULL; list = list->next) {
		GAppInfo *application = list->data;
		const gchar *name = g_app_info_get_name (application);
		gchar *icon = g_icon_to_string (g_app_info_get_icon (application));
		
		g_print ("found: %s %s\n", name, icon);
	}
}

gint
main (gint argc, gchar **argv)
{
	GFileInfo* info;
	GError   * error = NULL;
	GFile    * file;

	gtk_init (&argc, &argv);

	if (argc < 2) {
		printf ("%s <absolute path filename>\n", argv[0]);
		return -1;
	}

	file = g_file_new_for_commandline_arg (argv[1]);
	info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
			          0, NULL,
				  &error);
	if (error) {
		g_printerr ("Could not open file: %s\n",
			    error->message);
		g_error_free (error);
		
		return -1;
	}

	print_actions_for_file (info);

	return 0;
}
