2009-03-09  Sven Herzberg  <herzi@lanedo.com>

	Bug 566311 – Conflicting default shortcut

	Patch by Bastien Nocera

	* data/gnome-launch-box.schemas.in: change the default key from
	Alt-Space (which window managers use for the window icon's menu) to
	AltGr-Space

2009-03-09  Sven Herzberg  <herzi@lanedo.com>

	Bug 574006 – gnome-launch-box state is retained across calls to it

	* src/lb-window.c (lb_window_present): finally fix this by also
	cleaning the last search string

2009-03-09  Sven Herzberg  <herzi@lanedo.com>

	Bug 566313 – Automatically use "-t" when compositing is on

	Patch from Bastien Nocera

	* src/lb-main.c (update_composited_window), (show_window),
	(composited_changed_cb), (main): Automatically use transparency if
	there's a compositor; Update the window type properly when the
	compositing changes

2009-03-09  Sven Herzberg  <herzi@lanedo.com>

	Bug 574006 – gnome-launch-box state is retained across calls to it

	* src/lb-window.c (lb_window_present): also reset the focus

2009-03-09  Sven Herzberg  <herzi@lanedo.com>

	Bug 566316 – Makes open in default application first action for file
	items
	GLB-22

	Patch written by David Siegel and fixed by Pietro Battiston

	* src/lb-module-files.c (module_files_add_actions): properly prefer
	the default application over others

2009-03-09  Sven Herzberg  <herzi@lanedo.com>

	Bug 574006 – gnome-launch-box state is retained across calls to it

	* src/lb-window.c (lb_window_present): reset the result icon and label
	properly

2008-01-09  Sven Herzberg  <sven@imendio.com>

	Fix GLB-14 (Patch from Matthias Hasselmann)

	* src/lb-utils.c,
	* tests/Makefile.am,
	* tests/slug-search.c: fix highlighting of slug matches

2008-01-09  Sven Herzberg  <sven@imendio.com>

	Start GLB-14 (Patch from Matthias Hasselmann)

	* src/lb-module-evolution.c,
	* src/lb-module-manager.c,
	* src/lb-module.c,
	* src/lb-module.h,
	* src/lb-utils.c,
	* src/lb-utils.h: implemented slug search

2008-01-09  Sven Herzberg  <sven@imendio.com>

	Fix GLB-28 (Patch from Marc-Andre Lureau)

	* src/lb-window.c: indicate a zero-match

2008-01-09  Sven Herzberg  <sven@imendio.com>

	Fix GLB-37 (Patch from Bastien Nocera)

	* configure.ac,
	* src/lb-module-files.c: use xdg-user-dirs

2007-06-10  Sven Herzberg  <sven@imendio.com>

	* data/90-gnome-launch-box.xml.in: set some group

2007-06-05  Sven Herzberg  <herzi@imendio.com>

	Patch by Johan Hilding

	* src/lb-main.c: initialize GTK+ before doing IPC (fixes GLB-10)

2007-06-05  Sven Herzberg  <herzi@imendio.com>

	Patch by Bastien Nocera

	* configure.ac: check whether the control center supports the new
	keybinding stuff (GNOME 2.20)
	* data/Makefile.am: install custom keybinding data if supported
	* data/90-gnome-launch-box.xml.in: added custom keybinding data
	* data/gnome-launch-box.schemas.in: Improved documentation

2007-06-05  Sven Herzberg  <herzi@imendio.com>

	* autogen.sh: updated to latest gnome-common (Patch by Bastien Nocera)

2007-03-07  Sven Herzberg  <herzi@imendio.com>

	* NEWS: added closed bugs

2007-03-07  Sven Herzberg  <herzi@imendio.com>

	* NEWS:
	* configure.ac: renamed to 0.2

2007-03-07  Sven Herzberg  <herzi@imendio.com>

	* NEWS,
	* configure.ac: prepared for the release of 0.1.95
	* Makefile.am,
	* data/Makefile.am: fixed distcheck

2007-03-07  Sven Herzberg  <herzi@imendio.com>

	* src/lb-utils.c: made the code a bit more robust against non-UTF8
	file names

2007-02-15  Sven Herzberg  <herzi@gnome-de.org>

	* configure.ac: updated copyright and language list

2007-01-11  Richard Hult  <richard@imendio.com>

	* src/lb-module-bookmarks.c (lb_module_bookmarks_add_actions):
	Remove check for the scheme, if a bookmark is in the list, the
	browser should be able to handle it. Patch from Kai Willadsen
	<kaiw@itee.uq.edu.au>.

2007-01-04  Richard Hult  <richard@imendio.com>

	* configure.ac:
	* src/Makefile.am:
	* src/lb-module-recent.c: (lb_module_recent_init),
	(lb_module_recent_finalize), (lb_module_recent_query):
	* src/lb-module-recent.h:
	* src/lb-recent-item.c:
	* src/lb-recent-item.h:
	* src/lb-recent-list.c:
	* src/lb-recent-list.h:
	* src/lb-window.c: (lb_window_new): Patch from Bastien Nocera to
	use GtkRecent.

	* src/lb-module-bookmarks.c: (lb_module_bookmarks_query): Don't
	unref null pixbuf.

2007-01-04  Richard Hult  <richard@imendio.com>

	* src/lb-bookmark-list.c: (html_startElement),
	(lb_bookmark_list_get_firefox):
	* src/lb-bookmark-list.h:
	* src/lb-module-bookmarks.c: (lb_module_bookmarks_init),
	(lb_module_bookmarks_finalize), (lb_module_bookmarks_get_property),
	(lb_module_bookmarks_file_changed), (lb_module_bookmarks_query),
	(lb_module_bookmarks_add_actions), (lb_module_bookmarks_activate):
	Patch from Bastien Nocera to add support for Epiphany bookmarks.
	
2006-11-06  Richard Hult  <richard@imendio.com>

	* autogen.sh:
	* configure.ac: 
	* data/gnome-launch-box.schemas.in:
	* src/Makefile.am:
	* src/eggaccelerators.c:
	* src/eggaccelerators.h:
	* src/lb-main.c: (message_received_cb), (key_handler_bind), (main):
	* src/lb-module-evolution.c: (module_evolution_query):
	* src/tomboykeybinder.c:
	* src/tomboykeybinder.h: Commit some hoarded code...

2006-08-23  Sven Herzberg  <herzi@gnome-de.org>

	* configure.ac: don't check for CXX and F77
	* src/lb-module-files.c: support desktop_is_home_dir
	* src/lb-module-recent.c: XML-encode the filename

2006-08-17  Sven Herzberg  <herzi@gnome-de.org>

	* configure.ac: require newer libebook
	* src/lb-module-evolution.c: update to the new EContactPhoto API

2006-01-28  Richard Hult  <richard@imendio.com>

	Patch from Ulrik Sverdrup:

	* src/lb-module-recent.c: (lb_module_recent_query): Convert
	filenames for display (also use that for searching). Check that
	recent files still exist before adding them.

2006-01-21  Richard Hult  <richard@imendio.com>

	* src/lb-module-applications.c (module_applications_query):
	Protect against NULL result from gmenu, can happen.

	* src/bacon-message-connection.c:
	* src/bacon-message-connection.h: Update from bacon.

2005-11-27  Richard Hult  <richard@imendio.com>

	* configure.ac: Add fr translation from Olivier Lê Thanh Duong.

2005-11-27  Richard Hult  <richard@imendio.com>

	* src/lb-window.c: (window_set_transparent): Only set a shadow for
	the treeview when in non-transparent mode, another patch from
	Christian.

2005-11-27  Richard Hult  <richard@imendio.com>

	* Makefile.am:
	* configure.ac:
	* po/POTFILES.in:
	* po/sv.po:
	* src/Makefile.am:
	* src/lb-action.c:
	* src/lb-bookmark-list.h:
	* src/lb-main.c: (show_window), (main):
	* src/lb-module-applications.c:
	(module_applications_add_directory):
	* src/lb-window.c: (lb_window_init): Add i18n support, add more
	warnings and fix the code.

2005-11-26  Richard Hult  <richard@imendio.com>

	* Improve behavior for esc/tab/arrow keys so that searches are not
	reset while navigating the UI. Also reuse the last search when
	tabbing back to the icon box from the action box.

2005-11-26  Richard Hult  <richard@imendio.com>

	* configure.ac:
	* src/Makefile.am:
	* src/lb-frame.c:
	* src/lb-frame.h:
	* src/lb-icon-box.c:
	* src/lb-icon-box.h:
	* src/lb-main.c:
	* src/lb-window.c:
	* src/lb-window.h: Add eye-candy patch from Christian Kellner to
	add a nice rounded frame for the icon and action boxes. Also add
	support for real transparency when composite if available.
	
2005-11-22  Richard Hult  <richard@imendio.com>

	Apply patches from Christian Kellner:

	* configure.ac: Check for GTK+ 2.6, don't check for Xinerama.

	* src/lb-main.c: (main): Add cmdline option for displaying no
	window when launched.
	
	* src/lb-module-evolution.c: (module_evolution_activate_action):
	Include the name for email hits.

	* src/lb-window.c: (lb_window_present), (window_set_position):
	Improve the window positioning and use gdk instead of Xinerama
	directly.

2005-10-26  Richard Hult  <richard@imendio.com>

	* src/bacon-message-connection.c: Update.

	* src/lb-module-applications.c: Use the correct include path for
	gmenu.

2005-10-14  Richard Hult  <richard@imendio.com>

	* src/lb-window.c (window_key_press_event_cb): Don't reset the
	search when we just press a modifier.

	* src/lb-bookmark-list.c: Fix gcc 4 warnings...

	* src/lb-icon-box.c: (lb_icon_box_init): Use size define.

	* src/lb-module-applications.c: (module_applications_query),
	(module_applications_add_directory): Don't create a pixbuf, just
	set the icon-name.

	* src/lb-module-manager.c: (lb_module_manager_query),
	(lb_module_manager_set_actions):

	* src/lb-module-recent.c: (lb_module_recent_query): Set icon-name
	only, not pixbuf. Use the mime type to get the icon instead of
	doing slow gnome-vfs lookups, that was adding several seconds when
	searching.

	* src/lb-object.c: (lb_object_set_property),
	(lb_object_get_property): Indentation.

	* src/lb-utils.c: (lb_get_icon_name_for_uri): 
	(lb_get_pixbuf_from_icon_name): Use 48x48 since most icons are not
	available in 64x64.

	* src/lb-window.c: (lb_window_init): Use size define.
	(window_key_press_event_cb): Base the search timeout on how many
	characters we have in the search string, making it faster when we
	have more chars.

2005-10-14  Richard Hult  <richard@imendio.com>

	* src/Makefile.am:
	* src/lb-module-applications.c: (module_applications_finalize),
	(module_applications_query), (module_applications_add_actions),
	(module_applications_ensure_app_list),
	(module_applications_free_app_list):
	* src/lb-window.c:
	* tests/Makefile.am:
	* tests/search-applications.c: (applications_get_list), (main):
	Patch from adam@battleaxe.net to update menu api to GNOME 2.12.

2005-10-14  Richard Hult  <richard@imendio.com>

	* src/lb-utils.c (lb_string_has_substring) 
	(lb_string_markup_substring): Use G_NORMALIZE_ALL_COMPOSE,
	otherwise multibyte chars get messed up.

2005-09-11  Richard Hult  <richard@imendio.com>

	* src/lb-window.c (window_key_press_event_cb): Don't continue
	searching after the window is hidden.

2005-09-06  Magnus Therning  <magnus@therning.org>

	* configure.ac:
	* src/lb-window.c: (window_set_position): Add support for Xinerama
	by placing the window centered on one of the screens.

2005-09-02  Richard Hult  <richard@imendio.com>

	* src/lb-window.c: (window_set_action), (window_search_timeout_cb),
	(window_search_reset_timeout_cb), (window_perform_search),
	(window_show_search_results), (window_hide_search_results),
	(window_result_row_selected_cb): Improve the delayed activation
	and make it really work.

2005-09-01  Richard Hult  <richard@imendio.com>

	* src/lb-module-applications.c:
	(module_applications_activate_action):
	* src/lb-window.c: (lb_window_init),
	(window_result_row_selected_cb),
	(window_string_utf8_remove_last_char),
	(window_key_press_event_cb): Make pressing enter before any hits
	has turned up possible, and activate the first found hit when this
	happens.

2005-09-01  Richard Hult  <richard@imendio.com>

	* src/lb-window.c: (lb_window_class_init), (lb_window_init),
	(window_realize_cb), (lb_window_new), (window_perform_search),
	(lb_window_present), (window_get_is_visible),
	(window_set_position): Make the window resize when we have hits
	instead of being large all the time. Add a small instructional
	label to the empty window to help the user. Place the window at
	1/3 of the height of the screen instead of in the middle.
	(window_key_press_event_cb): Hide the search results when we hit
	esc or activate.

2005-08-31  Richard Hult  <richard@imendio.com>

	* src/lb-action.c (lb_action_new): Remove C++ comment.

	* src/lb-main.c (main_show_window): Rearrange a bit.

	* src/lb-icon-box.c: (lb_icon_box_init): Don't set the text color
	to white now that we have a normal background.

	* src/lb-window.c: (lb_window_init), (lb_window_get_property),
	(window_realize_cb), (window_set_focus), (window_set_item),
	(window_set_action), (window_perform_search), (window_grab),
	(lb_window_present): Remove the hack that pretends that we have a
	transparent window. It's not working for all setups and looks
	really ugly when the background changes. Let's do this for real
	with composite when that's ready.
 	
	* configure.ac: Clean up a little bit.

	* tests/mime-actions.c (main, print_actions_for_file): Fix
	warnings, don't crash on invalid input.

2005-04-14  Mikael Hallendal  <micke@imendio.com>

	* configure.ac:
	- Depend on gnome-desktop
	* src/lb-item-application.[ch]:
	- Removed command property and added desktop-file instead.
	* src/lb-module-applications.c:
	(module_applications_query),
	(module_applications_add_actions),
	(module_applications_activate_action):
	- Use gnome_desktop_item_launch to launch applications instead. 
	- This should make startup notifications working.

2005-04-13  Mikael Hallendal  <micke@imendio.com>

	* src/lb-utils.c: (lb_get_pixbuf_from_icon_name):
	- Check if icon_name is NULL so we don't crash if application doesn't
	  have an icon. Fixes GLB-2.

2005-04-01  Mikael Hallendal  <micke@imendio.com>

	* AUTHORS: Fixed Mitch's email.

2005-01-21  Mikael Hallendal  <micke@imendio.com>

	* TODO: Updated
	* configure.ac: Fixed copyright year

2005-01-11  Mikael Hallendal  <micke@imendio.com>

	* Release 0.1

