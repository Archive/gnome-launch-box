/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module-evolution.c
 */

#include "config.h"

#include <bonobo/bonobo-main.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <glib/gi18n.h>
#include <libebook/e-book.h>

#include "lb-item-contact.h"
#include "lb-module-evolution.h"

#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_MODULE_EVOLUTION, LbModuleEvolutionPriv))

typedef struct _LbModuleEvolutionPriv LbModuleEvolutionPriv;
struct _LbModuleEvolutionPriv {
	EBook *book;
};

#if 0
enum {
	PROP_0,
	PROP_NAME,
	PROP_STOCK_ID,
	PROP_PIXBUF
};
#endif


static void    module_evolution_finalize     (GObject            *object);
static void    module_evolution_set_property (GObject            *object,
					      guint               property_id,
					      const GValue       *value,
					      GParamSpec         *pspec);
static void    module_evolution_get_property (GObject            *object,
					      guint               property_id,
					      GValue             *value,
					      GParamSpec         *pspec);

static LbModuleFlags module_evolution_get_flags (LbModule           *module);
static GList * module_evolution_query           (LbModule           *module,
						 const gchar        *match);
static void    module_evolution_add_actions     (LbModule           *module,
                                                 LbItem             *item);
static void    module_evolution_activate_action (LbAction          *action,
						 LbModuleEvolution *module);
static void    module_evolution_backend_died    (EBook              *book,
						 LbModuleEvolution   *module);
static EBookQuery * module_evolution_build_e_query (const gchar     *query_str);
static gboolean     module_evolution_open_book     (LbModuleEvolution *module);
static const gchar * module_evolution_get_email    (EContact          *contact);

G_DEFINE_TYPE (LbModuleEvolution, lb_module_evolution, LB_TYPE_MODULE);
static LbModuleClass *parent_class = NULL;

static void
lb_module_evolution_class_init (LbModuleEvolutionClass *klass)
{
	GObjectClass  *object_class = G_OBJECT_CLASS (klass);
	LbModuleClass *module_class = LB_MODULE_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize        = module_evolution_finalize;
	object_class->set_property    = module_evolution_set_property;
	object_class->get_property    = module_evolution_get_property;

	module_class->get_flags       = module_evolution_get_flags;
	module_class->query           = module_evolution_query;
	module_class->add_actions     = module_evolution_add_actions;

#if 0
	g_object_class_install_property (object_class, PROP_NAME,
					 g_param_spec_string ("name",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));
#endif

	g_type_class_add_private (object_class,
				  sizeof (LbModuleEvolutionPriv));

}

static void
lb_module_evolution_init (LbModuleEvolution *module)
{
	LbModuleEvolutionPriv *priv;

	priv = GET_PRIV (module);

	if (module_evolution_open_book (module)) {
		g_signal_connect (priv->book, "backend-died",
				  G_CALLBACK (module_evolution_backend_died),
				  module);
	}
}

static void
module_evolution_finalize (GObject *object)
{
	LbModuleEvolutionPriv *priv;

	priv = GET_PRIV (object);

	if (priv->book) {
		g_object_unref (priv->book);
	}

#if 0
	if (evolution->name) {
		g_free (evolution->name);
		evolution->name = NULL;
	}
	if (evolution->stock_id) {
		g_free (evolution->stock_id);
		evolution->stock_id = NULL;
	}
	if (evolution->pixbuf) {
		g_object_unref (evolution->pixbuf);
		evolution->pixbuf = NULL;
	}
#endif

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
module_evolution_set_property (GObject      *object,
			       guint         property_id,
			       const GValue *value,
			       GParamSpec   *pspec)
{
	LbModuleEvolutionPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
#if 0
	case PROP_NAME:
		if (evolution->name)
			g_free (evolution->name);
		evolution->name = g_value_dup_string (value);
		break;
	case PROP_STOCK_ID:
		if (evolution->stock_id)
			g_free (evolution->stock_id);
		evolution->stock_id = g_value_dup_string (value);
		break;
	case PROP_PIXBUF:
		if (evolution->pixbuf)
			g_object_unref (evolution->pixbuf);
		evolution->pixbuf = GDK_PIXBUF (g_value_dup_object (value));
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
module_evolution_get_property (GObject    *object,
			       guint       property_id,
			       GValue     *value,
			       GParamSpec *pspec)
{
	LbModuleEvolutionPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
#if 0
	case PROP_NAME:
		g_value_set_string (value, evolution->name);
		break;
	case PROP_STOCK_ID:
		g_value_set_string (value, evolution->stock_id);
		break;
	case PROP_PIXBUF:
		g_value_set_object (value, evolution->pixbuf);
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static LbModuleFlags
module_evolution_get_flags (LbModule *module)
{
	return LB_MODULE_DEFAULT_FLAGS & (~LB_MODULE_FLAGS_SLUGS_SUPPORTED);
}

static GList *
module_evolution_query (LbModule *module, const gchar *match)
{
	LbModuleEvolutionPriv *priv;
	EBookQuery           *query;
	gboolean              result;
	GList                *evolution, *l;
	GList                *items;

	priv = GET_PRIV (module);

	if (!priv->book) {
		if (!module_evolution_open_book (LB_MODULE_EVOLUTION (module))) {
			return NULL;
		}
	}
	
	query = module_evolution_build_e_query (match);
	result = e_book_get_contacts (priv->book, query, &evolution, NULL);
	e_book_query_unref (query);

	if (result == FALSE) {
		g_warning ("Error while querying evolution data server");
		return NULL;
	}

	items = NULL;
	for (l = evolution; l; l = l->next) {
		EContact        *contact;
		LbItem          *item;
		const gchar     *name;
		EContactPhoto   *photo;
		const gchar     *email;

		contact = E_CONTACT (l->data);
		name = e_contact_get_const (contact, E_CONTACT_FULL_NAME);
		photo = e_contact_get (contact, E_CONTACT_PHOTO);

		email = module_evolution_get_email (contact);

		item = g_object_new (LB_TYPE_ITEM_CONTACT,
				     "name", name,
				     "email", email,
				     NULL);
		if (photo) {
			GdkPixbufLoader *loader;
			GdkPixbuf       *pixbuf;

			if(photo->type == E_CONTACT_PHOTO_TYPE_INLINED) {
				loader = gdk_pixbuf_loader_new ();
				gdk_pixbuf_loader_write (loader, photo->data.inlined.data,
							 photo->data.inlined.length, NULL);
				pixbuf = gdk_pixbuf_loader_get_pixbuf (loader);
				if (pixbuf) {
					g_object_set (item,
						      "pixbuf", pixbuf,
						      NULL);
				}
				gdk_pixbuf_loader_close (loader, NULL);
				g_object_unref (loader);
			} else {
				// FIXME: implement E_CONTACT_PHOTO_TYPE_URL
			}
		} else {
			g_object_set (item,
				      "icon-name", "stock_person",
				      NULL);
		}

		items = g_list_prepend (items, item);
	}

	return items;
}

static void
module_evolution_add_actions (LbModule *module, LbItem *item)
{
	LbAction *action;

	if (!LB_IS_ITEM_CONTACT (item)) {
		return;
	}

	if (!lb_item_contact_get_email (LB_ITEM_CONTACT (item))) {
		/* We only want to add an action if we can actually mail
		 * to the contact
		 */
		return;
	}
	
	action = g_object_new (LB_TYPE_ACTION,
			       "name", _("Mail to"),
			       "icon-name", "stock_mail-compose",
			       NULL);

	g_signal_connect (action, "activate",
			  G_CALLBACK (module_evolution_activate_action),
			  module);

	lb_item_add_action (item, action);

	g_object_unref (action);
}

static void
module_evolution_activate_action (LbAction          *action,
				  LbModuleEvolution *module)
{
	LbItemContact *contact;
	gchar         *command;
	gboolean       result;
	GError        *error;

	contact = LB_ITEM_CONTACT (lb_action_get_item (action));
	command = g_strdup_printf ("evolution \"mailto:%s <%s>\"",
				   LB_OBJECT (contact)->name, 
				   lb_item_contact_get_email (contact));

	g_print ("Executing: %s\n", command);
	
	error = NULL;
	result = g_spawn_command_line_async (command, &error);
	g_free (command);

	if (result == FALSE) {
		g_warning ("Couldn't spawn evolution: %s", error->message);
		g_error_free (error);
	}
}

static void
module_evolution_backend_died (EBook *book, LbModuleEvolution *modules)
{
	LbModuleEvolutionPriv *priv;

	priv = GET_PRIV (modules);

	if (priv->book) {
		g_object_unref (priv->book);
		priv->book = NULL;
	}
}

static EBookQuery *
module_evolution_build_e_query (const gchar *query_str)
{
	EBookQuery *query;
	EBookQuery *queries[11];

	queries[0] = e_book_query_field_test (E_CONTACT_GIVEN_NAME,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[1] = e_book_query_field_test (E_CONTACT_FAMILY_NAME,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[2] = e_book_query_field_test (E_CONTACT_NICKNAME,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[3] = e_book_query_field_test (E_CONTACT_EMAIL,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[4] = e_book_query_field_test (E_CONTACT_FULL_NAME,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);

	/* Should we do the query here or leave that to an IM aware 
	 * application
	 */
	queries[5] = e_book_query_field_test (E_CONTACT_IM_AIM,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[6] = e_book_query_field_test (E_CONTACT_IM_GROUPWISE,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[7] = e_book_query_field_test (E_CONTACT_IM_JABBER,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[8] = e_book_query_field_test (E_CONTACT_IM_YAHOO,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[9] = e_book_query_field_test (E_CONTACT_IM_MSN,
					      E_BOOK_QUERY_BEGINS_WITH,
					      query_str);
	queries[10] = e_book_query_field_test (E_CONTACT_IM_ICQ,
					       E_BOOK_QUERY_BEGINS_WITH,
					       query_str);

	query = e_book_query_or (11, queries, TRUE);

	return query;
}

static gboolean
module_evolution_open_book (LbModuleEvolution *modules)
{
	LbModuleEvolutionPriv *priv;
	EBook                *book;

	priv = GET_PRIV (modules);
	book = e_book_new_system_addressbook (NULL);

	if (priv->book) {
		g_object_unref (priv->book);
	}

	if (!e_book_open (book, TRUE, NULL)) {
		priv->book = NULL;
		return FALSE;
	}

	priv->book = book;
	return TRUE;
}

static const gchar *
module_evolution_get_email (EContact *contact)
{
	gint i;

	for (i = E_CONTACT_FIRST_EMAIL_ID; i <= E_CONTACT_LAST_EMAIL_ID; ++i) {
		if (e_contact_get_const (contact, i)) {
			return e_contact_get_const (contact, i);
		}
	}

	return NULL;
}

