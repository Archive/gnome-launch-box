/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-object.h
 */

#ifndef __LB_OBJECT_H__
#define __LB_OBJECT_H__

#include <glib-object.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#define LB_TYPE_OBJECT            (lb_object_get_type ())
#define LB_OBJECT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_OBJECT, LbObject))
#define LB_OBJECT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_OBJECT, LbObjectClass))
#define LB_IS_OBJECT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_OBJECT))
#define LB_IS_OBJECT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_OBJECT))
#define LB_OBJECT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_OBJECT, LbObjectClass))


typedef struct _LbObject      LbObject;
typedef struct _LbObjectClass LbObjectClass;

struct _LbObject {
	GObject    parent_instance;

	gchar     *name;
	gchar     *icon_name;
	GdkPixbuf *pixbuf;
};

struct _LbObjectClass {
	GObjectClass  parent_class;
};


GType   lb_object_get_type (void) G_GNUC_CONST;


#endif /* __LB_OBJECT_H__ */
