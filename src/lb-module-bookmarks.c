/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module-bookmarks.c
 */

#include "config.h"

#include <string.h>

#include <gtk/gtk.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "lb-action.h"
#include "lb-bookmark-list.h"
#include "lb-item-file.h"
#include "lb-module-bookmarks.h"
#include "lb-utils.h"


#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_MODULE_BOOKMARKS, LbModuleBookmarksPriv))

typedef struct _LbModuleBookmarksPriv LbModuleBookmarksPriv;

struct _LbModuleBookmarksPriv {
	gchar  *filename_firefox;
	gchar  *filename_epiphany;
	GList  *bookmarks;
	time_t  mtime_firefox;
	time_t  mtime_epiphany;
};

enum {
	PROP_0,
};


static void    lb_module_bookmarks_finalize     (GObject      *object);
static void    lb_module_bookmarks_set_property (GObject      *object,
						 guint         property_id,
						 const GValue *value,
						 GParamSpec   *pspec);
static void    lb_module_bookmarks_get_property (GObject      *object,
						 guint         property_id,
						 GValue       *value,
						 GParamSpec   *pspec);

static GList * lb_module_bookmarks_query        (LbModule     *module,
						 const gchar  *match);
static void    lb_module_bookmarks_add_actions  (LbModule     *module,
						 LbItem       *item);
static void    lb_module_bookmarks_activate     (LbAction     *action,
						 LbModule     *module);


G_DEFINE_TYPE (LbModuleBookmarks, lb_module_bookmarks, LB_TYPE_MODULE);
static LbModuleClass *parent_class = NULL;

static void
lb_module_bookmarks_class_init (LbModuleBookmarksClass *klass)
{
	GObjectClass  *object_class = G_OBJECT_CLASS (klass);
	LbModuleClass *module_class = LB_MODULE_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = lb_module_bookmarks_finalize;
	object_class->set_property = lb_module_bookmarks_set_property;
	object_class->get_property = lb_module_bookmarks_get_property;

	module_class->query        = lb_module_bookmarks_query;
	module_class->add_actions  = lb_module_bookmarks_add_actions;

	g_type_class_add_private (object_class,
				  sizeof (LbModuleBookmarksPriv));
}

static void
lb_module_bookmarks_init (LbModuleBookmarks *bookmarks)
{
	LbModuleBookmarksPriv *priv = GET_PRIV (bookmarks);
	gchar                 *path;

	path = lb_get_firefox_path ();
	priv->filename_firefox = lb_bookmark_list_get_path_firefox (path);
	g_free (path);
	priv->filename_epiphany = lb_bookmark_list_get_path_epiphany ();
}

static void
lb_module_bookmarks_finalize (GObject *object)
{
	LbModuleBookmarksPriv *priv = GET_PRIV (object);

	if (priv->bookmarks) {
		lb_bookmark_list_free (priv->bookmarks);
		g_list_free (priv->bookmarks);
		priv->bookmarks = NULL;
	}

	g_free (priv->filename_firefox);
	g_free (priv->filename_epiphany);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
lb_module_bookmarks_set_property (GObject      *object,
				  guint         property_id,
				  const GValue *value,
				  GParamSpec   *pspec)
{
	switch (property_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
lb_module_bookmarks_get_property (GObject    *object,
				  guint       property_id,
				  GValue     *value,
				  GParamSpec *pspec)
{
	switch (property_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static gboolean
lb_module_bookmarks_file_changed (const char *filename, time_t mtime)
{
	struct stat buf;

	/* FIXME: use GFile */

	if (stat (filename, &buf) < 0) {
		if (mtime == -1)
			return FALSE;
		return TRUE;
	}
	return (buf.st_mtime != mtime);
}

static GList *
lb_module_bookmarks_query (LbModule    *module,
			   const gchar *match)
{
	LbModuleBookmarksPriv *priv = GET_PRIV (module);
        GList                 *list;
        GList                 *result = NULL;
	struct stat            buf;

	if (priv->bookmarks) {
		if (lb_module_bookmarks_file_changed (priv->filename_firefox, priv->mtime_firefox) ||
		    lb_module_bookmarks_file_changed (priv->filename_epiphany, priv->mtime_epiphany)) {
			lb_bookmark_list_free (priv->bookmarks);
			g_list_free (priv->bookmarks);
			priv->bookmarks      = NULL;
			priv->mtime_firefox  = 0;
			priv->mtime_epiphany = 0;
		}
	}

	if (! priv->bookmarks) {
		if (stat (priv->filename_firefox, &buf) == 0) {
			priv->mtime_firefox = buf.st_mtime;
			priv->bookmarks     = lb_bookmark_list_get_firefox (priv->filename_firefox);
		} else {
			priv->mtime_firefox = -1;
		}
		if (stat (priv->filename_epiphany, &buf) == 0) {
			GList *bookmarks;
			priv->mtime_epiphany = buf.st_mtime;
			bookmarks = lb_bookmark_list_get_epiphany (priv->filename_epiphany);
			priv->bookmarks = g_list_concat (priv->bookmarks, bookmarks);
		} else {
			priv->mtime_epiphany = -1;
		}
	}

	for (list = priv->bookmarks; list; list = list->next) {
		LbBookmark *bookmark = list->data;

		if (lb_string_has_substring (bookmark->name, match)) {
			LbItem    *new;

			if (bookmark->icon_path) {
				GdkPixbuf *pixbuf;
				pixbuf = gdk_pixbuf_new_from_file (bookmark->icon_path,
								   NULL);
				new = g_object_new (LB_TYPE_ITEM_FILE,
						    "name",      bookmark->name,
						    "uri",       bookmark->href,
						    "pixbuf",    pixbuf,
						    NULL);
				if (pixbuf) {
					gdk_pixbuf_unref (pixbuf);
				}
			} else {
				new = g_object_new (LB_TYPE_ITEM_FILE,
						    "name",      bookmark->name,
						    "uri",       bookmark->href,
						    "icon-name", "gnome-fs-bookmark",
						    NULL);
			}

			result = g_list_prepend (result, new);
		}
        }

        return g_list_reverse (result);
}

static void
lb_module_bookmarks_add_actions (LbModule *module,
				 LbItem   *item)
{
	LbAction    *action;
	const gchar *uri;

	if (! LB_IS_ITEM_FILE (item)) {
		return;
	}

	uri = lb_item_file_get_uri (LB_ITEM_FILE (item));

	action = lb_action_new (LB_ACTION_OPEN);

	lb_action_set_data (action, g_strdup (uri), g_free);

	g_signal_connect (action, "activate",
			  G_CALLBACK (lb_module_bookmarks_activate),
			  module);

	lb_item_add_action (item, action);
	g_object_unref (action);
}

static void
lb_module_bookmarks_activate (LbAction *action,
			      LbModule *module)
{
	const gchar *uri;
	GError      *error = NULL;

	uri = (const gchar *) lb_action_get_data (action);

	/* FIXME: add the GdkScreen */
	if (! gtk_show_uri (NULL, uri, GDK_CURRENT_TIME, &error)) {
		g_print ("Failed to launch: '%s'\n", error->message);
		g_error_free (error);
	}
}

