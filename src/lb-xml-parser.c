/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * The GIMP -- an image manipulation program
 * Copyright (C) 1995-1997 Spencer Kimball and Peter Mattis
 *
 * GimpXmlParser
 * Copyright (C) 2003  Sven Neumann <sven@gimp.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <string.h>

#include <glib-object.h>

#include "lb-xml-parser.h"


struct _LbXmlParser {
	GMarkupParseContext *context;
};


static gboolean parse_encoding (const gchar  *text,
                                gint          text_len,
                                gchar       **encodind);


/**
 * lb_xml_parser_new:
 * @markup_parser: a #GMarkupParser
 * @user_data: user data to pass to #GMarkupParser functions
 *
 * LbXmlParser is a thin wrapper around GMarkupParser. This function
 * creates one for you and sets up a GMarkupParseContext.
 *
 * Return value: a new #LbXmlParser
 **/
LbXmlParser *
lb_xml_parser_new (const GMarkupParser *markup_parser,
		   gpointer             user_data)
{
	LbXmlParser *parser;

	g_return_val_if_fail (markup_parser != NULL, NULL);

	parser = g_new (LbXmlParser, 1);

	parser->context = g_markup_parse_context_new (markup_parser,
						      0, user_data, NULL);

	return parser;
}

/**
 * lb_xml_parser_parse_file:
 * @parser: a #LbXmlParser
 * @filename: name of a file to parse
 * @error: return location for possible errors
 *
 * This function creates a GIOChannel for @filename and calls
 * lb_xml_parser_parse_io_channel() for you.
 *
 * Return value: %TRUE on success, %FALSE otherwise
 **/
gboolean
lb_xml_parser_parse_file (LbXmlParser  *parser,
			  const gchar  *filename,
			  GError      **error)
{
	GIOChannel *io;
	gboolean    success;

	g_return_val_if_fail (parser != NULL, FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	io = g_io_channel_new_file (filename, "r", error);
	if (!io)
		return FALSE;

	success = lb_xml_parser_parse_io_channel (parser, io, error);

	g_io_channel_unref (io);

	return success;
}

/**
 * lb_xml_parser_parse_fd:
 * @parser: a #LbXmlParser
 * @fd:     a file descriptor
 * @error: return location for possible errors
 *
 * This function creates a GIOChannel for @fd and calls
 * lb_xml_parser_parse_io_channel() for you.
 *
 * Return value: %TRUE on success, %FALSE otherwise
 **/
gboolean
lb_xml_parser_parse_fd (LbXmlParser  *parser,
			gint          fd,
			GError      **error)
{
	GIOChannel *io;
	gboolean    success;

	g_return_val_if_fail (parser != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	io = g_io_channel_unix_new (fd);

	success = lb_xml_parser_parse_io_channel (parser, io, error);

	g_io_channel_unref (io);

	return success;
}

/**
 * lb_xml_parser_parse_io_channel:
 * @parser: a #LbXmlParser
 * @io: a #GIOChannel
 * @error: return location for possible errors
 *
 * Makes @parser read from the specified @io channel. This function
 * returns when the GIOChannel becomes empty (end of file) or an
 * error occurs, either reading from @io or parsing the read data.
 *
 * This function tries to determine the character encoding from the
 * XML header and converts the content to UTF-8 for you. For this
 * feature to work, the XML header with the encoding attribute must be
 * contained in the first 4096 bytes read. Otherwise UTF-8 encoding
 * will be assumed and parsing may break later if this assumption
 * was wrong.
 *
 * Return value: %TRUE on success, %FALSE otherwise
 **/
gboolean
lb_xml_parser_parse_io_channel (LbXmlParser  *parser,
				GIOChannel   *io,
				GError      **error)
{
	GIOStatus    status;
	guchar       buffer[4096];
	gsize        len = 0;
	gsize        bytes;
	const gchar *io_encoding;
	gchar       *encoding = NULL;

	g_return_val_if_fail (parser != NULL, FALSE);
	g_return_val_if_fail (io != NULL, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	io_encoding = g_io_channel_get_encoding (io);
	if (io_encoding && strcmp (io_encoding, "UTF-8")) {
		g_warning ("lb_xml_parser_parse_io_channel():\n"
			   "The encoding has already been set on this GIOChannel!");
		return FALSE;
	}

	/* try to determine the encoding */

	g_io_channel_set_encoding (io, NULL, NULL);

	while (len < sizeof (buffer)) {
		status = g_io_channel_read_chars (io, buffer + len, 1,
						  &bytes, error);
		len += bytes;

		if (status == G_IO_STATUS_ERROR)
			return FALSE;
		if (status == G_IO_STATUS_EOF)
			break;

		if (parse_encoding (buffer, len, &encoding))
			break;
	}

	if (encoding) {
		if (! g_io_channel_set_encoding (io, encoding, error))
			return FALSE;

		g_free (encoding);
	} else {
		g_io_channel_set_encoding (io, "UTF-8", NULL);
	}

	while (TRUE) {
		if (!g_markup_parse_context_parse (parser->context, buffer,
						   len, error))
			return FALSE;

		status = g_io_channel_read_chars (io,
						  buffer, sizeof (buffer),
						  &len, error);

		switch (status) {
		case G_IO_STATUS_ERROR:
			return FALSE;
		case G_IO_STATUS_EOF:
			return g_markup_parse_context_end_parse (parser->context,
								 error);
		case G_IO_STATUS_NORMAL:
		case G_IO_STATUS_AGAIN:
			break;
		}
	}
}

gboolean
lb_xml_parser_parse_buffer (LbXmlParser *parser,
			    const gchar *buffer,
			    gssize       len,
			    GError     **error)
{
	gchar    *encoding = NULL;
	gchar    *conv     = NULL;
	gboolean  success;

	g_return_val_if_fail (parser != NULL, FALSE);
	g_return_val_if_fail (buffer != NULL || len == 0, FALSE);
	g_return_val_if_fail (error == NULL || *error == NULL, FALSE);

	if (len < 0)
		len = strlen (buffer);

	if (parse_encoding (buffer, len, &encoding)) {
		if (g_ascii_strcasecmp (encoding, "UTF-8") &&
		    g_ascii_strcasecmp (encoding, "UTF8")) {
			conv = g_convert (buffer, len, "UTF-8", encoding,
					  NULL, &len, error);

			g_free (encoding);

			if (! conv)
				return FALSE;
		}
	}

	success = g_markup_parse_context_parse (parser->context,
						conv ? conv : buffer,
						len, error);

	g_free (conv);

	return success;
}

/**
 * lb_xml_parser_free:
 * @parser: a #LbXmlParser
 *
 * Frees the resources allocated for @parser. You must not access
 * @parser after calling this function.
 **/
void
lb_xml_parser_free (LbXmlParser *parser)
{
	g_return_if_fail (parser != NULL);

	g_markup_parse_context_free (parser->context);
	g_free (parser);
}


/* Try to determine encoding from XML header.  This function returns
   FALSE when it doesn't have enough text to parse.  It returns TRUE
   and sets encoding when the XML header has been parsed.
 */
static gboolean
parse_encoding (const gchar  *text,
                gint          text_len,
                gchar       **encoding)
{
	const gchar *start;
	const gchar *end;
	gint         i;

	g_return_val_if_fail (text, FALSE);

	if (text_len < 20)
		return FALSE;

	start = g_strstr_len (text, text_len, "<?xml");
	if (!start)
		return FALSE;

	end = g_strstr_len (start, text_len - (start - text), "?>");
	if (!end)
		return FALSE;

	*encoding = NULL;

	text_len = end - start;
	if (text_len < 12)
		return TRUE;

	start = g_strstr_len (start + 1, text_len - 1, "encoding");
	if (!start)
		return TRUE;

	start += 8;

	while (start < end && *start == ' ')
		start++;

	if (*start != '=')
		return TRUE;

	start++;

	while (start < end && *start == ' ')
		start++;

	if (*start != '\"' && *start != '\'')
		return TRUE;

	text_len = end - start;
	if (text_len < 1)
		return TRUE;

	for (i = 1; i < text_len; i++)
		if (start[i] == start[0])
			break;

	if (i == text_len || i < 3)
		return TRUE;

	*encoding = g_strndup (start + 1, i - 1);

	return TRUE;
}
