/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-object.c
 */

#include "config.h"

#include <string.h>

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "lb-object.h"
#include "lb-utils.h"

enum {
	PROP_0,
	PROP_NAME,
	PROP_ICON_NAME,
	PROP_PIXBUF
};


static void   lb_object_class_init   (LbObjectClass *klass);
static void   lb_object_init         (LbObject      *object);

static void   lb_object_finalize     (GObject       *object);
static void   lb_object_set_property (GObject       *object,
                                      guint          property_id,
                                      const GValue  *value,
                                      GParamSpec    *pspec);
static void   lb_object_get_property (GObject       *object,
                                      guint          property_id,
                                      GValue        *value,
                                      GParamSpec    *pspec);


static GObjectClass *parent_class = NULL;


GType
lb_object_get_type (void)
{
	static GType type = 0;

	if (! type) {
		static const GTypeInfo info = {
			sizeof (LbObjectClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) lb_object_class_init,
			NULL,           /* class_finalize */
			NULL,           /* class_data     */
			sizeof (LbObject),
			0,              /* n_preallocs    */
			(GInstanceInitFunc) lb_object_init,
		};

		type = g_type_register_static (G_TYPE_OBJECT,
					       "LbObject",
					       &info, 0);
	}

	return type;
}

static void
lb_object_class_init (LbObjectClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = lb_object_finalize;
	object_class->set_property = lb_object_set_property;
	object_class->get_property = lb_object_get_property;

	g_object_class_install_property (object_class, PROP_NAME,
					 g_param_spec_string ("name",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_ICON_NAME,
					 g_param_spec_string ("icon-name",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE));

	g_object_class_install_property (object_class, PROP_PIXBUF,
					 g_param_spec_object ("pixbuf",
							      NULL, NULL,
							      GDK_TYPE_PIXBUF,
							      G_PARAM_READWRITE));
}

static void
lb_object_init (LbObject *object)
{
}

static void
lb_object_finalize (GObject *gobject)
{
	LbObject *object = LB_OBJECT (gobject);

	if (object->name) {
		g_free (object->name);
		object->name = NULL;
	}
	if (object->icon_name) {
		g_free (object->icon_name);
		object->icon_name = NULL;
	}
	if (object->pixbuf) {
		g_object_unref (object->pixbuf);
		object->pixbuf = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (gobject);
}

static void
lb_object_set_property (GObject      *gobject,
                        guint         property_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
	LbObject *object = LB_OBJECT (gobject);

	switch (property_id) {
	case PROP_NAME:
		if (object->name) {
			g_free (object->name);
		}
		object->name = g_value_dup_string (value);
		break;
	case PROP_ICON_NAME:
		if (object->icon_name) {
			g_free (object->icon_name);
		}
		object->icon_name = g_value_dup_string (value);
		break;
	case PROP_PIXBUF:
		if (object->pixbuf) {
			g_object_unref (object->pixbuf);
		}
		object->pixbuf = GDK_PIXBUF (g_value_dup_object (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
lb_object_get_property (GObject    *gobject,
                        guint       property_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
	LbObject *object = LB_OBJECT (gobject);

	switch (property_id) {
	case PROP_NAME:
		g_value_set_string (value, object->name);
		break;
	case PROP_ICON_NAME:
		g_value_set_string (value, object->icon_name);
		break;
	case PROP_PIXBUF:
		if (!object->pixbuf && object->icon_name) {
			GdkPixbuf *pixbuf;

			pixbuf = lb_get_pixbuf_from_icon_name (object->icon_name);
			g_value_take_object (value, pixbuf);
		} else {
			g_value_set_object (value, object->pixbuf);
		}
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}
