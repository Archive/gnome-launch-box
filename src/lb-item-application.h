/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-item-application.h
 */

#ifndef __LB_ITEM_APPLICATION_H__
#define __LB_ITEM_APPLICATION_H__


#include "lb-item.h"


#define LB_TYPE_ITEM_APPLICATION            (lb_item_application_get_type ())
#define LB_ITEM_APPLICATION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_ITEM_APPLICATION, LbItemApplication))
#define LB_ITEM_APPLICATION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_ITEM_APPLICATION, LbItemApplicationClass))
#define LB_IS_ITEM_APPLICATION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_ITEM_APPLICATION))
#define LB_IS_ITEM_APPLICATION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_ITEM_APPLICATION))
#define LB_ITEM_APPLICATION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_ITEM_APPLICATION, LbItemApplicationClass))


typedef struct _LbItemApplication      LbItemApplication;
typedef struct _LbItemApplicationClass LbItemApplicationClass;

struct _LbItemApplication {
	LbItem  parent_instance;
};

struct _LbItemApplicationClass {
	LbItemClass  parent_class;
};


GType         lb_item_application_get_type (void) G_GNUC_CONST;
LbItem*       lb_item_application_new      (gchar const* desktop_file);

void          lb_item_application_add_actions      (LbItemApplication* item);

const gchar * lb_item_application_get_desktop_file (LbItemApplication *item);
void          lb_item_application_set_desktop_file (LbItemApplication *item,
						    const gchar       *path);

#endif /* __LB_ITEM_APPLICATION_H__ */
