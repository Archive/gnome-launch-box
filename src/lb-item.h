/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-item.h
 */

#ifndef __LB_ITEM_H__
#define __LB_ITEM_H__

#include "lb-object.h"

#define LB_TYPE_ITEM            (lb_item_get_type ())
#define LB_ITEM(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_ITEM, LbItem))
#define LB_ITEM_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_ITEM, LbItemClass))
#define LB_IS_ITEM(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_ITEM))
#define LB_IS_ITEM_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_ITEM))
#define LB_ITEM_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_ITEM, LbItemClass))

typedef struct _LbItem      LbItem;
typedef struct _LbItemClass LbItemClass;

#include "lb-action.h"

struct _LbItem {
	LbObject  parent_instance;
};

struct _LbItemClass {
	LbObjectClass  parent_class;
};


GType   lb_item_get_type    (void) G_GNUC_CONST;

void    lb_item_add_action  (LbItem      *item,
			     LbAction    *action);
GList * lb_item_get_actions (LbItem      *item,
			     const gchar *match);

#endif /* __LB_ITEM_H__ */
