/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-item-file.h
 */

#ifndef __LB_ITEM_FILE_H__
#define __LB_ITEM_FILE_H__


#include "lb-item.h"


#define LB_TYPE_ITEM_FILE            (lb_item_file_get_type ())
#define LB_ITEM_FILE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_ITEM_FILE, LbItemFile))
#define LB_ITEM_FILE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_ITEM_FILE, LbItemFileClass))
#define LB_IS_ITEM_FILE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_ITEM_FILE))
#define LB_IS_ITEM_FILE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_ITEM_FILE))
#define LB_ITEM_FILE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_ITEM_FILE, LbItemFileClass))


typedef struct _LbItemFile      LbItemFile;
typedef struct _LbItemFileClass LbItemFileClass;

struct _LbItemFile {
	LbItem  parent_instance;
};

struct _LbItemFileClass {
	LbItemClass  parent_class;
};


GType         lb_item_file_get_type      (void) G_GNUC_CONST;

const gchar * lb_item_file_get_uri       (LbItemFile  *item);
void          lb_item_file_set_uri       (LbItemFile  *item,
					  const gchar *uri);

const gchar * lb_item_file_get_mime_type (LbItemFile  *item);
void          lb_item_file_set_mime_type (LbItemFile  *item,
					  const gchar *mime_type);


#endif /* __LB_ITEM_FILE_H__ */
