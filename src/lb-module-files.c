/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module-files.c
 */

#include <config.h>

#include <string.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtkicontheme.h>
#include <gconf/gconf-client.h>

#include "lb-item.h"
#include "lb-module-files.h"
#include "lb-utils.h"
#include "lb-item-file.h"

#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_MODULE_FILES, LbModuleFilesPriv))

typedef struct _LbModuleFilesPriv LbModuleFilesPriv;
struct _LbModuleFilesPriv {
	gchar *dir_uri;
};

enum {
	PROP_0,
	PROP_DIR_URI
};


static void    module_files_finalize        (GObject            *object);
static void    module_files_set_property    (GObject            *object,
					     guint               property_id,
					     const GValue       *value,
					     GParamSpec         *pspec);
static void    module_files_get_property    (GObject            *object,
					     guint               property_id,
					     GValue             *value,
					     GParamSpec         *pspec);
static GList * module_files_query           (LbModule           *module,
					     const gchar        *match);
static void    module_files_add_actions     (LbModule           *module,
					     LbItem             *item);
static void    module_files_activate_action (LbAction           *action,
					     LbModuleFiles      *module);

G_DEFINE_TYPE (LbModuleFiles, lb_module_files, LB_TYPE_MODULE);
static LbModuleClass *parent_class = NULL;

static void
lb_module_files_class_init (LbModuleFilesClass *klass)
{
	GObjectClass  *object_class = G_OBJECT_CLASS (klass);
	LbModuleClass *module_class = LB_MODULE_CLASS (klass);
	const gchar   *desktop;
	gchar         *uri;

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = module_files_finalize;
	object_class->set_property = module_files_set_property;
	object_class->get_property = module_files_get_property;

	module_class->query        = module_files_query;
	module_class->add_actions  = module_files_add_actions;

	desktop = g_get_user_special_dir (G_USER_DIRECTORY_DESKTOP);
	if (desktop == NULL)
		desktop = g_get_home_dir ();
	uri = g_filename_to_uri (desktop, NULL, NULL);

	g_object_class_install_property (object_class, PROP_DIR_URI,
					 g_param_spec_string ("dir-uri",
							      NULL, NULL,
							      uri,
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_free (uri);

	g_type_class_add_private (object_class,
				  sizeof (LbModuleFilesPriv));
}

static void
lb_module_files_init (LbModuleFiles *files)
{
}

static void
module_files_finalize (GObject *object)
{
	LbModuleFilesPriv *priv = GET_PRIV (object);

	g_free (priv->dir_uri);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
module_files_set_property (GObject      *object,
			   guint         property_id,
			   const GValue *value,
			   GParamSpec   *pspec)
{
	LbModuleFilesPriv *priv = GET_PRIV (object);

	switch (property_id) {
	case PROP_DIR_URI:
		g_free (priv->dir_uri);
		priv->dir_uri = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
module_files_get_property (GObject    *object,
			   guint       property_id,
			   GValue     *value,
			   GParamSpec *pspec)
{
	LbModuleFilesPriv *priv = GET_PRIV (object);

	switch (property_id) {
	case PROP_DIR_URI:
		g_value_set_string (value, priv->dir_uri);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static GList *
module_files_query (LbModule    *module,
		    const gchar *match)
{
	LbModuleFilesPriv* priv  = GET_PRIV (module);
	GFileEnumerator  * enumerator;
	GFileInfo        * info;
	GError           * error;
	GList            * items = NULL;
	GFile            * parent;
	GFile            * file;

        parent = g_file_new_for_uri (priv->dir_uri);
        enumerator = g_file_enumerate_children (parent,
                                                G_FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME ","
                                                G_FILE_ATTRIBUTE_STANDARD_NAME ","
                                                G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
                                                G_FILE_QUERY_INFO_NONE, NULL,
                                                &error);

        for (info = g_file_enumerator_next_file (enumerator, NULL, NULL);
             info;
             info = g_file_enumerator_next_file (enumerator, NULL, NULL))
          {
            gchar const* name;
            gchar      * uri;


            if (!info)
              {
                g_printerr ("error getting file information: %s",
                            error->message);
                g_error_free (error);

                continue;
              }

            name = g_file_info_get_display_name (info);
            file = g_file_get_child (parent, g_file_info_get_name (info));
            uri = g_file_get_uri (file);

		if (name[0] != '.' && name[strlen (name) - 1] != '~') {
			gchar *dot = strrchr (name, '.');

			if (dot)
				*dot = '\0';

			if (lb_string_has_substring (name, match)) {
				LbItem    *item;
				gchar     *icon_name;

				icon_name = lb_get_icon_name_for_uri (uri);

				item = g_object_new (LB_TYPE_ITEM_FILE,
						     "name",      name,
						     "icon-name", icon_name,
						     "uri",       uri,
						     "mime-type", g_file_info_get_content_type (info),
						     NULL);

				g_free (icon_name);

				items = g_list_prepend (items, item);
			}
		}

            g_free (uri);
            g_object_unref (file);
            g_object_unref (info);
          }

        g_object_unref (enumerator);
        g_object_unref (parent);

	return items;
}

static void
module_files_add_actions (LbModule *module,
			  LbItem   *item)
{
	LbItemFile              *item_file;
	GAppInfo  * default_application;
	GList                   *applications = NULL;
	GList                   *actions = NULL;
	GList                   *list = NULL;
	const gchar             *mime_type;

	if (! LB_IS_ITEM_FILE (item)) {
		return;
	}

	item_file = LB_ITEM_FILE (item);

	mime_type = lb_item_file_get_mime_type (item_file);

	if (!mime_type)
	  {
	    GFileInfo* info;
	    GFile* file;

	    file = g_file_new_for_uri (lb_item_file_get_uri (item_file));
	    info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE,
			              0, NULL,
				      NULL);

	    if (info)
	      {
		lb_item_file_set_mime_type (item_file, g_file_info_get_content_type (info));
	      }

	      g_object_unref (info);
	      g_object_unref (file);

	      mime_type = lb_item_file_get_mime_type (item_file);
	}

	if (! mime_type) {
		return;
	}

	default_application = g_app_info_get_default_for_type (mime_type, TRUE);
	applications = g_app_info_get_all_for_type (mime_type);

	for (list = applications; list ; list = list->next) {
		GAppInfo *application = list->data;
		LbAction                *action;
		const gchar             *name;
		const gchar             *icon = NULL;
		GIcon                   *gicon;

		gicon = g_app_info_get_icon (application);
		if (G_IS_THEMED_ICON (gicon))
		  {
		    gchar const* const* names = g_themed_icon_get_names (G_THEMED_ICON (gicon));

		    icon = names ? names[0] : NULL;
		  }
		name = g_app_info_get_name (application);

		action = g_object_new (LB_TYPE_ACTION,
				       "name",      name,
				       "icon-name", icon,
				       NULL);

		lb_action_set_data (action, application,
				    (GDestroyNotify)
				    g_object_unref);

		g_signal_connect (action, "activate",
				  G_CALLBACK (module_files_activate_action),
				  module);

		// We want the default application to be the first action so
		// we construct a list of actions with the default at the
		// head. We add the actions to the item later.
		if (g_app_info_equal (application, default_application)) {
			actions = g_list_prepend (actions, action);
		} else {
			actions = g_list_append (actions, action);
		}
	}
	g_list_free (applications);

	// Now that we have the list of actions with the default application at
	// the head of the list, add the actions to the item.
	for (list = actions; list; list = list->next) {
		LbAction	*action = LB_ACTION (list->data);

		lb_item_add_action (item, action);
		g_object_unref (action);
	}
	g_list_free (actions);
}

static void
module_files_activate_action (LbAction      *action,
			      LbModuleFiles *module)
{
	GdkAppLaunchContext* context;
	GAppInfo  * application;
	LbItemFile* item;
	GList       list;

	item = LB_ITEM_FILE (lb_action_get_item (action));

	list.next = NULL;
	list.data = (gpointer) lb_item_file_get_uri (item);

	application = G_APP_INFO (lb_action_get_data (action));

	context = gdk_app_launch_context_new ();
	g_app_info_launch (application, &list, G_APP_LAUNCH_CONTEXT (context), NULL);
	g_object_unref (context);
}
