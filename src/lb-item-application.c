/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-item-application.c
 */

#include "config.h"

#include "lb-item-application.h"

#include <gio/gdesktopappinfo.h>
#include <gdk/gdk.h>

#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_ITEM_APPLICATION, LbItemApplicationPriv))

typedef struct _LbItemApplicationPriv LbItemApplicationPriv;
struct _LbItemApplicationPriv {
	gchar *desktop_file;
};

enum {
	PROP_0,
	PROP_DESKTOP_FILE
};


static void   item_application_finalize     (GObject            *object);
static void   item_application_set_property (GObject            *object,
					     guint               property_id,
					     const GValue       *value,
					     GParamSpec         *pspec);
static void   item_application_get_property (GObject            *object,
					     guint               property_id,
					     GValue             *value,
					     GParamSpec         *pspec);
static void    module_applications_activate_action (LbAction  *action);

G_DEFINE_TYPE (LbItemApplication, lb_item_application, LB_TYPE_ITEM);
static LbItemClass *parent_class = NULL;

static void
lb_item_application_class_init (LbItemApplicationClass *klass)
{
	GObjectClass  *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = item_application_finalize;
	object_class->set_property = item_application_set_property;
	object_class->get_property = item_application_get_property;

	g_object_class_install_property (object_class, 
					 PROP_DESKTOP_FILE,
					 g_param_spec_string ("desktop-file",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_type_class_add_private (object_class, sizeof (LbItemApplicationPriv));
}

static void
lb_item_application_init (LbItemApplication *application)
{
	LbItemApplicationPriv *priv;

	priv = GET_PRIV (application);

	priv->desktop_file = NULL;
	
	/* Set a fall-back icon. */
	g_object_set (application,
		      "icon-name", "gnome-fs-executable",
		      NULL);
}

static void
item_application_finalize (GObject *object)
{
	LbItemApplicationPriv *priv;

	priv = GET_PRIV (object);

	if (priv->desktop_file) {
		g_free (priv->desktop_file);
		priv->desktop_file = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
item_application_set_property (GObject      *object,
			       guint         property_id,
			       const GValue *value,
			       GParamSpec   *pspec)
{
	LbItemApplicationPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
	case PROP_DESKTOP_FILE:
		if (priv->desktop_file) {
			g_free (priv->desktop_file);
		} 
		priv->desktop_file = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
item_application_get_property (GObject    *object,
			       guint       property_id,
			       GValue     *value,
			       GParamSpec *pspec)
{
	LbItemApplicationPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
	case PROP_DESKTOP_FILE:
		g_value_set_string (value, priv->desktop_file);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
module_applications_activate_action (LbAction *action)
{
  GError              *error = NULL;
  GDesktopAppInfo     *info;
  const gchar         *item_path;
  GdkAppLaunchContext *context;

  item_path = lb_action_get_data (action);

  info = g_desktop_app_info_new_from_filename (item_path);

  if (!info)
  {
    g_printerr ("Unable to open desktop file %s for launcher\n",
        item_path);

    return;
  }

  context = gdk_app_launch_context_new ();
  if (!g_app_info_launch (G_APP_INFO (info), NULL, G_APP_LAUNCH_CONTEXT (context), &error))
  {
    g_printerr ("Unable to launch: '%s'\n", error->message);
    g_error_free (error);
  }
  g_object_unref (context);
}

void
lb_item_application_add_actions (LbItemApplication* item)
{
	const gchar *desktop_file;
	LbAction *action;

	g_return_if_fail (LB_IS_ITEM_APPLICATION (item));

	if (!lb_item_application_get_desktop_file (LB_ITEM_APPLICATION (item))) {
		return;
	}

	desktop_file = lb_item_application_get_desktop_file (LB_ITEM_APPLICATION (item));
	action = lb_action_new (LB_ACTION_LAUNCH);

	lb_action_set_data (action, g_strdup (desktop_file), g_free);

	g_signal_connect (action, "activate",
			  G_CALLBACK (module_applications_activate_action),
			  NULL);

	lb_item_add_action (LB_ITEM (item), action);

	g_object_unref (action);
}

const gchar *
lb_item_application_get_desktop_file (LbItemApplication *item)
{
	LbItemApplicationPriv *priv;
	
	g_return_val_if_fail (LB_IS_ITEM_APPLICATION (item), NULL);

	priv = GET_PRIV (item);

	return priv->desktop_file;
}

void 
lb_item_application_set_desktop_file (LbItemApplication *item,
				      const gchar       *path)
{
	LbItemApplicationPriv *priv;

	g_return_if_fail (LB_IS_ITEM_APPLICATION (item));
	g_return_if_fail (path != NULL);

	priv = GET_PRIV (item);

	g_free (priv->desktop_file);
	priv->desktop_file = g_strdup (path);

	g_object_notify (G_OBJECT (item), "desktop-file");
}

LbItem*
lb_item_application_new (gchar const* desktop_file)
{
  return g_object_new (LB_TYPE_ITEM_APPLICATION,
                       "desktop-file", desktop_file,
                       NULL);
}

