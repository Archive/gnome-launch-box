/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-frame.h
 */

#ifndef __LB_FRAME_H__
#define __LB_FRAME_H__

#include <gtk/gtkbin.h>


#define LB_TYPE_FRAME            (lb_frame_get_type ())
#define LB_FRAME(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_FRAME, LbFrame))
#define LB_FRAME_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_FRAME, LbFrameClass))
#define LB_IS_FRAME(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_FRAME))
#define LB_IS_FRAME_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_FRAME))
#define LB_FRAME_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_FRAME, LbFrameClass))


typedef struct _LbFrame      LbFrame;
typedef struct _LbFrameClass LbFrameClass;

struct _LbFrame {
	GtkBin       parent_instance;
};

struct _LbFrameClass {
	GtkBinClass  parent_class;
};


GType      lb_frame_get_type        (void) G_GNUC_CONST;
GtkWidget *lb_frame_new             (void);
void       lb_frame_set_fill        (LbFrame       *frame,
				     gboolean       fill);
void       lb_frame_set_fill_color  (LbFrame       *frame,
				     GdkColor      *color);
void       lb_frame_set_fill_alpha  (LbFrame       *frame,
				     guint16        alpha);
void       lb_frame_set_frame       (LbFrame       *frame,
				     gboolean       draw_frame);
void       lb_frame_set_frame_color (LbFrame       *frame,
				     GdkColor      *color);
void       lb_frame_set_frame_alpha (LbFrame       *frame,
				     guint16        alpha);

#endif /* __LB_FRAME_H__ */

