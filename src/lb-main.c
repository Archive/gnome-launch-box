/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <config.h>
#include <string.h>
#include <glib/gi18n.h>
#include <gtk/gtkmain.h>
#include <gconf/gconf-client.h>

#include "bacon-message-connection.h"
#include "lb-window.h"
#include "lb-module-manager.h"

#include "tomboykeybinder.h"

#define COMMAND_SHOW_WINDOW "ShowWindow"

static LbModuleManager *manager = NULL;
static GtkWidget *window = NULL;

/* Command-line options */
static gboolean no_window = FALSE;

static GOptionEntry cmd_args[] = {

	{ "no-default-window", 'n', 0, G_OPTION_ARG_NONE,
	  &no_window, "Do not show the initial window", NULL},
};


static void
update_composited_window (GdkScreen *screen)
{
	gboolean composited;

	composited = gdk_screen_is_composited (screen);

	if (window)
		g_object_unref (window);
	if (!manager)
		manager = g_object_new (LB_TYPE_MODULE_MANAGER, NULL);

	window = lb_window_new (manager, composited);
	gtk_window_set_default_size (GTK_WINDOW (window), 300, -1);
}

static void
show_window (void)
{
	if (!manager)
		update_composited_window (gdk_screen_get_default ());

	lb_window_present (LB_WINDOW (window));
}

static void
message_received_cb (const gchar *message, gpointer unused)
{
	if (strcmp (message, COMMAND_SHOW_WINDOW) != 0) {
		return;
	}

	show_window ();
}

static void
key_handler_func (char *keystring, gpointer user_data)
{
	show_window ();
}

static void
composited_changed_cb (GdkScreen *screen, gpointer user_data)
{
	update_composited_window (screen);
}

static void
key_handler_bind (const char *keystring)
{
	static gchar *old = NULL;

	if (old) {
		tomboy_keybinder_unbind (old, key_handler_func);
		g_free (old);
	}
		
	old = g_strdup (keystring);

	if (keystring) {
		tomboy_keybinder_bind (keystring,
				       key_handler_func,
				       NULL);
	} else {
		tomboy_keybinder_bind ("<Alt>space",
				       key_handler_func,
				       NULL);
	}
}

int
main (int argc, char **argv)
{
	BaconMessageConnection *message_conn;
	GError                 *error = NULL;
	GConfClient            *gconf_client;
	gchar                  *key;

	bindtextdomain (GETTEXT_PACKAGE, GNOMELOCALEDIR);
        bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);

	if (!gtk_init_with_args (&argc,
			 &argv,
			 _("- launch program for GNOME"),
			 cmd_args,
			 GETTEXT_PACKAGE,
			 &error)) {
		g_printerr ("gnome-launch-box: %s\n", error->message);
		g_error_free (error);
		return 1;
	}
	
	message_conn = bacon_message_connection_new ("gnome-launch-box");
	if (!bacon_message_connection_get_is_server (message_conn)) {
		bacon_message_connection_send (message_conn, 
					       COMMAND_SHOW_WINDOW);
		return 0;
	}

	bacon_message_connection_set_callback (
		message_conn,
		(BaconMessageReceivedFunc) message_received_cb,
		NULL);

	tomboy_keybinder_init ();

	gconf_client = gconf_client_get_default ();
	
	key = gconf_client_get_string (gconf_client,
				       "/apps/gnome-launch-box/bindings/activate",
				       NULL);

	key_handler_bind (key);
	g_free (key);

	g_signal_connect (G_OBJECT (gdk_screen_get_default ()), "composited-changed",
			  G_CALLBACK (composited_changed_cb), NULL);

	/* FIXME: Add a notify handler on that key and update the binding when
	 * it changes.
	 */

	if (!no_window) {
		show_window ();
	}

	gtk_main ();

	if (manager) {
		g_object_unref (manager);
	}

	bacon_message_connection_free (message_conn);

	g_object_unref (gconf_client);
				 
	return 0;
}
