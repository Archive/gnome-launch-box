/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module-manager.c
 */

#include "config.h"

#include <string.h>

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "lb-module-bookmarks.h"
#include "lb-module-evolution.h"
#include "lb-module-manager.h"
#include "lb-module-recent.h"
#include "lb-module-files.h"
#include "lb-utils.h"

#ifdef HAVE_TRACKER
#include "lb-module-tracker.h"
#else
#include "lb-module-applications.h"
#endif

static void    lb_module_manager_finalize     (GObject              *object);
static void    lb_module_manager_set_property (GObject              *object,
					       guint                 property_id,
					       const GValue         *value,
					       GParamSpec           *pspec);
static void    lb_module_manager_get_property (GObject              *object,
					       guint                 property_id,
					       GValue               *value,
					       GParamSpec           *pspec);

G_DEFINE_TYPE (LbModuleManager, lb_module_manager, G_TYPE_OBJECT);
static LbObjectClass *parent_class = NULL;

static void
lb_module_manager_class_init (LbModuleManagerClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = lb_module_manager_finalize;
	object_class->set_property = lb_module_manager_set_property;
	object_class->get_property = lb_module_manager_get_property;

#if 0
	g_object_class_install_property (object_class, PROP_NAME,
					 g_param_spec_string ("name",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

#endif
}

static void
lb_module_manager_init (LbModuleManager *manager)
{
#ifdef HAVE_TRACKER
	manager->modules = g_list_append (manager->modules,
					  g_object_new (LB_TYPE_MODULE_TRACKER,
							NULL));
#else
	manager->modules = g_list_append (manager->modules,
					  g_object_new (LB_TYPE_MODULE_APPLICATIONS,
							NULL));
#endif
	manager->modules = g_list_append (manager->modules,
					  g_object_new (LB_TYPE_MODULE_RECENT,
							NULL));
	manager->modules = g_list_append (manager->modules,
					  g_object_new (LB_TYPE_MODULE_BOOKMARKS,
							NULL));
	manager->modules = g_list_append (manager->modules,
					  g_object_new (LB_TYPE_MODULE_EVOLUTION,
							NULL));
	manager->modules = g_list_append (manager->modules,
					  g_object_new (LB_TYPE_MODULE_FILES,
							NULL));
}

static void
lb_module_manager_finalize (GObject *object)
{
	LbModuleManager *manager = LB_MODULE_MANAGER (object);

	if (manager->modules) {
		g_list_foreach (manager->modules, (GFunc) g_object_unref, NULL);
		g_list_free (manager->modules);
		manager->modules = NULL;
	}

#if 0
	if (manager->name) {
		g_free (manager->name);
		manager->name = NULL;
	}
#endif

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
lb_module_manager_set_property (GObject      *object,
				guint         property_id,
				const GValue *value,
				GParamSpec   *pspec)
{
	/*LbModuleManager *manager = LB_MODULE_MANAGER (object);*/

	switch (property_id) {
#if 0
	case PROP_NAME:
		if (manager->name)
			g_free (manager->name);
		manager->name = g_value_dup_string (value);
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
lb_module_manager_get_property (GObject    *object,
				guint       property_id,
				GValue     *value,
				GParamSpec *pspec)
{
	/*LbModuleManager *manager = LB_MODULE_MANAGER (object);*/

	switch (property_id) {
#if 0
	case PROP_NAME:
		g_value_set_string (value, manager->name);
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

GList *
lb_module_manager_query (LbModuleManager *manager,
			 const gchar     *match)
{
	GList *l;
	GList *result = NULL;
	gchar *slug;

	g_return_val_if_fail (LB_IS_MODULE_MANAGER (manager), NULL);

	slug = lb_string_to_slug (match);

	for (l = manager->modules; l; l = l->next) {
		LbModule *module = l->data;

		if (module->enabled) {
			GList *matched;

			if (LB_MODULE_SLUGS_SUPPORTED (module)) {
				matched = lb_module_query (module, slug);
			} else {
				matched = lb_module_query (module, match);
			}

			result = g_list_concat (result, matched);
		}
	}

	g_free (slug);
	return result;
}

void
lb_module_manager_set_actions (LbModuleManager *manager,
			       LbItem          *item)
{
	GList *l;

	g_return_if_fail (LB_IS_MODULE_MANAGER (manager));
	g_return_if_fail (LB_IS_ITEM (item));

	for (l = manager->modules; l; l = l->next) {
		LbModule *module = l->data;

		if (module->enabled) {
			lb_module_add_actions (module, item);
		}
	}

	return;
}
