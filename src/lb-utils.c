/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-utils.c
 */

#include <config.h>

#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtkicontheme.h>

#include "lb-utils.h"

static GtkIconTheme          *icon_theme = NULL;

static void
ensure_icon_theme (void)
{
	if (icon_theme) {
		return;
	}

	icon_theme = gtk_icon_theme_get_default ();
}

gchar*
lb_string_to_slug (const gchar *str)
{
	gchar		*slug, *head;
	const gchar	*next;
	gunichar	 curr;

	g_return_val_if_fail (str, NULL);

	slug = g_new (gchar, strlen (str) + 1);

	for (head = slug; *str; str = next) {
		next = g_utf8_next_char (str);
		curr = g_utf8_get_char (str);

		if (g_unichar_isalnum (curr)) {
			memcpy (head, str, next - str);
			head += (next - str);
		}
	}

	*head = '\0';

	return slug;
}

static inline const gchar *
lb_slug_skip_noise (const gchar *str,
		    gunichar    *chr)
{
	gunichar c;

	while ('\0' != (c = g_utf8_get_char (str)) && !g_unichar_isalnum (c)) {
		str = g_utf8_next_char (str);
	}

	if (chr)
		*chr = c;

	return str;
}

static gboolean
lb_string_find_slug (const gchar *str,
		     const gchar *slug,
		     size_t      *first,	 
		     size_t      *last)
{
	const gchar *head, *tail;
	const gchar *match;
	gunichar t, m;

	head = NULL;
	tail = str;

	match = lb_slug_skip_noise (slug, &m);

	while (*tail && *match) {
		tail = lb_slug_skip_noise (tail, &t);

		if (t == m) {
			if (NULL == head)
				head = tail;

			match = g_utf8_next_char (match);
			match = lb_slug_skip_noise (match, &m);
		} else {
			match = lb_slug_skip_noise (slug, &m);
			head = NULL;
		}

		tail = g_utf8_next_char (tail);
	}

	if (!head || *match)
		return FALSE;
	if (first)
		*first = head - str;
	if (last)
		*last = tail - str;

	return TRUE;
}

gboolean
lb_string_has_substring (const gchar *str,
			 const gchar *substr)
{
	gchar    *norm_str, *norm_substr, *tmp;
	gboolean  retval;

	/* Case-fold and normalize */
	tmp = g_utf8_normalize (str, -1, G_NORMALIZE_ALL_COMPOSE);
	norm_str = g_utf8_casefold (tmp, -1);
	g_free (tmp);

	g_return_val_if_fail (norm_str, FALSE);

	tmp = g_utf8_normalize (substr, -1, G_NORMALIZE_ALL_COMPOSE);
	norm_substr = g_utf8_casefold (tmp, -1);
	g_free (tmp);

	g_return_val_if_fail (norm_substr, FALSE);

	retval = lb_string_find_slug (norm_str, norm_substr, NULL, NULL);

	g_free (norm_str);
	g_free (norm_substr);

	return retval;
}

gchar *
lb_string_markup_substring (const gchar *str,
			    const gchar *substr,
			    const gchar *tag)
{
	gchar 	*norm_str, *norm_substr;
	gchar 	*fold_str, *fold_substr;
	gchar 	*markup = NULL;
	size_t	 head, tail;

	g_return_val_if_fail (substr && *substr, NULL);

	/* Case-fold and normalize */
	norm_str = g_utf8_normalize (str, -1, G_NORMALIZE_ALL_COMPOSE);
	fold_str = g_utf8_casefold (norm_str, -1);

	norm_substr = g_utf8_normalize (substr, -1, G_NORMALIZE_ALL_COMPOSE);
	fold_substr = g_utf8_casefold (norm_substr, -1);

	if (lb_string_find_slug (fold_str, fold_substr, &head, &tail)) {
		gchar *prefix, *infix, *suffix;
		size_t length;

		length = strlen (norm_str);

		prefix = g_markup_escape_text (norm_str, head);
		 infix = g_markup_escape_text (norm_str + head, tail - head);
		suffix = g_markup_escape_text (norm_str + tail, length - tail);

		markup = g_strconcat (prefix, "<", tag, ">",
				      infix, "</", tag, ">",
				      suffix, NULL);

		g_free (suffix);
		g_free ( infix);
		g_free (prefix);
	}

	g_free (fold_substr);
	g_free (norm_substr);

	g_free (fold_str);
	g_free (norm_str);

	return markup;
}

gchar *
lb_get_icon_name_for_uri (const gchar *uri)
{
  GFileInfo* info;
  GError   * error = NULL;
  GFile    * file = g_file_new_for_uri (uri);
  gchar    * result = NULL;

  info = g_file_query_info (file, G_FILE_ATTRIBUTE_STANDARD_ICON,
                            G_FILE_QUERY_INFO_NONE, NULL,
                            &error);

  if (!info)
    {
      g_warning ("error reading file info for %s: %s",
                  uri, error->message);
      g_error_free (error);
    }
  else
    {
      GIcon* icon = g_file_info_get_icon (info);

      if (G_IS_THEMED_ICON (icon))
        {
          gchar const * const * names = g_themed_icon_get_names (G_THEMED_ICON (icon));

          result = names ? g_strdup (names[g_strv_length ((gchar**)names) - 1]) : NULL;

          /* FIXME: return a GIcon to be more flexible */
        }
      else if (G_IS_ICON (icon))
        {
          g_printerr ("missing implementation for icon type %s",
                      G_OBJECT_TYPE_NAME (icon));
        }

      g_object_unref (info);
    }

  g_object_unref (file);

  return result;
}

gchar *
lb_get_icon_name_for_mime_type (const gchar *mime_type)
{
	gchar *without_slashes, *icon_name;
	gchar *p;
  
	if (mime_type == NULL) {
		return NULL;
	}

	without_slashes = g_strdup (mime_type);

	p = without_slashes;
	while ((p = strchr (p, '/')) != NULL) {
		*p = '-';
	}

	icon_name = g_strconcat ("gnome-mime-", without_slashes, NULL);
	g_free (without_slashes);
  
	return icon_name;
}

GdkPixbuf *
lb_get_pixbuf_from_icon_name (const gchar *icon_name)
{
	GdkPixbuf *pixbuf;

	/* Return null if we get a null or empty string. */
        if (icon_name == NULL || icon_name[0] == '\0') {
                return NULL;
	}

	/* Handle absolute paths. */
	if (icon_name[0] == '/') {
		return gdk_pixbuf_new_from_file_at_scale (
			icon_name,
			LB_ICON_SIZE,
			LB_ICON_SIZE,
			TRUE,
			NULL);
	}

	ensure_icon_theme ();

	/* We use 48 here since most mime type icons look a lot better at that
	 * size.
	 */
	pixbuf = gtk_icon_theme_load_icon (icon_theme,
					   icon_name,
					   48,
					   0,
					   NULL);

	/* Try again, but remove the extension so that old crufty things like
	 * "xpdf.xpm" works.
	 */
	if (!pixbuf) {
		gchar *tmp, *dot;
		
		tmp = g_strdup (icon_name);
		dot = strrchr (tmp, '.');
		if (dot) {
			*dot = '\0';

			pixbuf = gtk_icon_theme_load_icon (icon_theme,
							   tmp,
							   48,
							   0,
							   NULL);
			g_free (tmp);
		}
	}

	/* Note: very ugly, but the text mime type isn't detected properly for
	 * some reason... We might have to deal with mime type inheritance
	 * (remove the last part after slash...?).
	 */
	if (!pixbuf) {
		if (strcmp (icon_name, "gnome-mime-text-plain") == 0) {
			pixbuf = gtk_icon_theme_load_icon (icon_theme,
							   "gnome-mime-text",
							   48,
							   0,
							   NULL);
		}
	}
	
	return pixbuf;
}

gchar *
lb_get_firefox_path (void)
{
	gchar     *profile_path;
	GKeyFile  *key_file;
	GError    *error = NULL;
	gchar    **groups;
	gchar     *default_group = NULL;
	gchar     *tmp, *path = NULL;
	int        i;

	profile_path = g_build_filename (g_get_home_dir (),
					 ".mozilla/firefox/profiles.ini",
					 NULL);

	key_file = g_key_file_new ();

	if (!g_key_file_load_from_file (key_file, profile_path, 0,
					&error)) {
		g_free (profile_path);

		g_warning ("Could not load profiles.ini: %s\n", error->message);
		return NULL;
	}

	/* Try to find the default group */
	groups = g_key_file_get_groups (key_file, NULL);
	for (i = 0; groups[i] != NULL; i++) {
		if (!g_str_has_prefix (groups[i], "Profile")) {
			continue;
		}

		if (!default_group) {
			default_group = g_strdup (groups[i]);
		} else {
			tmp = g_key_file_get_string (key_file, groups[i],
						     "Default", NULL);
			
			if (tmp && strcmp (tmp, "1") == 0) {
				g_free (default_group);
				default_group = g_strdup (groups[i]);
			}

			g_free (tmp);
		}
	}

	g_strfreev (groups);
	g_free (profile_path);

	if (!default_group) {
		g_key_file_free (key_file);
		return NULL;
	}

	tmp = g_key_file_get_string (key_file, default_group,
				     "Path", NULL);

	if (tmp) {
		path = g_build_filename (g_get_home_dir (),
					 ".mozilla/firefox",
					 tmp, NULL);
		g_free (tmp);
	}

	return path;
}

gboolean
lb_file_lock (gint fd)
{
	gint i;

	/* Attempt to lock the file 5 times,
	 * waiting a random interval (< 1 second)
	 * in between attempts.
	 * We should really be doing asynchronous
	 * locking, but requires substantially larger
	 * changes.
	 */

	lseek (fd, 0, SEEK_SET);

	for (i = 0; i < 5; i++) {
		gint rand_interval;

		if (lockf (fd, F_TLOCK, 0) == 0) {
			return TRUE;
		}

		rand_interval = 1 + (gint) (10.0 * rand () / (RAND_MAX + 1.0));

		g_usleep (100000 * rand_interval);
	}

	return FALSE;
}

gboolean
lb_file_unlock (gint fd)
{
	lseek (fd, 0, SEEK_SET);

	return (lockf (fd, F_ULOCK, 0) == 0) ? TRUE : FALSE;
}
