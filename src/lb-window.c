/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-window.c
 */

#include <config.h>

#include <string.h>
#include <glib/gi18n.h>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>
#include <gdk/gdkx.h>

#include "lb-window.h"
#include "lb-item.h"
#include "lb-icon-box.h"
#include "lb-utils.h"
#include "lb-action.h"

#define SEARCH_TIMEOUT_BASE    50
#define SEARCH_TIMEOUT_MAX    700
#define SEARCH_TIMEOUT_STEP   200

#define SEARCH_RESET_TIMEOUT 3000

typedef enum {
	LB_WINDOW_FOCUS_ITEM,
	LB_WINDOW_FOCUS_ACTION
} LbWindowFocus;

enum {
	COL_OBJECT,
	COL_PIXBUF,
	COL_NAME,
	NUM_COLS
};

enum {
	PROP_0,
	PROP_MANAGER,
	PROP_TRANS
};

typedef struct {
	LbModuleManager    *manager;

	LbWindowFocus       focus;

	GString            *search_string;
	gchar              *last_item_search_str;

	LbItem             *current_item;
	LbAction           *current_action;

	GtkWidget          *frame;
	
	GtkWidget          *instruction;
	GtkWidget          *instr_label;
	
	GtkWidget          *result_sw;
	GtkWidget          *result_hbox;

	GtkWidget          *item_icon_box;
	GtkWidget          *action_icon_box;

	GtkWidget          *result_treeview;

	GdkPixbuf          *empty_pixbuf;
	GdkPixbuf          *unknown_pixbuf;

	guint               search_timeout_id;

	gboolean            reset_search;
	guint               search_reset_timeout_id;

	gboolean            delayed_activation;
	gboolean            can_rgba;

	gboolean            transparent;

} LbWindowPriv;


static void     lb_window_finalize             (GObject           *object);
static void     lb_window_set_property         (GObject           *object,
						guint              property_id,
						const GValue      *value,
						GParamSpec        *pspec);
static void     lb_window_get_property         (GObject           *object,
						guint              property_id,
						GValue            *value,
						GParamSpec        *pspec);
static void     window_realize_cb              (GtkWidget         *widget,
						LbWindow          *window);
static void     window_screen_changed_cb       (GtkWidget         *widget,
						GdkScreen         *old_screen,
						gpointer           data);
static gboolean window_expose_cb               (GtkWidget         *widget,
						GdkEventExpose    *event,
						gpointer           data);
static void     window_set_focus               (LbWindow          *window,
						LbWindowFocus      focus);
static void     window_set_item                (LbWindow          *window,
						LbItem            *item,
						const gchar       *match);
static void     window_set_action              (LbWindow          *window,
						LbAction          *action,
						const gchar       *match);
static void     window_queue_search            (LbWindow          *window);
static gboolean window_search_timeout_cb       (LbWindow          *window);
static gboolean window_search_reset_timeout_cb (LbWindow          *window);
static void     window_perform_search          (LbWindow          *window,
						const gchar       *str);
static void     window_show_search_results     (LbWindow          *window);
static void     window_hide_search_results     (LbWindow          *window);
static void     window_result_row_selected_cb  (GtkTreeSelection  *selection,
						LbWindow          *window);
static void     window_reset_result            (LbWindow          *window);
static gboolean window_key_press_event_cb      (GtkWidget         *widget,
						GdkEventKey       *event,
						LbWindow          *window);
static gboolean window_get_is_visible          (LbWindow          *window);
static void     window_set_position            (LbWindow          *window);
static void     window_set_colormap            (LbWindow          *window);
static void     window_start_reset_timeout     (LbWindow          *window);

static void     window_set_transparent         (LbWindow          *window,
						gboolean           transparent);

G_DEFINE_TYPE (LbWindow, lb_window, GTK_TYPE_WINDOW);
#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_WINDOW, LbWindowPriv))


static void
lb_window_class_init (LbWindowClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize     = lb_window_finalize;
	object_class->set_property = lb_window_set_property;
	object_class->get_property = lb_window_get_property;

	g_object_class_install_property (object_class, PROP_MANAGER,
					 g_param_spec_object ("manager",
							      NULL, NULL,
							      LB_TYPE_MODULE_MANAGER,
							      G_PARAM_READWRITE | 
							      G_PARAM_CONSTRUCT_ONLY));

	g_object_class_install_property (object_class, PROP_TRANS,
					 g_param_spec_boolean ("transparent",
							       NULL, NULL,
							       FALSE,
							       G_PARAM_READWRITE | 
							       G_PARAM_CONSTRUCT_ONLY));
			
	g_type_class_add_private (klass, sizeof (LbWindowPriv));
}

static void
lb_window_init (LbWindow *window)
{
	LbWindowPriv      *priv = GET_PRIV (window);
	GdkColor           color;
	GtkWidget         *vbox;
	GtkWidget         *label;
	GtkListStore      *store;
	GtkTreeViewColumn *column;
	GtkCellRenderer   *cell;
	GtkTreeSelection  *selection;
	gint               xpad, ypad;
	gchar             *tmp;
	
	gtk_widget_set_app_paintable (GTK_WIDGET (window), TRUE);
	gtk_window_set_keep_above (GTK_WINDOW (window), TRUE);

	window_set_colormap (window);

	priv->focus = LB_WINDOW_FOCUS_ITEM;

	priv->search_timeout_id = 0;
	priv->search_reset_timeout_id = 0;
	priv->reset_search = FALSE;
	priv->delayed_activation = FALSE;

	priv->search_string = g_string_new (NULL);

	priv->empty_pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
					     TRUE,
                                             8,
                                             LB_ICON_SIZE,
					     LB_ICON_SIZE);
	gdk_pixbuf_fill (priv->empty_pixbuf, 0x00000000);

	priv->unknown_pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB,
					       TRUE,
					       8,
					       LB_ICON_SIZE,
					       LB_ICON_SIZE);
	gdk_pixbuf_fill (priv->unknown_pixbuf, 0x00000000);

	priv->frame = lb_frame_new ();
	
	color.red = color.green = color.blue = 0;
	
	lb_frame_set_fill_color (LB_FRAME (priv->frame), &color);
	lb_frame_set_fill_alpha (LB_FRAME (priv->frame), G_MAXUINT16 * 0.7);
	
	gtk_container_add (GTK_CONTAINER (window), priv->frame);
	gtk_widget_show (priv->frame);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 18);
	gtk_widget_show (vbox);

	gtk_container_add (GTK_CONTAINER (priv->frame), vbox);

	priv->result_hbox = gtk_hbox_new (FALSE, 12);
	gtk_widget_show (priv->result_hbox);
	gtk_box_pack_start (GTK_BOX (vbox),
			    priv->result_hbox, FALSE, FALSE, 0);

	priv->item_icon_box = lb_icon_box_new (NULL, priv->empty_pixbuf);
	g_object_set (priv->item_icon_box, "focused", TRUE, NULL);
	gtk_widget_show (priv->item_icon_box);
	gtk_box_pack_start (GTK_BOX (priv->result_hbox), priv->item_icon_box,
			    TRUE, TRUE, 0);

	priv->action_icon_box = lb_icon_box_new (NULL, priv->empty_pixbuf);
	g_object_set (priv->action_icon_box, "focused", FALSE, NULL);
	gtk_widget_show (priv->action_icon_box);
	gtk_box_pack_start (GTK_BOX (priv->result_hbox), priv->action_icon_box,
			    TRUE, TRUE, 0);

	priv->instruction = gtk_alignment_new (0.5, 0.5, 1, 1);
	gtk_alignment_set_padding (GTK_ALIGNMENT (priv->instruction),
				   8, 0, 0, 0);
	tmp = g_strdup_printf ("<i>%s</i>", _("Type to start searching"));
	label = gtk_label_new (tmp);
	g_free (tmp);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_widget_show (label);
	gtk_widget_show (priv->instruction);
	gtk_container_add (GTK_CONTAINER (priv->instruction), label);
	gtk_box_pack_start (GTK_BOX (vbox), priv->instruction, FALSE, FALSE, 0);
	priv->instr_label = label;
	
	priv->result_sw = gtk_scrolled_window_new (NULL, NULL);

	/* Note: kind of arbitrary value here, seems to show exactly 4 lines, at
	 * least for clearlooks.
	 */
	gtk_widget_set_size_request (priv->result_sw, -1, (LB_ICON_SIZE + 4) * 4 + 2);

	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (priv->result_sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);

	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (priv->result_sw),
					     GTK_SHADOW_IN);
	
	gtk_box_pack_start (GTK_BOX (vbox), priv->result_sw, TRUE, TRUE, 0);

	priv->result_treeview = gtk_tree_view_new ();
	gtk_tree_view_set_enable_search (GTK_TREE_VIEW (priv->result_treeview), FALSE);
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (priv->result_treeview), FALSE);
	gtk_widget_show (priv->result_treeview);

	gtk_container_add (GTK_CONTAINER (priv->result_sw), priv->result_treeview);

	store = gtk_list_store_new (NUM_COLS,
				    LB_TYPE_OBJECT,
				    GDK_TYPE_PIXBUF,
				    G_TYPE_STRING);

	gtk_tree_view_set_model (GTK_TREE_VIEW (priv->result_treeview),
				 GTK_TREE_MODEL (store));

	column = gtk_tree_view_column_new ();

	cell = gtk_cell_renderer_pixbuf_new ();
	
	gtk_tree_view_column_pack_start (column, cell, FALSE);
	
	color.red = color.green = color.blue = G_MAXUINT16;
	
	g_object_set (cell,
		      "cell-background-gdk", &color,
		      "cell-background-set", TRUE,
		      NULL);

	g_object_get (cell,
		      "xpad", &xpad,
		      "ypad", &ypad,
		      NULL);

	gtk_cell_renderer_set_fixed_size (cell,
					  -1,
					  LB_ICON_SIZE - ypad);
	
	gtk_tree_view_column_add_attribute (column,
					    cell,
					    "pixbuf",
					    COL_PIXBUF);
	
	cell = gtk_cell_renderer_text_new ();
	g_object_set (cell, "ellipsize", PANGO_ELLIPSIZE_END, NULL);
	gtk_tree_view_column_pack_start (column, cell, TRUE);

	gtk_tree_view_column_add_attribute (column,
					    cell,
					    "markup",
					    COL_NAME);

	gtk_tree_view_append_column (GTK_TREE_VIEW (priv->result_treeview), column);

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->result_treeview));
	g_signal_connect (selection, "changed",
			  G_CALLBACK (window_result_row_selected_cb),
			  window);

	g_signal_connect (window, "key-press-event",
			  G_CALLBACK (window_key_press_event_cb),
			  window);

	g_signal_connect (window, "realize",
			  G_CALLBACK (window_realize_cb),
			  window);

	g_signal_connect (window, "screen-changed",
			  G_CALLBACK (window_screen_changed_cb),
			  window);

	g_signal_connect (window, "expose-event",
			  G_CALLBACK (window_expose_cb),
			  window);
}

static void
lb_window_finalize (GObject *object)
{
	LbWindow     *window = LB_WINDOW (object);
	LbWindowPriv *priv   = GET_PRIV (window);

	if (priv->search_timeout_id) {
		g_source_remove (priv->search_timeout_id);
	}

	if (priv->search_reset_timeout_id) {
		g_source_remove (priv->search_reset_timeout_id);
	}

	if (priv->current_item) {
		g_object_unref (priv->current_item);
	}
	if (priv->current_action) {
		g_object_unref (priv->current_action);
	}

	g_string_free (priv->search_string, TRUE);

	g_free (priv->last_item_search_str);

	g_object_unref (priv->unknown_pixbuf);
	g_object_unref (priv->empty_pixbuf);

	G_OBJECT_CLASS (lb_window_parent_class)->finalize (object);
}

static void
lb_window_set_property (GObject      *object,
                        guint         property_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
	LbWindow     *window = LB_WINDOW (object);
	LbWindowPriv *priv   = GET_PRIV (window);

	switch (property_id) {
	case PROP_MANAGER:
		priv->manager = LB_MODULE_MANAGER (g_value_dup_object (value));
		break;
	case PROP_TRANS:
		window_set_transparent (window, g_value_get_boolean (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
lb_window_get_property (GObject    *object,
                        guint       property_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
	LbWindow     *window = LB_WINDOW (object);
	LbWindowPriv *priv   = GET_PRIV (window);

	switch (property_id) {
	case PROP_MANAGER:
		g_value_set_object (value, priv->manager);
		break;
	case PROP_TRANS:
		g_value_set_boolean (value, priv->transparent);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
window_realize_cb (GtkWidget *widget,
		   LbWindow  *window)
{
	window_set_position (LB_WINDOW (window));
}

static void
window_screen_changed_cb (GtkWidget *widget,
			  GdkScreen *old_screen,
			  gpointer   data)
{
	window_set_colormap (LB_WINDOW (widget));
}

static gboolean
window_expose_cb (GtkWidget      *widget,
		  GdkEventExpose *event,
		  gpointer        data)
{
	LbWindow     *window = LB_WINDOW (widget);
	LbWindowPriv *priv   = GET_PRIV (window);
	cairo_t      *cr;
	
	if (!priv->transparent) {
		gtk_paint_box (widget->style,
			       widget->window,
			       GTK_STATE_NORMAL, 
			       GTK_SHADOW_IN,
			       &event->area,
			       widget,
			       "base",
			       0, 0, -1, -1);
		return FALSE;
	}
	
	cr = gdk_cairo_create (widget->window);	

	cairo_rectangle (cr, event->area.x,
			     event->area.y,
			     event->area.width,
			     event->area.height);
        cairo_clip (cr);

	/* fully transparent */
	cairo_set_source_rgba (cr, 1.0, 1.0, 1.0, 0.0);

	cairo_set_operator (cr, CAIRO_OPERATOR_SOURCE);
	cairo_paint (cr);
		
	cairo_destroy (cr);
	
	return FALSE;
}

static void
window_set_transparent (LbWindow *window,
			gboolean  transparent)
{
	LbWindowPriv      *priv = GET_PRIV (window);
	GtkScrolledWindow *sw;
	
	if (priv->transparent == transparent) {
		return;
	}

	if (transparent && !priv->can_rgba) {
		g_warning ("Cannot paint window transparent (no rgba)");
		return;
	}

	priv->transparent = transparent;

	g_object_set (priv->action_icon_box, "transparent", transparent, NULL);
	g_object_set (priv->item_icon_box, "transparent", transparent, NULL);
	
	sw = GTK_SCROLLED_WINDOW (priv->result_sw);
	
	if (transparent) {
		LbFrame  *frame = LB_FRAME (priv->frame);

		lb_frame_set_fill (frame, TRUE);

		gtk_widget_modify_fg (priv->instr_label,
				      GTK_STATE_NORMAL,
				      &priv->instr_label->style->white);
		
		gtk_scrolled_window_set_shadow_type (sw, GTK_SHADOW_NONE);
		
	} else {
		gtk_widget_modify_fg (priv->instr_label,
				      GTK_STATE_NORMAL,
				      &priv->instr_label->style->fg[GTK_STATE_NORMAL]);

		lb_frame_set_fill (LB_FRAME (priv->frame), FALSE);

		gtk_scrolled_window_set_shadow_type (sw, GTK_SHADOW_IN);
	}
	
	if (GTK_WIDGET_DRAWABLE (window)) {
		gtk_widget_queue_draw (GTK_WIDGET (window));
	}     
	
}

GtkWidget *
lb_window_new (LbModuleManager *manager,
	       gboolean         transparent)
{
	GtkWidget *window;

	window = g_object_new (LB_TYPE_WINDOW,
			       "type", GTK_WINDOW_POPUP,
			       "manager", manager,
			       "transparent", transparent,
			       NULL);
	return window;
}

static void
window_set_focus (LbWindow      *window,
		  LbWindowFocus  focus)
{
	LbWindowPriv *priv = GET_PRIV (window);
	GtkListStore *store;
	
	if (priv->focus == focus) {
		return;
	}

	if (focus == LB_WINDOW_FOCUS_ACTION) {
		if (!priv->current_action) {
			return;
		}
	}

	store = GTK_LIST_STORE (gtk_tree_view_get_model (
					GTK_TREE_VIEW (priv->result_treeview)));
	gtk_list_store_clear (store);

	if (focus == LB_WINDOW_FOCUS_ACTION) {
		/* Save the search string so that we can reuse it when tabbing
		 * back to the icon box.
		 */
		g_free (priv->last_item_search_str);
		priv->last_item_search_str = g_strdup (priv->search_string->str);
		g_string_truncate (priv->search_string, 0);
	} else {
		/* Reset the search string. */
		g_string_truncate (priv->search_string, 0);

		if (priv->last_item_search_str) {
			g_string_append (priv->search_string, priv->last_item_search_str);
		}
	}

	g_object_set (priv->item_icon_box,
		      "focused",
		      focus == LB_WINDOW_FOCUS_ITEM,
		      NULL);

	g_object_set (priv->action_icon_box,
		      "focused",
		      focus == LB_WINDOW_FOCUS_ACTION,
		      NULL);

	priv->focus = focus;

	window_queue_search (window);
}

static void
window_set_item (LbWindow    *window,
		 LbItem      *item,
		 const gchar *match)
{
	LbWindowPriv *priv = GET_PRIV (window);

	if (priv->current_item == item) {
		return;
	}

	if (priv->current_item) {
		g_object_unref (priv->current_item);
		priv->current_item = NULL;
	}

	if (item) {
		GdkPixbuf *pixbuf;
		gchar     *markup = NULL;
		GList     *actions;

		priv->current_item = g_object_ref (item);

		g_object_get (item, "pixbuf", &pixbuf, NULL);

		if (match) {
			markup = lb_string_markup_substring (LB_OBJECT (item)->name,
							     match, "u");
		}

		if (!markup) {
			markup = g_markup_escape_text (LB_OBJECT (item)->name,
						       -1);
		}

		g_object_set (priv->item_icon_box,
			      "caption", markup,
			      "pixbuf",  pixbuf ? pixbuf : priv->unknown_pixbuf,
			      NULL);

		if (pixbuf) {
			g_object_unref (pixbuf);
		}

		g_free (markup);

		actions = lb_item_get_actions (item, NULL);

		if (!actions) {
			lb_module_manager_set_actions (priv->manager, item);
			actions = lb_item_get_actions (item, NULL);
		}

		if (actions) {
			window_set_action (window, actions->data, NULL);

			g_list_free (actions);
		} else {
			window_set_action (window, NULL, NULL);
		}
	} else {
		g_object_set (priv->item_icon_box,
			      "caption", NULL,
			      "pixbuf",  priv->empty_pixbuf,
			      NULL);
		window_set_action (window, NULL, NULL);
	}
}

static void
window_set_action (LbWindow    *window,
		   LbAction    *action,
		   const gchar *match)
{
	LbWindowPriv *priv = GET_PRIV (window);

	if (action && priv->delayed_activation) {
		/* If the user hit enter before getting any
		 * hits, we just activate the first hit that is
		 * found.
		 */
		if (priv->search_timeout_id) {
			g_source_remove (priv->search_timeout_id);
			priv->search_timeout_id = 0;
		}

		lb_action_activate (action);

		window_hide_search_results (window);
		gtk_widget_hide (GTK_WIDGET (window));

		priv->delayed_activation = FALSE;

		window_reset_result (window);

		return;
	}

	if (priv->current_action == action) {
		return;
	}

	if (priv->current_action) {
		g_object_unref (priv->current_action);
		priv->current_action = NULL;
	}

	if (action) {
		GdkPixbuf *pixbuf;
		gchar     *markup = NULL;

		priv->current_action = g_object_ref (action);

		g_object_get (LB_OBJECT (action), "pixbuf", &pixbuf, NULL);

		if (match) {
			markup = lb_string_markup_substring (LB_OBJECT (action)->name,
							     match, "u");
		}

		if (!markup) {
			markup = g_markup_escape_text (LB_OBJECT (action)->name,
						       -1);
		}

		g_object_set (priv->action_icon_box,
			      "caption", markup,
			      "pixbuf",  pixbuf ? pixbuf : priv->unknown_pixbuf,
			      NULL);

		if (pixbuf) {
			g_object_unref (pixbuf);
		}

		g_free (markup);
	} else {
		g_object_set (priv->action_icon_box,
			      "caption", NULL,
			      "pixbuf",  priv->empty_pixbuf,
			      NULL);
	}
}

static void
window_queue_search (LbWindow *window)
{
	LbWindowPriv *priv = GET_PRIV (window);
	LbObject     *current;
	LbIconBox    *icon_box;
	gint          timeout;

	if (priv->focus == LB_WINDOW_FOCUS_ITEM) {
		current  = LB_OBJECT (priv->current_item);
		icon_box = LB_ICON_BOX (priv->item_icon_box);
	} else {
		current  = LB_OBJECT (priv->current_action);
		icon_box = LB_ICON_BOX (priv->action_icon_box);
	}

	/* If we already have a match, try matching further on it. */
	if (current) {
		if (lb_string_has_substring (current->name,
					     priv->search_string->str)) {
			gchar *markup;

			markup = lb_string_markup_substring (current->name,
							     priv->search_string->str, "u");

			g_object_set (icon_box,
				      "caption", markup ? markup : current->name,
				      NULL);

			g_free (markup);
		} else {
			if (priv->focus == LB_WINDOW_FOCUS_ITEM) {
				window_set_item (window, NULL, NULL);
			} else {
				window_set_action (window, NULL, NULL);
			}

			g_object_set (icon_box,
				      "caption", priv->search_string->str,
				      NULL);
		}
	} else {
		g_object_set (icon_box,
			      "caption", priv->search_string->str,
			      NULL);
	}

	if (priv->search_timeout_id) {
		g_source_remove (priv->search_timeout_id);
	}

	timeout = SEARCH_TIMEOUT_BASE +
		SEARCH_TIMEOUT_MAX -
		SEARCH_TIMEOUT_STEP * strlen (priv->search_string->str);
	timeout = CLAMP (timeout, SEARCH_TIMEOUT_BASE, SEARCH_TIMEOUT_MAX);

	priv->search_timeout_id = g_timeout_add (timeout,
						 (GSourceFunc) window_search_timeout_cb,
						 window);
}

static gboolean
window_search_timeout_cb (LbWindow *window)
{
	LbWindowPriv *priv = GET_PRIV (window);

	priv->search_timeout_id = 0;

	window_perform_search (window, priv->search_string->str);

	return FALSE;
}

static gboolean
window_search_reset_timeout_cb (LbWindow *window)
{
	LbWindowPriv *priv = GET_PRIV (window);

	priv->search_reset_timeout_id = 0;
	priv->reset_search = TRUE;

	return FALSE;
}

static void
window_perform_search (LbWindow    *window,
		       const gchar *str)
{
	LbWindowPriv     *priv;
	GList            *list, *l;
	GtkListStore     *store;
	GtkTreeSelection *selection;
	gboolean          selected = FALSE;
	gboolean          delayed_activation;

	priv = GET_PRIV (window);

	delayed_activation = priv->delayed_activation;

	window_reset_result (window);

	if (str && str[0] == '\0') {
		gchar            *tmp;
		str = NULL;
		tmp = g_strdup_printf ("<i>%s</i>", _("Type to start searching"));
		gtk_label_set_markup (GTK_LABEL (priv->instr_label), tmp);
		g_free (tmp);
	}

	if (!str && priv->focus == LB_WINDOW_FOCUS_ITEM) {
		window_hide_search_results (window);
		return;
	}

	if (priv->focus == LB_WINDOW_FOCUS_ITEM) {
		list = lb_module_manager_query (priv->manager, str);
	} else {
		list = lb_item_get_actions (priv->current_item, str);
	}

	if (!list) {
		gchar            *tmp;
		window_hide_search_results (window);
		tmp = g_strdup_printf ("<b>%s</b>", _("No match"));
		gtk_label_set_markup (GTK_LABEL (priv->instr_label), tmp);
		g_free (tmp);
		return;
	}

	store = GTK_LIST_STORE (gtk_tree_view_get_model (
					GTK_TREE_VIEW (priv->result_treeview)));

	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->result_treeview));

	for (l = list; l; l = l->next) {
		LbObject    *object;
		gchar       *markup;
		GdkPixbuf   *pixbuf;
		GtkTreeIter  iter;

		object = LB_OBJECT (l->data);

		/* Comment out for now, it looks a bit bad. */
		markup = NULL; /*lb_string_markup_substring (object->name, str, "b");*/
		
		g_object_get (object, "pixbuf", &pixbuf, NULL);
		if (!pixbuf) {
			pixbuf = g_object_ref (priv->unknown_pixbuf);
		}

		gtk_list_store_append (store, &iter);
		gtk_list_store_set (store, &iter,
				    COL_OBJECT, object,
				    COL_PIXBUF, pixbuf,
				    COL_NAME,   markup ? markup : object->name,
				    -1);
		g_object_unref (pixbuf);
		g_free (markup);

		if (!selected) {
			gtk_tree_selection_select_iter (selection, &iter);
			selected = TRUE;
		}
	}

	g_list_free (list);

	/* Don't show the results again if we already activated. */
	if (!delayed_activation) {
		window_show_search_results (window);
	}
}

static void
window_show_search_results (LbWindow *window)
{
	LbWindowPriv   *priv = GET_PRIV (window);
	GtkRequisition req;

	gtk_widget_show (priv->result_sw);
	gtk_widget_hide (priv->instruction);

	/* Resize the window to fit the new content. */
	gtk_widget_size_request (GTK_WIDGET (window), &req);
	gtk_window_resize (GTK_WINDOW (window), req.width, req.height);
}

static void
window_hide_search_results (LbWindow *window)
{
	LbWindowPriv   *priv = GET_PRIV (window);
	GtkRequisition  req;

	gtk_widget_hide (priv->result_sw);
	gtk_widget_show (priv->instruction);

	/* Resize the window to fit the new content. */
	gtk_widget_size_request (GTK_WIDGET (window), &req);
	gtk_window_resize (GTK_WINDOW (window), req.width, req.height);
}

static void
window_result_row_selected_cb (GtkTreeSelection *selection,
			       LbWindow         *window)
{
	LbWindowPriv *priv = GET_PRIV (window);
	GtkTreeModel *model;
	GtkTreeIter   iter;

	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		LbObject *object;

		gtk_tree_model_get (model, &iter,
				    COL_OBJECT, &object,
				    -1);

		if (priv->focus == LB_WINDOW_FOCUS_ITEM) {
			window_set_item (window, LB_ITEM (object),
					 priv->search_string->str);
		} else {
			window_set_action (window, LB_ACTION (object),
					   priv->search_string->str);
		}

		g_object_unref (object);
	}
}

static void
window_reset_result (LbWindow *window)
{
	LbWindowPriv *priv;
	GtkListStore *store;

	priv = GET_PRIV (window);

	priv->reset_search = FALSE;
	store = GTK_LIST_STORE (gtk_tree_view_get_model (
					GTK_TREE_VIEW (priv->result_treeview)));
	gtk_list_store_clear (store);

	if (priv->focus == LB_WINDOW_FOCUS_ITEM) {
		window_set_item (window, NULL, NULL);
		g_object_set (priv->item_icon_box,
			      "caption", priv->search_string->str,
			      NULL);
	} else {
		window_set_action (window, NULL, NULL);
		g_object_set (priv->action_icon_box,
			      "caption", priv->search_string->str,
			      NULL);
	}
}


/* Test some search stuff. */

static void
window_string_utf8_remove_last_char (GString *string)
{
	gchar *ptr;
	glong  utf8_len;

	utf8_len = g_utf8_strlen (string->str, -1);

	if (utf8_len == 0) {
		return;
	}

	utf8_len--;

	ptr = g_utf8_offset_to_pointer (string->str, utf8_len);

	g_string_truncate (string, string->len - strlen (ptr));
}

static gboolean
window_key_press_event_cb (GtkWidget   *widget,
			   GdkEventKey *event,
			   LbWindow    *window)
{
	LbWindowPriv *priv = GET_PRIV (window);

	priv->delayed_activation = FALSE;

	if (event->state & GDK_CONTROL_MASK) {
		return TRUE;
	}

	switch (event->keyval) {
	case GDK_Escape:
		if (priv->search_timeout_id) {
			g_source_remove (priv->search_timeout_id);
			priv->search_timeout_id = 0;
		}

		window_hide_search_results (window);
		
		/* If text, clear it out, otherwise close the window. */
		if (priv->search_string->len > 0) {
			g_string_truncate (priv->search_string, 0);
			window_reset_result (window);
		} else {
			gtk_widget_hide (GTK_WIDGET (window));
		}
		
		return TRUE;

	case GDK_BackSpace:
		if (priv->search_string->len == 0) {
			return TRUE;
		}

		window_string_utf8_remove_last_char (priv->search_string);
		window_queue_search (window);
		
		window_start_reset_timeout (window);
		return TRUE;

	case GDK_Tab:
		if (priv->focus == LB_WINDOW_FOCUS_ITEM) {
			if (priv->current_action) {
				window_set_focus (window,
						  LB_WINDOW_FOCUS_ACTION);
			}
		} else {
			window_set_focus (window, LB_WINDOW_FOCUS_ITEM);
		}

		window_start_reset_timeout (window);
		return TRUE;

	case GDK_Up:
	case GDK_Down:
	{
		GtkTreeView      *tv;
		GtkTreeSelection *selection;
		GtkTreeModel     *model;
		GtkTreeIter       iter;

		tv = GTK_TREE_VIEW (priv->result_treeview);

		selection = gtk_tree_view_get_selection (tv);

		if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
			GtkTreePath *path;

			path = gtk_tree_model_get_path (model, &iter);

			if (event->keyval == GDK_Up) {
				gtk_tree_path_prev (path);
			} else {
				gtk_tree_path_next (path);
			}

			gtk_tree_selection_select_path (selection, path);
			gtk_tree_view_scroll_to_cell (tv, path,
						      NULL, FALSE, 0.0, 0.0);
			gtk_tree_path_free (path);
		}

		window_start_reset_timeout (window);
		return TRUE;
	}

	case GDK_Return:
	case GDK_ISO_Enter:
		if (priv->current_action) {
			lb_action_activate (priv->current_action);
			window_hide_search_results (window);
			gtk_widget_hide (GTK_WIDGET (window));
		} else {
			priv->delayed_activation = TRUE;
		}

		return TRUE;

	default:
		if (event->length > 0) {
			if (priv->reset_search) {
				g_string_truncate (priv->search_string, 0);
				window_reset_result (window);
			}
			
			g_string_append_unichar (priv->search_string,
						 gdk_keyval_to_unicode (event->keyval));
		} else {
			return TRUE;
		}
	}

	window_queue_search (window);
	window_start_reset_timeout (window);

	return TRUE;
}

static gboolean
window_grab (LbWindow *window)
{
	GdkWindow *gdk_window;
	guint32    time;

	gdk_window = GTK_WIDGET (window)->window;
	time = gdk_x11_get_server_time (gdk_window);

	if ((gdk_pointer_grab (gdk_window, TRUE,
			       GDK_BUTTON_PRESS_MASK |
			       GDK_BUTTON_RELEASE_MASK |
			       GDK_POINTER_MOTION_MASK,
			       NULL, NULL, time) == GDK_GRAB_SUCCESS)) {
		if (gdk_keyboard_grab (gdk_window, TRUE, time) == GDK_GRAB_SUCCESS) {
			return TRUE;
		} else {
			gdk_pointer_ungrab (time);
			return FALSE;
		}
	}

	return FALSE;
}

void
lb_window_present (LbWindow *window)
{
	LbWindowPriv *priv = GET_PRIV (window);
	gint          i = 0;

	if (!window_get_is_visible (window)) {
		g_string_truncate (priv->search_string, 0);
                g_free (priv->last_item_search_str);
                priv->last_item_search_str = NULL;

                window_set_focus (window, LB_WINDOW_FOCUS_ITEM);
		window_reset_result (window);

		if (GTK_WIDGET_REALIZED (GTK_WIDGET (window))) {
			window_set_position (window);
		}
		
		gtk_window_present (GTK_WINDOW (window));
	}

	/* This is a really ugly hack. There seems to be a problem in recent
	 * metacity versions that grab the keyboard when invoking a keybinding
	 * command, which makes our grabbing fail. So we retry for a while...
	 */
	while (i < 100) {
		if (window_grab (window)) {
			break;
		}
		i++;

		g_usleep (100);
	}
}

static gboolean
window_get_is_visible (LbWindow *window)
{
	gboolean visible;

	g_object_get (window,
		      "visible", &visible,
		      NULL);

	if (GTK_WIDGET (window)->window) {
		gboolean iconified;

		iconified = gdk_window_get_state (GTK_WIDGET (window)->window) &
			GDK_WINDOW_STATE_ICONIFIED;

		visible = visible && !iconified;
	}

	return visible;
}

static void
window_set_position (LbWindow *window)
{
	LbWindowPriv *priv;
	GdkDisplay   *display;
        GdkScreen    *screen;
	GdkRectangle  moni_geo;
        gint          screen_xorg, screen_yorg;
	gint          screen_width, screen_height;
	gint          x, y;
	gint          px, py;
	gint          width, height;
	gint          moni_n;

	priv = GET_PRIV (window);

	gtk_window_get_size (GTK_WINDOW (window), &width, &height);
	
	display = gtk_widget_get_display (GTK_WIDGET (window));
	gdk_display_get_pointer (display, &screen, &px, &py, NULL);
	moni_n = gdk_screen_get_monitor_at_point (screen, px, py);
	gdk_screen_get_monitor_geometry (screen, moni_n, &moni_geo);
	
	screen_xorg = moni_geo.x;
        screen_yorg = moni_geo.y;
        screen_width = moni_geo.width;
        screen_height = moni_geo.height;
	
	x = screen_width / 2 - width / 2 + screen_xorg;
	y = screen_height / 3 - height / 2 + screen_yorg;

	gtk_window_move (GTK_WINDOW (window), x, y);
}

static void
window_set_colormap (LbWindow *window)
{
	LbWindowPriv *priv = GET_PRIV (window);
	GtkWidget    *widget;
	GdkScreen    *screen;
	GdkColormap  *colormap;

	widget = GTK_WIDGET (window);

	screen = gtk_widget_get_screen (widget);
	colormap = gdk_screen_get_rgba_colormap (screen);

	if (colormap != NULL) {
		priv->can_rgba = TRUE;
	} else {
		colormap = gdk_screen_get_rgb_colormap (screen);
		g_debug (" No Alpha support \n");
	}

	gtk_widget_set_colormap (widget, colormap);
}

static void
window_start_reset_timeout (LbWindow *window)
{
	LbWindowPriv *priv = GET_PRIV (window);

	if (priv->search_reset_timeout_id) {
		g_source_remove (priv->search_reset_timeout_id);
	}

	priv->search_reset_timeout_id = g_timeout_add (
		SEARCH_RESET_TIMEOUT,
		(GSourceFunc) window_search_reset_timeout_cb,
		window);
}

