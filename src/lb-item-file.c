/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-item-file.c
 */

#include "config.h"

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "lb-item-file.h"
#include "lb-action.h"

#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_ITEM_FILE, LbItemFilePriv))

typedef struct _LbItemFilePriv LbItemFilePriv;
struct _LbItemFilePriv {
	gchar *uri;
	gchar *mime_type;
};

enum {
	PROP_0,
	PROP_URI,
	PROP_MIME_TYPE
};

static void   lb_item_file_finalize     (GObject         *object);
static void   lb_item_file_set_property (GObject         *object,
					 guint            property_id,
					 const GValue    *value,
					 GParamSpec      *pspec);
static void   lb_item_file_get_property (GObject         *object,
					 guint            property_id,
					 GValue          *value,
					 GParamSpec      *pspec);

G_DEFINE_TYPE (LbItemFile, lb_item_file, LB_TYPE_ITEM);
static LbItemClass *parent_class = NULL;

#if 0
static void
activate_action (LbAction *action, ActionInfo *info)
{
	GList list;

	list.next = NULL;
	list.data = info->file;

	gnome_vfs_mime_application_launch (info->application, &list);

	g_print ("activate me!\n");
}
#endif

static void
lb_item_file_class_init (LbItemFileClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = lb_item_file_finalize;
	object_class->set_property = lb_item_file_set_property;
	object_class->get_property = lb_item_file_get_property;

	g_object_class_install_property (object_class, PROP_URI,
					 g_param_spec_string ("uri",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_MIME_TYPE,
					 g_param_spec_string ("mime-type",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_type_class_add_private (object_class, sizeof (LbItemFilePriv));
}

static void
lb_item_file_init (LbItemFile *file)
{
	LbItemFilePriv *priv;

	priv = GET_PRIV (file);

	priv->uri       = NULL;
	priv->mime_type = NULL;

	/* Set a fall-back icon. */
	g_object_set (file,
		      "icon-name", "gnome-mime-text",
		      NULL);
}

static void
lb_item_file_finalize (GObject *object)
{
	LbItemFilePriv *priv;

	priv = GET_PRIV (object);

	if (priv->uri) {
		g_free (priv->uri);
		priv->uri = NULL;
	}
	if (priv->mime_type) {
		g_free (priv->mime_type);
		priv->mime_type = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
lb_item_file_set_property (GObject      *object,
			   guint         property_id,
			   const GValue *value,
			   GParamSpec   *pspec)
{
	LbItemFilePriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
	case PROP_URI:
		if (priv->uri) {
			g_free (priv->uri);
		}

		priv->uri = g_value_dup_string (value);
		break;
	case PROP_MIME_TYPE:
		if (priv->mime_type) {
			g_free (priv->mime_type);
		}

		priv->mime_type = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
lb_item_file_get_property (GObject    *object,
			   guint       property_id,
			   GValue     *value,
			   GParamSpec *pspec)
{
	LbItemFilePriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
	case PROP_URI:
		g_value_set_string (value, priv->uri);
		break;
	case PROP_MIME_TYPE:
		g_value_set_string (value, priv->mime_type);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

const gchar *
lb_item_file_get_uri (LbItemFile *item)
{
	LbItemFilePriv *priv;

	g_return_val_if_fail (LB_IS_ITEM_FILE (item), NULL);

	priv = GET_PRIV (item);

	return priv->uri;
}

void
lb_item_file_set_uri (LbItemFile  *item,
		      const gchar *uri)
{
	LbItemFilePriv *priv;

	g_return_if_fail (LB_IS_ITEM_FILE (item));

	priv = GET_PRIV (item);

	g_free (priv->uri);
	priv->uri = g_strdup (uri);

	g_object_notify (G_OBJECT (item), "uri");
}


const gchar *
lb_item_file_get_mime_type (LbItemFile *item)
{
	LbItemFilePriv *priv;

	g_return_val_if_fail (LB_IS_ITEM_FILE (item), NULL);

	priv = GET_PRIV (item);

	return priv->mime_type;
}

void
lb_item_file_set_mime_type (LbItemFile  *item,
			    const gchar *mime_type)
{
	LbItemFilePriv *priv;

	g_return_if_fail (LB_IS_ITEM_FILE (item));

	priv = GET_PRIV (item);

	g_free (priv->mime_type);
	priv->mime_type = g_strdup (mime_type);

	g_object_notify (G_OBJECT (item), "mime-type");
}
