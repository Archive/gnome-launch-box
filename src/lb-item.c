/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-item.c
 */

#include "config.h"

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "lb-private.h"
#include "lb-item.h"
#include "lb-private.h"
#include "lb-utils.h"

#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_ITEM, LbItemPriv))

typedef struct _LbItemPriv LbItemPriv;
struct _LbItemPriv {
	GList *actions;
};

#if 0
enum {
	PROP_0,
	PROP_NAME,
	PROP_STOCK_ID,
	PROP_PIXBUF
};
#endif

static void   lb_item_finalize     (GObject      *object);
static void   lb_item_set_property (GObject      *object,
                                    guint         property_id,
                                    const GValue *value,
                                    GParamSpec   *pspec);
static void   lb_item_get_property (GObject      *object,
                                    guint         property_id,
                                    GValue       *value,
                                    GParamSpec   *pspec);

G_DEFINE_TYPE (LbItem, lb_item, LB_TYPE_OBJECT);
static LbObjectClass *parent_class = NULL;

static void
lb_item_class_init (LbItemClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = lb_item_finalize;
	object_class->set_property = lb_item_set_property;
	object_class->get_property = lb_item_get_property;

#if 0
	g_object_class_install_property (object_class, PROP_NAME,
					 g_param_spec_string ("name",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_STOCK_ID,
					 g_param_spec_string ("stock-id",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_PIXBUF,
					 g_param_spec_object ("pixbuf",
							      NULL, NULL,
							      GDK_TYPE_PIXBUF,
							      G_PARAM_READWRITE));
#endif
	g_type_class_add_private (object_class, sizeof (LbItemPriv));
}

static void
lb_item_init (LbItem *item)
{
	LbItemPriv *priv;

	priv = GET_PRIV (item);

	priv->actions = NULL;

	/* Set a fall-back icon. */
	g_object_set (item,
		      "icon-name", "gnome-question",
		      NULL);
}

static void
lb_item_finalize (GObject *object)
{
	LbItemPriv *priv;
	GList      *l;

	priv = GET_PRIV (object);

	for (l = priv->actions; l; l = l->next) {
		g_object_unref (G_OBJECT (l->data));
	}
	g_list_free (priv->actions);

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
lb_item_set_property (GObject      *object,
                      guint         property_id,
                      const GValue *value,
                      GParamSpec   *pspec)
{
	/*LbItem *item = LB_ITEM (object);*/

	switch (property_id) {
#if 0
	case PROP_NAME:
		if (item->name)
			g_free (item->name);
		item->name = g_value_dup_string (value);
		break;
	case PROP_STOCK_ID:
		if (item->stock_id)
			g_free (item->stock_id);
		item->stock_id = g_value_dup_string (value);
		break;
	case PROP_PIXBUF:
		if (item->pixbuf)
			g_object_unref (item->pixbuf);
		item->pixbuf = GDK_PIXBUF (g_value_dup_object (value));
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
lb_item_get_property (GObject    *object,
                      guint       property_id,
                      GValue     *value,
                      GParamSpec *pspec)
{
	/*LbItem *item = LB_ITEM (object);*/

	switch (property_id) {
#if 0
	case PROP_NAME:
		g_value_set_string (value, item->name);
		break;
	case PROP_STOCK_ID:
		g_value_set_string (value, item->stock_id);
		break;
	case PROP_PIXBUF:
		g_value_set_object (value, item->pixbuf);
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

void
lb_item_add_action (LbItem   *item,
		    LbAction *action)
{
	LbItemPriv *priv;

	g_return_if_fail (LB_IS_ITEM (item));
	g_return_if_fail (LB_IS_ACTION (action));

	priv = GET_PRIV (item);

	priv->actions = g_list_prepend (priv->actions, g_object_ref (action));
	_lb_action_set_item (action, item);
}

GList *
lb_item_get_actions (LbItem      *item,
		     const gchar *match)
{
	LbItemPriv *priv;
	GList      *list;
	GList      *result = NULL;

	g_return_val_if_fail (LB_IS_ITEM (item), NULL);

	priv = GET_PRIV (item);

	for (list = priv->actions; list; list = list->next) {
		LbObject *object = list->data;

		if (! match || lb_string_has_substring (object->name, match)) {
			result = g_list_prepend (result, object);
		}
	}

	return g_list_reverse (result);
}
