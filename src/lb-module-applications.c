/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module-applications.c
 */

#include "config.h"

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtkicontheme.h>
#include <string.h>
#define GMENU_I_KNOW_THIS_IS_UNSTABLE
#include <gmenu-tree.h>

#include "lb-item-application.h"
#include "lb-utils.h"
#include "lb-module-applications.h"

#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_MODULE_APPLICATIONS, LbModuleApplicationsPriv))

enum {
	PROP_0
};

typedef struct _LbModuleApplicationsPriv LbModuleApplicationsPriv;
struct _LbModuleApplicationsPriv {
	GSList    *applications;
	GMenuTree *tree;
};


static void    module_applications_finalize     (GObject            *object);
static void    module_applications_set_property (GObject            *object,
						 guint               property_id,
						 const GValue       *value,
						 GParamSpec         *pspec);
static void    module_applications_get_property (GObject            *object,
						 guint               property_id,
						 GValue             *value,
						 GParamSpec         *pspec);

static GList * module_applications_query        (LbModule           *module,
						 const gchar        *match);
static void    module_applications_add_actions  (LbModule           *module,
						 LbItem             *item);
static void    module_applications_add_directory   (GSList           **applications,
						    GMenuTreeDirectory *dir);
static gboolean module_applications_ensure_app_list (LbModuleApplications *module);
static void    module_applications_tree_changed    (GMenuTree          *tree,
						    LbModuleApplications *module);
static void    module_applications_free_app_list   (LbModuleApplications *module);
gchar *        module_applications_strip_arguments (const gchar       *exec_name);


G_DEFINE_TYPE (LbModuleApplications, lb_module_applications,
	       LB_TYPE_MODULE);


static void
lb_module_applications_class_init (LbModuleApplicationsClass *klass)
{
	GObjectClass  *object_class = G_OBJECT_CLASS (klass);
	LbModuleClass *module_class = LB_MODULE_CLASS (klass);

	object_class->finalize     = module_applications_finalize;
	object_class->set_property = module_applications_set_property;
	object_class->get_property = module_applications_get_property;

	module_class->query        = module_applications_query;
	module_class->add_actions  = module_applications_add_actions;

	g_type_class_add_private (object_class,
				  sizeof (LbModuleApplicationsPriv));
}

static void
lb_module_applications_init (LbModuleApplications *module)
{
	LbModuleApplicationsPriv *priv = GET_PRIV (module);

	priv->applications = NULL;
	priv->tree         = NULL;

	module_applications_ensure_app_list (module);
}

static void
module_applications_finalize (GObject *object)
{
	LbModuleApplicationsPriv *priv = GET_PRIV (object);

	module_applications_free_app_list (LB_MODULE_APPLICATIONS (object));

	if (priv->tree) {
		gmenu_tree_remove_monitor (priv->tree,
					  (GMenuTreeChangedFunc)
					  module_applications_tree_changed,
					  object);

		gmenu_tree_unref (priv->tree);
		priv->tree = NULL;
	}

	G_OBJECT_CLASS (lb_module_applications_parent_class)->finalize (object);
}

static void
module_applications_set_property (GObject      *object,
				  guint         property_id,
				  const GValue *value,
				  GParamSpec   *pspec)
{
	switch (property_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
module_applications_get_property (GObject    *object,
				  guint       property_id,
				  GValue     *value,
				  GParamSpec *pspec)
{
	switch (property_id) {
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static GList *
module_applications_query (LbModule    *module,
			   const gchar *match)
{
	LbModuleApplicationsPriv *priv = GET_PRIV (module);
	GSList                   *l;
	GList                    *items;

	if (!module_applications_ensure_app_list (LB_MODULE_APPLICATIONS (module))) {
		return NULL;
	}

	items = NULL;

	for (l = priv->applications; l; l = l->next) {
		GMenuTreeEntry *entry;
		const gchar    *name;
		gchar          *exec_name;
		LbItem         *item;
		const gchar    *matched_name;

		entry = l->data;

		name = gmenu_tree_entry_get_name (entry);
		exec_name = module_applications_strip_arguments (gmenu_tree_entry_get_exec (entry));

		matched_name = NULL;
		if (name && lb_string_has_substring (name, match)) {
			matched_name = name;
		}
		else if (exec_name && lb_string_has_substring (exec_name, match)) {
			matched_name = (const gchar *) exec_name;
		}

		if (matched_name) {
			item = lb_item_application_new (gmenu_tree_entry_get_desktop_file_path (entry));
			g_object_set (item,
				"name", matched_name,
				"icon-name", gmenu_tree_entry_get_icon (entry),
				NULL);

			items = g_list_prepend (items, item);
		}

		g_free (exec_name);
	}

	return items;
}

static void
module_applications_add_actions (LbModule *module, LbItem *item)
{
	if (!LB_IS_ITEM_APPLICATION (item)) {
		return;
	}

	lb_item_application_add_actions (LB_ITEM_APPLICATION (item));
}

static void
module_applications_add_directory (GSList             **applications,
				   GMenuTreeDirectory  *dir)
{
	GSList *items;
	GSList *l;

	items = gmenu_tree_directory_get_contents (dir);

	for (l = items; l; l = l->next) {
		GMenuTreeItem *subitem;

		subitem = l->data;
		if (gmenu_tree_item_get_type (subitem) == GMENU_TREE_ITEM_DIRECTORY) {
			module_applications_add_directory (
				applications, GMENU_TREE_DIRECTORY (subitem));
			gmenu_tree_item_unref (subitem);
			
		}
		else if (gmenu_tree_item_get_type (subitem) == GMENU_TREE_ITEM_ENTRY) {
		   *applications = g_slist_append (*applications, subitem);
		}
	}

	g_slist_free (items);
}

static gboolean
module_applications_ensure_app_list (LbModuleApplications *module)
{
	LbModuleApplicationsPriv *priv = GET_PRIV (module);
	GMenuTreeDirectory        *root;
	GSList                   *applications;

	if (priv->applications) {
		return TRUE;
	}

	if (!priv->tree) {
		priv->tree = gmenu_tree_lookup ("applications.menu", GMENU_TREE_FLAGS_NONE);
		if (!priv->tree) {
			g_warning ("Failed to look up applications.menu");
			return FALSE;
		}

		/* Monitor the menu tree for changes */
		gmenu_tree_add_monitor (priv->tree,
				       (GMenuTreeChangedFunc)
				       module_applications_tree_changed,
				       module);

	}

	root = gmenu_tree_get_root_directory (priv->tree);
	if (!root) {
		g_warning ("Menu tree empty");
		gmenu_tree_unref (priv->tree);
		priv->tree = NULL;
		return FALSE;
	}

	applications = NULL;

	module_applications_add_directory (&applications, root);

	gmenu_tree_item_unref (root);

	priv->applications = applications;

	return TRUE;
}

static void
module_applications_tree_changed (GMenuTree             *tree,
				  LbModuleApplications *module)
{
	/* Remove the cached applications list */
	module_applications_free_app_list (module);
}

static void
module_applications_free_app_list (LbModuleApplications *module)
{
	LbModuleApplicationsPriv *priv = GET_PRIV (module);

	g_slist_foreach (priv->applications,
			 (GFunc) gmenu_tree_item_unref, NULL);
	g_slist_free (priv->applications);
	priv->applications = NULL;
}

gchar *
module_applications_strip_arguments (const gchar *exec_name)
{
	gchar *command;
	gchar *ch;

	command = g_strdup (exec_name);

	ch = strchr (command, ' ');
	if (ch) {
		*ch = '\0';
	}

	return command;
}
