/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module-recent.h
 */

#ifndef __LB_MODULE_RECENT_H__
#define __LB_MODULE_RECENT_H__


#include <gtk/gtk.h>
#include "lb-module.h"


#define LB_TYPE_MODULE_RECENT            (lb_module_recent_get_type ())
#define LB_MODULE_RECENT(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_MODULE_RECENT, LbModuleRecent))
#define LB_MODULE_RECENT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_MODULE_RECENT, LbModuleRecentClass))
#define LB_IS_MODULE_RECENT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_MODULE_RECENT))
#define LB_IS_MODULE_RECENT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_MODULE_RECENT))
#define LB_MODULE_RECENT_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_MODULE_RECENT, LbModuleRecentClass))


typedef struct _LbModuleRecent      LbModuleRecent;
typedef struct _LbModuleRecentClass LbModuleRecentClass;

struct _LbModuleRecent {
	LbModule  parent_instance;

	GtkRecentManager *manager;
	GList            *recent_items;
	gboolean          changed;
};

struct _LbModuleRecentClass {
	LbModuleClass  parent_class;
};


GType   lb_module_recent_get_type (void) G_GNUC_CONST;


#endif /* __LB_MODULE_RECENT_H__ */
