/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-utils.h
 */

#ifndef __LB_UTILS_H__
#define __LB_UTILS_H__

#include <gdk-pixbuf/gdk-pixbuf.h>

#define LB_ICON_SIZE 64

gchar*     lb_string_to_slug              (const gchar      *str);
gboolean   lb_string_has_substring        (const gchar      *str,
					   const gchar      *substr);
gchar     *lb_string_markup_substring     (const gchar      *str,
					   const gchar      *substr,
					   const gchar      *tag);
gchar     *lb_get_icon_name_for_uri       (const gchar      *uri);
gchar *    lb_get_icon_name_for_mime_type (const gchar      *mime_type);
GdkPixbuf *lb_get_pixbuf_from_icon_name   (const gchar      *icon_name);
gchar     *lb_get_firefox_path            (void);
gboolean   lb_file_lock                   (gint              fd);
gboolean   lb_file_unlock                 (gint              fd);

#endif /* __LB_UTILS_H__ */
