/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module-dummy.h
 */

#ifndef __LB_MODULE_DUMMY_H__
#define __LB_MODULE_DUMMY_H__


#include "lb-module.h"


#define LB_TYPE_MODULE_DUMMY            (lb_module_dummy_get_type ())
#define LB_MODULE_DUMMY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_MODULE_DUMMY, LbModuleDummy))
#define LB_MODULE_DUMMY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_MODULE_DUMMY, LbModuleDummyClass))
#define LB_IS_MODULE_DUMMY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_MODULE_DUMMY))
#define LB_IS_MODULE_DUMMY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_MODULE_DUMMY))
#define LB_MODULE_DUMMY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_MODULE_DUMMY, LbModuleDummyClass))


typedef struct _LbModuleDummy      LbModuleDummy;
typedef struct _LbModuleDummyClass LbModuleDummyClass;

struct _LbModuleDummy {
	LbModule  parent_instance;
};

struct _LbModuleDummyClass {
	LbModuleClass  parent_class;
};


GType   lb_module_dummy_get_type (void) G_GNUC_CONST;


#endif /* __LB_MODULE_DUMMY_H__ */
