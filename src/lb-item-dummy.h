/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-item-dummy.h
 */

#ifndef __LB_ITEM_DUMMY_H__
#define __LB_ITEM_DUMMY_H__


#include "lb-item.h"


#define LB_TYPE_ITEM_DUMMY            (lb_item_dummy_get_type ())
#define LB_ITEM_DUMMY(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_ITEM_DUMMY, LbItemDummy))
#define LB_ITEM_DUMMY_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_ITEM_DUMMY, LbItemDummyClass))
#define LB_IS_ITEM_DUMMY(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_ITEM_DUMMY))
#define LB_IS_ITEM_DUMMY_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_ITEM_DUMMY))
#define LB_ITEM_DUMMY_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_ITEM_DUMMY, LbItemDummyClass))


typedef struct _LbItemDummy      LbItemDummy;
typedef struct _LbItemDummyClass LbItemDummyClass;

struct _LbItemDummy {
	LbItem  parent_instance;
};

struct _LbItemDummyClass {
	LbItemClass  parent_class;
};


GType   lb_item_dummy_get_type (void) G_GNUC_CONST;

#if 0
const gchar *   lb_item_dummy_get_name (LbItemDummy *item);
void            lb_item_dummy_set_name (LbItemDummy *item,
					const gchar *name);
#endif

#endif /* __LB_ITEM_DUMMY_H__ */
