/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-icon-box.c
 */

#include <config.h>

#include <string.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkimage.h>
#include <gtk/gtklabel.h>

#include "lb-icon-box.h"
#include "lb-utils.h"


enum {
	PROP_0,
	PROP_CAPTION,
	PROP_PIXBUF,
	PROP_IS_FOCUSED,
	PROP_TRANSPARENT
};


typedef struct {
	gchar     *caption;
	GdkPixbuf *pixbuf;

	GtkWidget *box;
	GtkWidget *image;
	GtkWidget *label;

	gboolean   focused;
	gboolean   transparent;
} LbIconBoxPriv;


static void lb_icon_box_finalize        (GObject      *object);
static void lb_icon_box_set_property    (GObject      *object,
					 guint         property_id,
					 const GValue *value,
					 GParamSpec   *pspec);
static void lb_icon_box_get_property    (GObject      *object,
					 guint         property_id,
					 GValue       *value,
					 GParamSpec   *pspec);
static void icon_box_realize_cb         (GtkWidget    *widget,
					 LbIconBox    *box);



G_DEFINE_TYPE (LbIconBox, lb_icon_box, LB_TYPE_FRAME);
#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_ICON_BOX, LbIconBoxPriv))


static void
lb_icon_box_class_init (LbIconBoxClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	object_class->finalize     = lb_icon_box_finalize;
	object_class->set_property = lb_icon_box_set_property;
	object_class->get_property = lb_icon_box_get_property;

	g_object_class_install_property (object_class, PROP_CAPTION,
					 g_param_spec_string ("caption",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE));

	g_object_class_install_property (object_class, PROP_PIXBUF,
					 g_param_spec_object ("pixbuf",
							      NULL, NULL,
							      GDK_TYPE_PIXBUF,
							      G_PARAM_READWRITE));

	g_object_class_install_property (object_class, PROP_IS_FOCUSED,
					 g_param_spec_boolean ("focused",
							       NULL, NULL,
							       FALSE,
							       G_PARAM_READWRITE));

	g_object_class_install_property (object_class, PROP_TRANSPARENT,
					 g_param_spec_boolean ("transparent",
							       NULL, NULL,
							       FALSE,
							       G_PARAM_READWRITE));


	g_type_class_add_private (klass, sizeof (LbIconBoxPriv));
}

static void
lb_icon_box_init (LbIconBox *box)
{
	LbIconBoxPriv *priv;
	GtkWidget     *vbox;

	priv = GET_PRIV (box);

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 6);
	gtk_widget_show (vbox);

	gtk_container_add (GTK_CONTAINER (box), vbox);

	priv->image = gtk_image_new ();
	gtk_widget_show (priv->image);
	gtk_box_pack_start (GTK_BOX (vbox), priv->image, FALSE, FALSE, 0);

	priv->label = gtk_label_new (NULL);

	gtk_label_set_ellipsize (GTK_LABEL (priv->label), PANGO_ELLIPSIZE_END);
	gtk_widget_show (priv->label);
	gtk_box_pack_start (GTK_BOX (vbox), priv->label, FALSE, FALSE, 0);

	/* Need to do this in a better way. (Thumbnails aren't always the same
	 * size).
	 */
	gtk_widget_set_size_request (priv->image,
				     LB_ICON_SIZE + 2,
				     LB_ICON_SIZE + 2);
	gtk_widget_set_size_request (priv->label, LB_ICON_SIZE * 2, -1);

	g_signal_connect (box, "realize",
			  G_CALLBACK (icon_box_realize_cb),
			  box);
}

static void
lb_icon_box_finalize (GObject *object)
{
	LbIconBox     *box;
	LbIconBoxPriv *priv;

	box = LB_ICON_BOX (object);
	priv = GET_PRIV (box);

	g_free (priv->caption);

	if (priv->pixbuf) {
		g_object_unref (priv->pixbuf);
	}

	G_OBJECT_CLASS (lb_icon_box_parent_class)->finalize (object);
}

static void
lb_icon_box_set_property (GObject      *object,
			  guint         property_id,
			  const GValue *value,
			  GParamSpec   *pspec)
{
	LbIconBox     *box;
	LbIconBoxPriv *priv;

	box = LB_ICON_BOX (object);
	priv = GET_PRIV (box);

	switch (property_id) {
	case PROP_CAPTION:
		if (priv->caption) {
			g_free (priv->caption);
		}
		priv->caption = g_value_dup_string (value);

		if (priv->caption) {
			gchar *tmp;

			tmp = g_strdup_printf ("<b>%s</b>", priv->caption);
			gtk_label_set_markup (GTK_LABEL (priv->label), tmp);
			g_free (tmp);
		} else {
			gtk_label_set_text (GTK_LABEL (priv->label), NULL);
		}

		break;
	case PROP_PIXBUF:
		if (priv->pixbuf) {
			g_object_unref (priv->pixbuf);
		}

		priv->pixbuf = GDK_PIXBUF (g_value_dup_object (value));
		gtk_image_set_from_pixbuf (GTK_IMAGE (priv->image), priv->pixbuf);
		break;

	case PROP_IS_FOCUSED:
		lb_icon_box_set_focused (box, g_value_get_boolean (value));
		break;

	case PROP_TRANSPARENT:
		lb_icon_box_set_transparent (box, g_value_get_boolean (value));
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
lb_icon_box_get_property (GObject    *object,
			  guint       property_id,
			  GValue     *value,
			  GParamSpec *pspec)
{
	LbIconBox     *box;
	LbIconBoxPriv *priv;

	box = LB_ICON_BOX (object);
	priv = GET_PRIV (box);

	switch (property_id) {
	case PROP_CAPTION:
		g_value_set_string (value, priv->caption);
		break;
	case PROP_PIXBUF:
		g_value_set_object (value, priv->pixbuf);
		break;
	case PROP_IS_FOCUSED:
		g_value_set_boolean (value, priv->focused);
		break;
	case PROP_TRANSPARENT:
		g_value_set_boolean (value, priv->transparent);
		break;

	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

GtkWidget *
lb_icon_box_new (const gchar *caption, GdkPixbuf *pixbuf)
{
	GtkWidget *box;

	box = g_object_new (LB_TYPE_ICON_BOX,
			    "caption", caption,
			    "pixbuf", pixbuf,
			    NULL);

	return box;
}

static void
icon_box_update_highlight (LbIconBox *box)
{
	LbIconBoxPriv *priv;

	priv = GET_PRIV (box);

	if (priv->transparent) {
		guint16 alpha;

		/* TWEAK ME */	
		if (priv->focused) {
			alpha = G_MAXUINT16 * 0.6;
		} else {
			alpha = G_MAXUINT16 * 0.2;
		}
	
		lb_frame_set_fill_alpha (LB_FRAME (box), alpha);
	} else {
		if (priv->focused) {
			lb_frame_set_fill_color (
				LB_FRAME (box),
				&GTK_WIDGET (box)->style->base[GTK_STATE_SELECTED]);
			lb_frame_set_fill (LB_FRAME (box), TRUE);
		} else {
			lb_frame_set_fill_color (
				LB_FRAME (box),
				&GTK_WIDGET (box)->style->base[GTK_STATE_NORMAL]);
			lb_frame_set_fill (LB_FRAME (box), TRUE);
		}
	}
}

static void
icon_box_realize_cb (GtkWidget *widget,
		     LbIconBox *box)
{
	icon_box_update_highlight (box);
}

void
lb_icon_box_set_focused (LbIconBox *box,
			 gboolean   focused)
{
	LbIconBoxPriv *priv;

	priv = GET_PRIV (box);

	if (priv->focused == focused) {
		return;
	}

	priv->focused = focused;

	icon_box_update_highlight (box);
}

void
lb_icon_box_set_transparent (LbIconBox *box,
			     gboolean   transparent)
{
	LbIconBoxPriv *priv;

	priv = GET_PRIV (box);

	if (priv->transparent == transparent) {
		return;
	}

	if (!transparent) {
		GtkWidget *widget = GTK_WIDGET (box);
		GdkColor  *color;
		
		/* TWEAK ME */
		color = &(widget->style->fg[GTK_STATE_SELECTED]);
		
		lb_frame_set_frame_color (LB_FRAME (box), color);

		gtk_widget_modify_fg (priv->label,
				      GTK_STATE_NORMAL,
				      &widget->style->fg[GTK_STATE_NORMAL]);
	} else {
		GtkWidget *widget = GTK_WIDGET (box);
		GdkColor   color;
		
		/* white */
		color.red   = G_MAXUINT16;
		color.green = G_MAXUINT16;
		color.blue  = G_MAXUINT16;
	
		lb_frame_set_frame (LB_FRAME (box), FALSE);	
		lb_frame_set_fill_color (LB_FRAME (box), &color);
		lb_frame_set_fill (LB_FRAME (box), TRUE);

		gtk_widget_modify_fg (priv->label,
				      GTK_STATE_NORMAL,
				      &widget->style->white);
	}	
	
	priv->transparent = transparent;

	icon_box_update_highlight (box);
}

