/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * The GIMP -- an image manipulation program
 * Copyright (C) 1995 Spencer Kimball and Peter Mattis
 *
 * Recent File Storage,
 * see http://freedesktop.org/Standards/recent-file-spec/
 *
 * This code is taken from libegg and has been adapted to the GIMP needs.
 * The original author is James Willcox <jwillcox@cs.indiana.edu>,
 * responsible for bugs in this version is Sven Neumann <sven@gimp.org>.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <gtk/gtk.h>
#include <libxml/parser.h>
#include <libxml/HTMLparser.h>

#include "lb-bookmark-list.h"
#include "lb-utils.h"

static gboolean lb_bookmark_ignore_url (const gchar *url);
static void lb_bookmark_free (LbBookmark *bookmark);

/* Firefox bookmarks parsing code */
typedef struct {
        xmlParserCtxtPtr  xmlctxt;
        gint              res;
        gchar             chars[10];

	LbBookmark       *current_bookmark;
	GList            *bookmarks;
} HtmlParser;


static void
html_characters (gpointer       ctx,
		 const xmlChar *ch,
		 gint           len)
{
	HtmlParser *parser = ctx;

	if (parser->current_bookmark) {
		gchar *chars = g_strndup ((gchar *) ch, len);

		if (parser->current_bookmark->name) {
			gchar *tmp = g_strconcat (parser->current_bookmark->name,
						  chars, NULL);
			g_free (parser->current_bookmark->name);
			parser->current_bookmark->name = tmp;
		} else {
			parser->current_bookmark->name = g_strndup ((gchar *) ch, len);
		}
	}
}

static void
html_startElement (gpointer        ctx,
		   const xmlChar  *name,
		   const xmlChar **atts)
{
        HtmlParser *parser = ctx;

	if (!g_ascii_strcasecmp ((char *) name, "a")) {
		do {
			const xmlChar *att   = NULL;
			const xmlChar *value = NULL;

			g_assert (parser->current_bookmark == NULL);

			if (*atts) att   = *atts++;
			if (*atts) value = *atts++;

			if (att && value &&
			    !g_ascii_strcasecmp ((gchar *) att, "href")) {


				if (lb_bookmark_ignore_url ((gchar *) value))
					break;

				parser->current_bookmark = g_new0 (LbBookmark, 1);
				parser->current_bookmark->href = g_strdup ((gchar *) value);

				parser->bookmarks =
					g_list_prepend (parser->bookmarks,
							parser->current_bookmark);

				break;
			}
		} while (*atts);
	}
}

static void
html_endElement (gpointer       ctx,
		 const xmlChar *name)
{
	HtmlParser *parser = ctx;

	if (!g_ascii_strcasecmp ((gchar *) name, "a")) {
		parser->current_bookmark = NULL;
	}
}

xmlSAXHandler SAXHandlerStruct = {
	NULL, /* internalSubset */
	NULL, /* isStandalone */
	NULL, /* hasInternalSubset */
	NULL, /* hasExternalSubset */
	NULL, /* resolveEntity */
	NULL, /* getEntity */
	NULL, /* entityDecl */
	NULL, /* notationDecl */
	NULL, /* attributeDecl */
	NULL, /* elementDecl */
	NULL, /* unparsedEntityDecl */
	NULL, /* setDocumentLocator */
	NULL, /* startDocument */
	NULL, /* endDocument */
	html_startElement, /* startElement */
	html_endElement, /* endElement */
	NULL, /* reference */
	html_characters, /* characters */
	NULL, /* ignorableWhitespace */
	NULL, /* processingInstruction */
	NULL, /* comment */
	NULL, /* xmlParserWarning */
	NULL, /* xmlParserError */
	NULL, /* xmlParserError */
	NULL, /* getParameterEntity */
	html_characters, /* cdataBlock; */
};

xmlSAXHandlerPtr SAXHandler = &SAXHandlerStruct;

static GList *
lb_bookmark_list_firefox_read (gint fd)
{
	HtmlParser  parser;
	GIOChannel *io;
	GIOStatus   status;
	guchar      buffer[4096];
	gsize       len = 0;
	GError     *error = NULL;

	lseek (fd, 0, SEEK_SET);

	parser.res      = 0;
	parser.chars[0] = '\0';
	parser.xmlctxt  = htmlCreatePushParserCtxt (SAXHandler, &parser,
						    parser.chars, parser.res,
						    NULL, 0);
	parser.bookmarks = NULL;
	parser.current_bookmark = NULL;

	io = g_io_channel_unix_new (fd);

	g_io_channel_set_encoding (io, "UTF-8", NULL);

	while (TRUE) {
		status = g_io_channel_read_chars (io,
						  (gchar *) buffer,
						  sizeof (buffer),
						  &len, &error);

		switch (status) {
		case G_IO_STATUS_ERROR:
			goto done;
		case G_IO_STATUS_EOF:
			htmlParseChunk (parser.xmlctxt, NULL, 0, 1);
			goto done;
		case G_IO_STATUS_NORMAL:
		case G_IO_STATUS_AGAIN:
			break;
		}

		htmlParseChunk (parser.xmlctxt, (gchar *) buffer, len, 0);
	}

 done:

	g_io_channel_unref (io);

	xmlFreeParserCtxt (parser.xmlctxt);

	return g_list_reverse (parser.bookmarks);
}

GList *
lb_bookmark_list_get_firefox (const gchar *filename)
{
	gint   fd;
	GList *list = NULL;

	g_return_val_if_fail (filename != NULL, NULL);

	fd = open (filename, O_RDWR);

	if (fd < 0) {
		g_warning ("Could not open bookmarks file: %s", strerror (errno));
		return FALSE;
	}

	if (lb_file_lock (fd)) {
		list = lb_bookmark_list_firefox_read (fd);

		if (! lb_file_unlock (fd))
			g_warning ("Failed to unlock: %s", strerror (errno));
	} else {
		g_warning ("Failed to lock:  %s", g_strerror (errno));
		return FALSE;
	}

	close (fd);

	return list;
}

char *
lb_bookmark_list_get_path_firefox (const char *prefix)
{
	g_return_val_if_fail (prefix != NULL, NULL);

	return g_build_filename (prefix, "bookmarks.html", NULL);
}

/* Epiphany parsing code */

typedef struct {
	xmlParserCtxtPtr  xmlctxt;
	gint              res;
	gchar             chars[10];
	int               next_item;

	LbBookmark       *current_bookmark;
	GList            *bookmarks;
} XmlParser;

/* Copied from ephy-bookmarks.h */
enum
{
	EPHY_NODE_BMK_PROP_TITLE        = 2,
	EPHY_NODE_BMK_PROP_LOCATION     = 3,
	EPHY_NODE_BMK_PROP_KEYWORDS     = 4,
	EPHY_NODE_KEYWORD_PROP_NAME     = 5,
	EPHY_NODE_BMK_PROP_ICON         = 7,
	EPHY_NODE_KEYWORD_PROP_PRIORITY = 8,
	EPHY_NODE_BMK_PROP_IMMUTABLE    = 15
};

static void
lb_bookmark_icon_to_local_file (LbBookmark *bookmark)
{
  gchar const* md5;
  GChecksum  * checksum;

  if (bookmark->icon_path == NULL)
    return;

  checksum = g_checksum_new (G_CHECKSUM_MD5);
  g_checksum_update (checksum, bookmark->icon_path, -1);
  md5 = g_checksum_get_string (checksum);
  g_checksum_free (checksum);

  bookmark->icon_path = g_build_filename (g_get_home_dir (),
                                          ".gnome2", "epiphany",
                                          "favicon_cache", md5,
                                          NULL);

  if (!g_file_test (bookmark->icon_path, G_FILE_TEST_EXISTS))
    {
      g_free (bookmark->icon_path);
      bookmark->icon_path = NULL;
    }
}

static void
xml_characters (gpointer       ctx,
		const xmlChar *ch,
		gint           len)
{
	XmlParser *parser = ctx;

	if (!parser->current_bookmark || !parser->next_item)
		return;

	if (parser->next_item == EPHY_NODE_BMK_PROP_TITLE) {
		gchar *chars = g_strndup ((gchar *) ch, len);

		if (parser->current_bookmark->name) {
			gchar *tmp = g_strconcat (parser->current_bookmark->name,
						  chars, NULL);
			g_free (parser->current_bookmark->name);
			parser->current_bookmark->name = tmp;
		} else {
			parser->current_bookmark->name = g_strndup ((gchar *) ch, len);
		}
	} else if (parser->next_item == EPHY_NODE_BMK_PROP_LOCATION) {
		gchar *chars = g_strndup ((gchar *) ch, len);

		if (parser->current_bookmark->href) {
			gchar *tmp = g_strconcat (parser->current_bookmark->href,
						  chars, NULL);
			g_free (parser->current_bookmark->href);
			parser->current_bookmark->href = tmp;
		} else {
			parser->current_bookmark->href = g_strndup ((gchar *) ch, len);
		}
	} else if (parser->next_item == EPHY_NODE_BMK_PROP_ICON) {
		gchar *chars = g_strndup ((gchar *) ch, len);

		if (parser->current_bookmark->icon_path) {
			gchar *tmp = g_strconcat (parser->current_bookmark->icon_path,
						  chars, NULL);
			g_free (parser->current_bookmark->icon_path);
			parser->current_bookmark->icon_path = tmp;
		} else {
			parser->current_bookmark->icon_path = g_strndup ((gchar *) ch, len);
		}
	}
}

static void
xml_startElement (gpointer        ctx,
		  const xmlChar  *name,
		  const xmlChar **atts)
{
        XmlParser *parser = ctx;

	if (!g_ascii_strcasecmp ((char *) name, "node")) {
		g_assert (parser->current_bookmark == NULL);
		parser->current_bookmark = g_new0 (LbBookmark, 1);
	} else if (!g_ascii_strcasecmp ((char *) name, "property")) {
		do {
			const xmlChar *att   = NULL;
			const xmlChar *value = NULL;

			g_assert (parser->current_bookmark != NULL);

			if (*atts) att   = *atts++;
			if (*atts) value = *atts++;

			if (att && value &&
			    !g_ascii_strcasecmp ((gchar *) att, "id")) {
				if (g_strtod ((gchar *) value, NULL) == EPHY_NODE_BMK_PROP_LOCATION) {
					parser->next_item = EPHY_NODE_BMK_PROP_LOCATION;
				} else if (g_strtod ((gchar *) value, NULL) == EPHY_NODE_BMK_PROP_TITLE) {
					parser->next_item = EPHY_NODE_BMK_PROP_TITLE;
				} else if (g_strtod ((gchar *) value, NULL) == EPHY_NODE_BMK_PROP_ICON) {
					parser->next_item = EPHY_NODE_BMK_PROP_ICON;
				} else {
					parser->next_item = 0;
				}
			}
		} while (*atts);
	} else if (!g_ascii_strcasecmp ((char *) name, "parent")) {
		do {
			const xmlChar *att   = NULL;
			const xmlChar *value = NULL;

			g_assert (parser->current_bookmark != NULL);

			if (*atts) att   = *atts++;
			if (*atts) value = *atts++;

			if (att && value &&
			    !g_ascii_strcasecmp ((gchar *) att, "id")) {
				if (g_strtod ((gchar *) value, NULL) == 1) {
					lb_bookmark_free (parser->current_bookmark);
					parser->next_item = 0;
					parser->current_bookmark = NULL;
				}
				break;
			}
		} while (*atts);
	}
}

static void
xml_endElement (gpointer       ctx,
		const xmlChar *name)
{
	XmlParser *parser = ctx;

	if (!g_ascii_strcasecmp ((gchar *) name, "node")) {
		if (parser->current_bookmark &&
		    parser->current_bookmark->name &&
		    parser->current_bookmark->href &&
		    !lb_bookmark_ignore_url (parser->current_bookmark->href)) {
			g_strchomp (parser->current_bookmark->name);
			g_strchomp (parser->current_bookmark->href);

			if (parser->current_bookmark->icon_path)
				g_strchomp (parser->current_bookmark->icon_path);
			lb_bookmark_icon_to_local_file (parser->current_bookmark);

			parser->bookmarks = g_list_prepend (parser->bookmarks,
							    parser->current_bookmark);
		} else if (parser->current_bookmark) {
			lb_bookmark_free (parser->current_bookmark);
		}
		parser->current_bookmark = NULL;
		parser->next_item = 0;
	}
}

xmlSAXHandler ephySAXHandlerStruct = {
	NULL, /* internalSubset */
	NULL, /* isStandalone */
	NULL, /* hasInternalSubset */
	NULL, /* hasExternalSubset */
	NULL, /* resolveEntity */
	NULL, /* getEntity */
	NULL, /* entityDecl */
	NULL, /* notationDecl */
	NULL, /* attributeDecl */
	NULL, /* elementDecl */
	NULL, /* unparsedEntityDecl */
	NULL, /* setDocumentLocator */
	NULL, /* startDocument */
	NULL, /* endDocument */
	xml_startElement, /* startElement */
	xml_endElement, /* endElement */
	NULL, /* reference */
	xml_characters, /* characters */
	NULL, /* ignorableWhitespace */
	NULL, /* processingInstruction */
	NULL, /* comment */
	NULL, /* xmlParserWarning */
	NULL, /* xmlParserError */
	NULL, /* xmlParserError */
	NULL, /* getParameterEntity */
	xml_characters, /* cdataBlock; */
};

xmlSAXHandlerPtr ephySAXHandler = &ephySAXHandlerStruct;

static GList *
lb_bookmark_list_read_epiphany (gint fd)
{
	XmlParser   parser;
	GIOChannel *io;
	GIOStatus   status;
	guchar      buffer[4096];
	gsize       len = 0;
	GError     *error = NULL;

	lseek (fd, 0, SEEK_SET);

	parser.res      = 0;
	parser.chars[0] = '\0';
	parser.xmlctxt  = xmlCreatePushParserCtxt (ephySAXHandler, &parser,
						   parser.chars, parser.res,
						   NULL);
	parser.bookmarks = NULL;
	parser.current_bookmark = NULL;
	parser.next_item = 0;

	io = g_io_channel_unix_new (fd);

	g_io_channel_set_encoding (io, "UTF-8", NULL);

	while (TRUE) {
		status = g_io_channel_read_chars (io,
						  (gchar *) buffer,
						  sizeof (buffer),
						  &len, &error);

		switch (status) {
		case G_IO_STATUS_ERROR:
			goto done;
		case G_IO_STATUS_EOF:
			xmlParseChunk (parser.xmlctxt, NULL, 0, 1);
			goto done;
		case G_IO_STATUS_NORMAL:
		case G_IO_STATUS_AGAIN:
			break;
		}

		xmlParseChunk (parser.xmlctxt, (gchar *) buffer, len, 0);
	}

 done:

	g_io_channel_unref (io);

	xmlFreeParserCtxt (parser.xmlctxt);

	return g_list_reverse (parser.bookmarks);
}

GList *
lb_bookmark_list_get_epiphany (const gchar *filename)
{
	gint   fd;
	GList *list = NULL;

	g_return_val_if_fail (filename != NULL, NULL);

	fd = open (filename, O_RDWR);

	if (fd < 0) {
		g_warning ("Could not open bookmarks file: %s", strerror (errno));
		return FALSE;
	}

	if (lb_file_lock (fd)) {
		list = lb_bookmark_list_read_epiphany (fd);

		if (! lb_file_unlock (fd))
			g_warning ("Failed to unlock: %s", strerror (errno));
	} else {
		g_warning ("Failed to lock:  %s", g_strerror (errno));
		return FALSE;
	}

	close (fd);

	return list;
}

gchar *
lb_bookmark_list_get_path_epiphany (void)
{
	return g_build_filename (g_get_home_dir (), ".gnome2",
				 "epiphany", "ephy-bookmarks.xml", NULL);
}

/* Generic code */

static gboolean
lb_bookmark_ignore_url (const gchar *url)
{
	if (url == NULL)
		return TRUE;

	/* Ignore bookmarklets */
	if (g_str_has_prefix (url, "javascript:") != FALSE)
		return TRUE;

	/* FIXME: If we encounter "%s" in the
	   url, we ignore the bookmark. This
	   is because %s is used for Mozilla
	   smart bookmarks. It would be nice
	   to be able to enter smart bookmarks
	   directly sometime */
	if (strstr (url, "%s") != NULL)
		return TRUE;

	return FALSE;
}

void
lb_bookmark_list_free (GList *list)
{
	GList *l;

	for (l = list; l; l = l->next)
		lb_bookmark_free ((LbBookmark *) l->data);
}

static void
lb_bookmark_free (LbBookmark *bookmark)
{
	g_return_if_fail (bookmark != NULL);

	g_free (bookmark->name);
	g_free (bookmark->href);
	g_free (bookmark->icon_path);
	g_free (bookmark);
}

