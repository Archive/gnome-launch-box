/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * The GIMP -- an image manipulation program
 * Copyright (C) 1995-1997 Spencer Kimball and Peter Mattis
 *
 * GimpXmlParser
 * Copyright (C) 2003  Sven Neumann <sven@gimp.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#ifndef __LB_XML_PARSER_H__
#define __LB_XML_PARSER_H__


typedef struct _LbXmlParser LbXmlParser;


LbXmlParser * lb_xml_parser_new              (const GMarkupParser *markup_parser,
                                              gpointer             user_data);
gboolean      lb_xml_parser_parse_file       (LbXmlParser         *parser,
                                              const gchar         *filename,
                                              GError             **error);
gboolean      lb_xml_parser_parse_fd         (LbXmlParser         *parser,
                                              gint                 fd,
                                              GError             **error);
gboolean      lb_xml_parser_parse_io_channel (LbXmlParser         *parser,
                                              GIOChannel          *io,
                                              GError             **error);
gboolean      lb_xml_parser_parse_buffer     (LbXmlParser         *parser,
                                              const gchar         *buffer,
                                              gssize               len,
                                              GError             **error);
void          lb_xml_parser_free             (LbXmlParser         *parser);


#endif  /* __LB_XML_PARSER_H__ */
