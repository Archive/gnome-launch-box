/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2010 Sven Herzberg
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module-tracker.c
 */

#include "config.h"

#include <string.h>
#include <gio/gdesktopappinfo.h>
#include <libtracker-client/tracker.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "lb-item-application.h"
#include "lb-module-tracker.h"

#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_MODULE_TRACKER, LbModuleTrackerPriv))

typedef struct _LbModuleTrackerPriv LbModuleTrackerPriv;

struct _LbModuleTrackerPriv
{
  TrackerClient* client;
};

#if 0
enum {
	PROP_0,
	PROP_NAME,
	PROP_STOCK_ID,
	PROP_PIXBUF
};
#endif


static void    lb_module_tracker_finalize     (GObject            *object);
static void    lb_module_tracker_set_property (GObject            *object,
					     guint               property_id,
					     const GValue       *value,
					     GParamSpec         *pspec);
static void    lb_module_tracker_get_property (GObject            *object,
					     guint               property_id,
					     GValue             *value,
					     GParamSpec         *pspec);

static GList * lb_module_tracker_query        (LbModule           *module,
					     const gchar        *match);


G_DEFINE_TYPE (LbModuleTracker, lb_module_tracker, LB_TYPE_MODULE);
static LbModuleClass *parent_class = NULL;

static void
lb_module_tracker_add_actions (LbModule* module,
                               LbItem  * item)
{
  if (!LB_IS_ITEM_APPLICATION (item))
    {
      return;
    }

  lb_item_application_add_actions (LB_ITEM_APPLICATION (item));
}

static void
lb_module_tracker_class_init (LbModuleTrackerClass *klass)
{
	GObjectClass  *object_class = G_OBJECT_CLASS (klass);
	LbModuleClass *module_class = LB_MODULE_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = lb_module_tracker_finalize;
	object_class->set_property = lb_module_tracker_set_property;
	object_class->get_property = lb_module_tracker_get_property;

	module_class->query        = lb_module_tracker_query;
        module_class->add_actions  = lb_module_tracker_add_actions;

#if 0
	g_object_class_install_property (object_class, PROP_NAME,
					 g_param_spec_string ("name",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_STOCK_ID,
					 g_param_spec_string ("stock-id",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_PIXBUF,
					 g_param_spec_object ("pixbuf",
							      NULL, NULL,
							      GDK_TYPE_PIXBUF,
							      G_PARAM_READWRITE));
#endif

	g_type_class_add_private (object_class,
				  sizeof (LbModuleTrackerPriv));

}

static void
lb_module_tracker_init (LbModuleTracker *tracker)
{
  LbModuleTrackerPriv *priv;

  priv = GET_PRIV (tracker);

  priv->client = tracker_client_new (0, -1);
}

static void
lb_module_tracker_finalize (GObject *object)
{
	LbModuleTrackerPriv *priv;

	priv = GET_PRIV (object);

#if 0
	if (tracker->name) {
		g_free (tracker->name);
		tracker->name = NULL;
	}
	if (tracker->stock_id) {
		g_free (tracker->stock_id);
		tracker->stock_id = NULL;
	}
	if (tracker->pixbuf) {
		g_object_unref (tracker->pixbuf);
		tracker->pixbuf = NULL;
	}
#endif

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
lb_module_tracker_set_property (GObject      *object,
			      guint         property_id,
			      const GValue *value,
			      GParamSpec   *pspec)
{
	LbModuleTrackerPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
#if 0
	case PROP_NAME:
		if (tracker->name)
			g_free (tracker->name);
		tracker->name = g_value_dup_string (value);
		break;
	case PROP_STOCK_ID:
		if (tracker->stock_id)
			g_free (tracker->stock_id);
		tracker->stock_id = g_value_dup_string (value);
		break;
	case PROP_PIXBUF:
		if (tracker->pixbuf)
			g_object_unref (tracker->pixbuf);
		tracker->pixbuf = GDK_PIXBUF (g_value_dup_object (value));
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
lb_module_tracker_get_property (GObject    *object,
			      guint       property_id,
			      GValue     *value,
			      GParamSpec *pspec)
{
	LbModuleTrackerPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
#if 0
	case PROP_NAME:
		g_value_set_string (value, tracker->name);
		break;
	case PROP_STOCK_ID:
		g_value_set_string (value, tracker->stock_id);
		break;
	case PROP_PIXBUF:
		g_value_set_object (value, tracker->pixbuf);
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static GList *
lb_module_tracker_query (LbModule    *module,
		       const gchar *match)
{
  LbModuleTrackerPriv *priv;
  gchar* argv[] = {
          "tracker-sparql",
          "-q",
          NULL,
          NULL
  };
  enum
    {
      URN, TITLE, COMMENT, COMMANDLINE
    };
  GList* list = NULL;
  gchar* sparql = g_strdup_printf ("SELECT ?urn ?title ?tooltip ?link fts:rank(?urn) "
                                   "WHERE { ?urn a nfo:Software ;"
                                   " nie:title ?title ;"
                                   " nie:comment ?tooltip ;"
                                   " nfo:softwareCmdLine ?link .  ?urn fts:match \"%s*\""
                                   " } "
                                   "ORDER BY DESC(fts:rank(?urn)) "
                                   "OFFSET 0 LIMIT 10", match);
  GPtrArray* result;
  GError* error = NULL;

  priv = GET_PRIV (module);

  argv[G_N_ELEMENTS (argv) - 2] = sparql;

  result = tracker_resources_sparql_query (priv->client, sparql, &error);

  if (error)
    {
      g_warning ("error querying tracker for \"%s\": %s",
                 match, error->message);
      g_error_free (error);
    }
  if (result)
    {
      size_t index;

      for (index = 0; index < result->len; index++)
        {
          gchar const** column = g_ptr_array_index (result, index);
          column[URN] += strlen ("file://");
          GDesktopAppInfo* info = g_desktop_app_info_new_from_filename (column[URN]);
          GIcon* icon = g_app_info_get_icon (G_APP_INFO (info));
          gchar const* name = NULL;
          if (G_IS_THEMED_ICON (icon))
            {
              gchar const * const * names = g_themed_icon_get_names (G_THEMED_ICON (icon));
              if (names)
                {
                  name = *names;
                }
            }
          LbItem* item = lb_item_application_new (column[URN]);
          g_object_set (item,
                        "name", column[TITLE],
                        "icon-name", name,
                        NULL);
          list = g_list_prepend (list, item);
        }
      list = g_list_reverse (list);

      g_ptr_array_free (result, TRUE);
    }

  return list;
}

/* vim:set et sw=2 cino=t0,f0,(0,{s,>2s,n-1s,^-1s,e2s: */
