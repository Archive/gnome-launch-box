/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module-dummy.c
 */

#include "config.h"

#include <string.h>

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "lb-module-dummy.h"

#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_MODULE_DUMMY, LbModuleDummyPriv))

typedef struct _LbModuleDummyPriv LbModuleDummyPriv;
struct _LbModuleDummyPriv {
	gint placeholder; /* Remove this, can't have empty struct */
};

#if 0
enum {
	PROP_0,
	PROP_NAME,
	PROP_STOCK_ID,
	PROP_PIXBUF
};
#endif


static void    lb_module_dummy_finalize     (GObject            *object);
static void    lb_module_dummy_set_property (GObject            *object,
					     guint               property_id,
					     const GValue       *value,
					     GParamSpec         *pspec);
static void    lb_module_dummy_get_property (GObject            *object,
					     guint               property_id,
					     GValue             *value,
					     GParamSpec         *pspec);

static GList * lb_module_dummy_query        (LbModule           *module,
					     const gchar        *match);


G_DEFINE_TYPE (LbModuleDummy, lb_module_dummy, LB_TYPE_MODULE);
static LbModuleClass *parent_class = NULL;

static void
lb_module_dummy_class_init (LbModuleDummyClass *klass)
{
	GObjectClass  *object_class = G_OBJECT_CLASS (klass);
	LbModuleClass *module_class = LB_MODULE_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = lb_module_dummy_finalize;
	object_class->set_property = lb_module_dummy_set_property;
	object_class->get_property = lb_module_dummy_get_property;

	module_class->query        = lb_module_dummy_query;

#if 0
	g_object_class_install_property (object_class, PROP_NAME,
					 g_param_spec_string ("name",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_STOCK_ID,
					 g_param_spec_string ("stock-id",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_PIXBUF,
					 g_param_spec_object ("pixbuf",
							      NULL, NULL,
							      GDK_TYPE_PIXBUF,
							      G_PARAM_READWRITE));
#endif

	g_type_class_add_private (object_class,
				  sizeof (LbModuleDummyPriv));

}

static void
lb_module_dummy_init (LbModuleDummy *dummy)
{
	LbModuleDummyPriv *priv;

	priv = GET_PRIV (dummy);
}

static void
lb_module_dummy_finalize (GObject *object)
{
	LbModuleDummyPriv *priv;

	priv = GET_PRIV (object);

#if 0
	if (dummy->name) {
		g_free (dummy->name);
		dummy->name = NULL;
	}
	if (dummy->stock_id) {
		g_free (dummy->stock_id);
		dummy->stock_id = NULL;
	}
	if (dummy->pixbuf) {
		g_object_unref (dummy->pixbuf);
		dummy->pixbuf = NULL;
	}
#endif

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
lb_module_dummy_set_property (GObject      *object,
			      guint         property_id,
			      const GValue *value,
			      GParamSpec   *pspec)
{
	LbModuleDummyPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
#if 0
	case PROP_NAME:
		if (dummy->name)
			g_free (dummy->name);
		dummy->name = g_value_dup_string (value);
		break;
	case PROP_STOCK_ID:
		if (dummy->stock_id)
			g_free (dummy->stock_id);
		dummy->stock_id = g_value_dup_string (value);
		break;
	case PROP_PIXBUF:
		if (dummy->pixbuf)
			g_object_unref (dummy->pixbuf);
		dummy->pixbuf = GDK_PIXBUF (g_value_dup_object (value));
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
lb_module_dummy_get_property (GObject    *object,
			      guint       property_id,
			      GValue     *value,
			      GParamSpec *pspec)
{
	LbModuleDummyPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
#if 0
	case PROP_NAME:
		g_value_set_string (value, dummy->name);
		break;
	case PROP_STOCK_ID:
		g_value_set_string (value, dummy->stock_id);
		break;
	case PROP_PIXBUF:
		g_value_set_object (value, dummy->pixbuf);
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static GList *
lb_module_dummy_query (LbModule    *module,
		       const gchar *match)
{
	LbModuleDummyPriv *priv;

	priv = GET_PRIV (module);

	return NULL;
}
