/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-action.c
 */

#include "config.h"

#include <glib/gi18n.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "lb-action.h"
#include "lb-private.h"

#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_ACTION, LbActionPriv))

typedef struct _LbActionPriv LbActionPriv;
struct _LbActionPriv {
	LbItem         *item;
	gpointer        data;
	GDestroyNotify  data_destroy_func;
};

enum {
        ACTIVATE,
	LAST_SIGNAL
};

#if 0
enum {
	PROP_0,
	PROP_NAME,
	PROP_STOCK_ID,
	PROP_PIXBUF
};
#endif


static void   lb_action_finalize     (GObject      *object);
static void   lb_action_set_property (GObject      *object,
                                      guint         property_id,
                                      const GValue *value,
                                      GParamSpec   *pspec);
static void   lb_action_get_property (GObject      *object,
                                      guint         property_id,
                                      GValue       *value,
                                      GParamSpec   *pspec);


G_DEFINE_TYPE (LbAction, lb_action, LB_TYPE_OBJECT);

static LbObjectClass *parent_class = NULL;
static guint signals[LAST_SIGNAL] = { 0 };

static void
lb_action_class_init (LbActionClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = lb_action_finalize;
	object_class->set_property = lb_action_set_property;
	object_class->get_property = lb_action_get_property;

	signals[ACTIVATE] =
		g_signal_new ("activate",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (LbActionClass, activate),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);

#if 0
	g_object_class_install_property (object_class, PROP_NAME,
					 g_param_spec_string ("name",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_STOCK_ID,
					 g_param_spec_string ("stock-id",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_object_class_install_property (object_class, PROP_PIXBUF,
					 g_param_spec_object ("pixbuf",
							      NULL, NULL,
							      GDK_TYPE_PIXBUF,
							      G_PARAM_READWRITE));
#endif
	g_type_class_add_private (object_class, sizeof (LbActionPriv));
}

static void
lb_action_init (LbAction *action)
{
	LbActionPriv *priv;

	priv = GET_PRIV (action);

	priv->item = NULL;
	priv->data = NULL;
	priv->data_destroy_func = NULL;

	/* Set a fall-back icon. */
	g_object_set (action,
		      "icon-name", "gnome-info",
		      NULL);
}

static void
lb_action_finalize (GObject *object)
{
	LbActionPriv *priv;

	priv = GET_PRIV (object);

	if (priv->data && priv->data_destroy_func) {
		priv->data_destroy_func (priv->data);
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
lb_action_set_property (GObject      *object,
                        guint         property_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
	/*LbAction *action = LB_ACTION (object);*/

	switch (property_id) {
#if 0
	case PROP_NAME:
		if (action->name)
			g_free (action->name);
		action->name = g_value_dup_string (value);
		break;
	case PROP_STOCK_ID:
		if (action->stock_id)
			g_free (action->stock_id);
		action->stock_id = g_value_dup_string (value);
		break;
	case PROP_PIXBUF:
		if (action->pixbuf)
			g_object_unref (action->pixbuf);
		action->pixbuf = GDK_PIXBUF (g_value_dup_object (value));
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
lb_action_get_property (GObject    *object,
                        guint       property_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
	/*LbAction *action = LB_ACTION (object);*/

	switch (property_id) {
#if 0
	case PROP_NAME:
		g_value_set_string (value, action->name);
		break;
	case PROP_STOCK_ID:
		g_value_set_string (value, action->stock_id);
		break;
	case PROP_PIXBUF:
		g_value_set_object (value, action->pixbuf);
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

void
_lb_action_set_item (LbAction *action,
		     LbItem   *item)
{
	LbActionPriv *priv;

	g_return_if_fail (LB_IS_ACTION (action));
	g_return_if_fail (LB_IS_ITEM (item));

	priv = GET_PRIV (action);

	priv->item = item;
}

void
lb_action_activate (LbAction *action)
{
	g_signal_emit (action, signals[ACTIVATE], 0);
}

LbItem *
lb_action_get_item (LbAction *action)
{
	LbActionPriv *priv;

	g_return_val_if_fail (LB_IS_ACTION (action), NULL);

	priv = GET_PRIV (action);

	return priv->item;
}

void
lb_action_set_data (LbAction       *action,
		    gpointer        data,
		    GDestroyNotify  data_destroy_func)
{
	LbActionPriv *priv;

	g_return_if_fail (LB_IS_ACTION (action));

	priv = GET_PRIV (action);

	if (priv->data && priv->data_destroy_func) {
		priv->data_destroy_func (priv->data);
	}

	priv->data = data;
	priv->data_destroy_func = data_destroy_func;
}

gconstpointer
lb_action_get_data (LbAction *action)
{
	LbActionPriv *priv;

	g_return_val_if_fail (LB_IS_ACTION (action), NULL);

	priv = GET_PRIV (action);

	return priv->data;
}

LbAction *
lb_action_new (LbActionType type)
{
	LbAction    *action;
	const gchar *name;
	const gchar *icon_name = NULL;

	action = NULL;

	switch (type) {
	case LB_ACTION_LAUNCH:
		name      = _("Launch");
		icon_name = "gnome-run"; /*fs-executable";*/
		break;
	case LB_ACTION_OPEN:
		name      = _("Open");
		icon_name = "gtk-open";
		break;
	default:
		return NULL;
	}

	action = g_object_new (LB_TYPE_ACTION,
			       "name",      name,
			       "icon-name", icon_name,
			       NULL);

	return action;
}

