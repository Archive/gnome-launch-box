/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-window.h
 */

#ifndef __LB_WINDOW_H__
#define __LB_WINDOW_H__

#include <gtk/gtkwindow.h>
#include "lb-module-manager.h"


#define LB_TYPE_WINDOW            (lb_window_get_type ())
#define LB_WINDOW(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_WINDOW, LbWindow))
#define LB_WINDOW_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_WINDOW, LbWindowClass))
#define LB_IS_WINDOW(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_WINDOW))
#define LB_IS_WINDOW_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_WINDOW))
#define LB_WINDOW_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_WINDOW, LbWindowClass))


typedef struct _LbWindow      LbWindow;
typedef struct _LbWindowClass LbWindowClass;

struct _LbWindow {
	GtkWindow       parent_instance;
};

struct _LbWindowClass {
	GtkWindowClass  parent_class;
};


GType      lb_window_get_type (void) G_GNUC_CONST;
GtkWidget *lb_window_new      (LbModuleManager *manager,
			       gboolean         transparent);
void       lb_window_present  (LbWindow *window);

#endif /* __LB_WINDOW_H__ */
