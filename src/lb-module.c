/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module.c
 */

#include "config.h"

#include <string.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "lb-module.h"


enum {
	PROP_0,
	PROP_ENABLED
};


static void   lb_module_class_init   (LbModuleClass  *klass);
static void   lb_module_init         (LbModule       *module);

static void   lb_module_finalize     (GObject      *object);
static void   lb_module_set_property (GObject      *object,
                                      guint         property_id,
                                      const GValue *value,
                                      GParamSpec   *pspec);
static void   lb_module_get_property (GObject      *object,
                                      guint         property_id,
                                      GValue       *value,
                                      GParamSpec   *pspec);


static LbObjectClass *parent_class = NULL;


GType
lb_module_get_type (void)
{
  static GType type = 0;

  if (! type) {
	  static const GTypeInfo info = {
		  sizeof (LbModuleClass),
		  (GBaseInitFunc) NULL,
		  (GBaseFinalizeFunc) NULL,
		  (GClassInitFunc) lb_module_class_init,
		  NULL,           /* class_finalize */
		  NULL,           /* class_data     */
		  sizeof (LbModule),
		  0,              /* n_preallocs    */
		  (GInstanceInitFunc) lb_module_init,
	  };

	  type = g_type_register_static (LB_TYPE_OBJECT,
					 "LbModule",
					 &info, 0);
    }

  return type;
}

static void
lb_module_class_init (LbModuleClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = lb_module_finalize;
	object_class->set_property = lb_module_set_property;
	object_class->get_property = lb_module_get_property;

	g_object_class_install_property (object_class, PROP_ENABLED,
					 g_param_spec_boolean ("enabled",
							       NULL, NULL,
							       TRUE,
							       G_PARAM_READWRITE |
							       G_PARAM_CONSTRUCT));
}

static void
lb_module_init (LbModule *module)
{
}

static void
lb_module_finalize (GObject *object)
{
	/*LbModule *module = LB_MODULE (object);*/

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
lb_module_set_property (GObject      *object,
                        guint         property_id,
                        const GValue *value,
                        GParamSpec   *pspec)
{
	LbModule *module = LB_MODULE (object);

	switch (property_id) {
	case PROP_ENABLED:
		module->enabled = g_value_get_boolean (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
lb_module_get_property (GObject    *object,
                        guint       property_id,
                        GValue     *value,
                        GParamSpec *pspec)
{
	LbModule *module = LB_MODULE (object);

	switch (property_id) {
	case PROP_ENABLED:
		g_value_set_boolean (value, module->enabled);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

LbModuleFlags
lb_module_get_flags (LbModule *module)
{
	g_return_val_if_fail (LB_IS_MODULE (module), 0);

	if (LB_MODULE_GET_CLASS (module)->get_flags) {
		return LB_MODULE_GET_CLASS (module)->get_flags (module);
	}

	return LB_MODULE_DEFAULT_FLAGS;
}

GList *
lb_module_query (LbModule    *module,
		 const gchar *match)
{
	g_return_val_if_fail (LB_IS_MODULE (module), NULL);

	if (LB_MODULE_GET_CLASS (module)->query) {
		return LB_MODULE_GET_CLASS (module)->query (module, match);
	}

	return NULL;
}

void
lb_module_add_actions (LbModule *module,
		       LbItem   *item)
{
	g_return_if_fail (LB_IS_MODULE (module));
	g_return_if_fail (LB_IS_ITEM (item));

	if (LB_MODULE_GET_CLASS (module)->add_actions) {
		LB_MODULE_GET_CLASS (module)->add_actions (module, item);
	}
}
