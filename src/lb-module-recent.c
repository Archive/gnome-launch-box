/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module-recent.c
 */

#include <config.h>

#include <string.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#include "lb-item-file.h"
#include "lb-module-recent.h"
#include "lb-utils.h"


static void    lb_module_recent_class_init   (LbModuleRecentClass *klass);
static void    lb_module_recent_init         (LbModuleRecent      *recent);

static void    lb_module_recent_finalize     (GObject             *object);

static GList * lb_module_recent_query        (LbModule            *module,
					      const gchar         *match);

static LbModuleClass *parent_class = NULL;


GType
lb_module_recent_get_type (void)
{
	static GType type = 0;

	if (!type) {
		static const GTypeInfo info = {
			sizeof (LbModuleRecentClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) lb_module_recent_class_init,
			NULL,           /* class_finalize */
			NULL,           /* class_data     */
			sizeof (LbModuleRecent),
			0,              /* n_preallocs    */
			(GInstanceInitFunc) lb_module_recent_init,
		};

		type = g_type_register_static (LB_TYPE_MODULE,
					       "LbModuleRecent",
					       &info, 0);
	}

	return type;
}

static void
lb_module_recent_class_init (LbModuleRecentClass *klass)
{
	GObjectClass  *object_class = G_OBJECT_CLASS (klass);
	LbModuleClass *module_class = LB_MODULE_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = lb_module_recent_finalize;
	module_class->query        = lb_module_recent_query;
}

static void
lb_module_recent_changed_cb (GtkRecentManager *manager,
			     gpointer data)
{
	LbModuleRecent *recent = (LbModuleRecent *) data;
	recent->changed = TRUE;
}

static void
lb_module_recent_init (LbModuleRecent *recent)
{
	recent->manager = gtk_recent_manager_get_default ();
	g_signal_connect (G_OBJECT (recent->manager), "changed",
			  G_CALLBACK (lb_module_recent_changed_cb),
			  recent);
}

static void
lb_module_recent_finalize (GObject *object)
{
	LbModuleRecent *recent = LB_MODULE_RECENT (object);

	if (recent->recent_items) {
		g_list_foreach (recent->recent_items,
				(GFunc) gtk_recent_info_unref, NULL);
		g_list_free (recent->recent_items);
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static GList *
lb_module_recent_query (LbModule    *module,
			const gchar *match)
{
	LbModuleRecent *recent = LB_MODULE_RECENT (module);
	GList          *list;
	GList          *result = NULL;

	if (recent->recent_items && recent->changed) {
		g_list_foreach (recent->recent_items,
				(GFunc) gtk_recent_info_unref, NULL);
		g_list_free (recent->recent_items);
		recent->recent_items = NULL;
		recent->changed      = FALSE;
	}

	if (!recent->recent_items) {
		recent->recent_items = gtk_recent_manager_get_items (recent->manager);
	}

	for (list = recent->recent_items; list; list = list->next) {
		GtkRecentInfo *item = list->data;
		const gchar   *uri;
		gchar         *basename;
		gchar         *filename;
		GError        *error = NULL;

		uri      = gtk_recent_info_get_uri (item);
		filename = g_filename_from_uri (uri, NULL, &error);
		if(error && error->code) {
			g_free (filename);
			continue;
		}

		basename = g_path_get_basename (filename);

		if (g_file_test (filename, G_FILE_TEST_EXISTS) &&
		    lb_string_has_substring (basename, match)) {
			LbItem      *new;
			const gchar *mime_type;
			gchar       *icon_name;
			GString     *string;
			gsize        len, i;

			mime_type = gtk_recent_info_get_mime_type (item);
			icon_name = lb_get_icon_name_for_mime_type (mime_type);

			string = g_string_new("");
			for(i = 0, len = strlen(basename); i < len; i++) {
				switch(basename[i]) {
				case '&':
				case '<':
				case '>':
					g_string_append_printf(string, "&#%hhi;", basename[i]);
					break;
				default:
					g_string_append_c(string, basename[i]);
				}
			}

			new = g_object_new (LB_TYPE_ITEM_FILE,
					    "name",      string->str,
					    "uri",       uri,
					    "mime-type", mime_type,
					    "icon-name", icon_name,
					    NULL);

			g_free (icon_name);

			result = g_list_prepend (result, new);

			g_string_free(string, TRUE);
		}

		g_free (basename);
		g_free (filename);
	}

	return g_list_reverse (result);
}

