/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-action.h
 */

#ifndef __LB_ACTION_H__
#define __LB_ACTION_H__

#include "lb-object.h"

#define LB_TYPE_ACTION            (lb_action_get_type ())
#define LB_ACTION(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_ACTION, LbAction))
#define LB_ACTION_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_ACTION, LbActionClass))
#define LB_IS_ACTION(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_ACTION))
#define LB_IS_ACTION_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_ACTION))
#define LB_ACTION_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_ACTION, LbActionClass))

typedef struct _LbAction      LbAction;
typedef struct _LbActionClass LbActionClass;

#include "lb-item.h"

struct _LbAction {
	LbObject  parent_instance;
};

struct _LbActionClass {
	LbObjectClass  parent_class;

	void (* activate) (LbAction *action);
};

/* These are just for a stock way of creating common actions.
 * If you require something else, just create the action with g_object_new
 */
typedef enum {
	LB_ACTION_LAUNCH,
	LB_ACTION_OPEN
} LbActionType;

GType            lb_action_get_type (void) G_GNUC_CONST;
void             lb_action_activate (LbAction       *action);

LbItem *         lb_action_get_item (LbAction       *action);

void             lb_action_set_data (LbAction       *action,
				     gpointer        data,
				     GDestroyNotify  data_destroy_func);
gconstpointer    lb_action_get_data (LbAction       *action);

/* Convinience API for common actions */
LbAction *       lb_action_new      (LbActionType    type);

#endif /* __LB_ACTION_H__ */
