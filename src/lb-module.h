/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-module.h
 */

#ifndef __LB_MODULE_H__
#define __LB_MODULE_H__


#include "lb-object.h"
#include "lb-item.h"

#define LB_TYPE_MODULE            (lb_module_get_type ())
#define LB_MODULE(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_MODULE, LbModule))
#define LB_MODULE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_MODULE, LbModuleClass))
#define LB_IS_MODULE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_MODULE))
#define LB_IS_MODULE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_MODULE))
#define LB_MODULE_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_MODULE, LbModuleClass))

#define LB_MODULE_SLUGS_SUPPORTED(mod) \
	(lb_module_get_flags (LB_MODULE ((mod))) & \
	 LB_MODULE_FLAGS_SLUGS_SUPPORTED)

typedef struct _LbModule      LbModule;
typedef struct _LbModuleClass LbModuleClass;

typedef enum {
	LB_MODULE_FLAGS_SLUGS_SUPPORTED = (1 << 0),
	LB_MODULE_DEFAULT_FLAGS = LB_MODULE_FLAGS_SLUGS_SUPPORTED
} LbModuleFlags;

struct _LbModule {
	LbObject  parent_instance;

	gboolean  enabled;
};

struct _LbModuleClass {
	LbObjectClass  parent_class;

	LbModuleFlags (* get_flags)   (LbModule    *module);
	GList *       (* query)       (LbModule    *module,
				       const gchar *match);
	void          (* add_actions) (LbModule    *module,
				       LbItem      *item);
};


GType         lb_module_get_type    (void) G_GNUC_CONST;

LbModuleFlags lb_module_get_flags   (LbModule    *module);
GList *       lb_module_query       (LbModule    *module,
				     const gchar *match);
void          lb_module_add_actions (LbModule    *module,
				     LbItem      *item);


#endif /* __LB_MODULE_H__ */
