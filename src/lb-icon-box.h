/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-icon-box.h
 */

#ifndef __LB_ICON_BOX_H__
#define __LB_ICON_BOX_H__


#include "lb-frame.h"

#define LB_TYPE_ICON_BOX            (lb_icon_box_get_type ())
#define LB_ICON_BOX(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), LB_TYPE_ICON_BOX, LbIconBox))
#define LB_ICON_BOX_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), LB_TYPE_ICON_BOX, LbIconBoxClass))
#define LB_IS_ICON_BOX(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), LB_TYPE_ICON_BOX))
#define LB_IS_ICON_BOX_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), LB_TYPE_ICON_BOX))
#define LB_ICON_BOX_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), LB_TYPE_ICON_BOX, LbIconBoxClass))


typedef struct _LbIconBox      LbIconBox;
typedef struct _LbIconBoxClass LbIconBoxClass;

struct _LbIconBox
{
	LbFrame parent_instance;
};

struct _LbIconBoxClass
{
	LbFrameClass parent_class;
};


GType      lb_icon_box_get_type         (void) G_GNUC_CONST;
GtkWidget *lb_icon_box_new              (const gchar *caption,
			                 GdkPixbuf   *pixbuf);

void        lb_icon_box_set_focused     (LbIconBox    *box,
				         gboolean      focused);
void        lb_icon_box_set_transparent (LbIconBox    *box,
					 gboolean      transparent);
#endif /* __LB_ICON_BOX_H__ */
