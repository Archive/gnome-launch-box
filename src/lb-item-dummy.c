/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-item-dummy.c
 */

#include "config.h"

#include <string.h>

#include <gdk-pixbuf/gdk-pixbuf.h>

#include "lb-item-dummy.h"

#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_ITEM_DUMMY, LbItemDummyPriv))

typedef struct _LbItemDummyPriv LbItemDummyPriv;
struct _LbItemDummyPriv {
	gint placeholder; /* Remove this, can't have empty struct */
};

#if 0
enum {
	PROP_0,
	PROP_NAME,
};
#endif


static void    item_dummy_finalize     (GObject            *object);
static void    item_dummy_set_property (GObject            *object,
					guint               property_id,
					const GValue       *value,
					GParamSpec         *pspec);
static void    item_dummy_get_property (GObject            *object,
					guint               property_id,
					GValue             *value,
					GParamSpec         *pspec);


G_DEFINE_TYPE (LbItemDummy, lb_item_dummy, LB_TYPE_ITEM);
static LbItemClass *parent_class = NULL;

static void
lb_item_dummy_class_init (LbItemDummyClass *klass)
{
	GObjectClass  *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = item_dummy_finalize;
	object_class->set_property = item_dummy_set_property;
	object_class->get_property = item_dummy_get_property;

#if 0
	g_object_class_install_property (object_class, PROP_NAME,
					 g_param_spec_string ("name",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

#endif

	g_type_class_add_private (object_class,
				  sizeof (LbItemDummyPriv));

}

static void
lb_item_dummy_init (LbItemDummy *dummy)
{
	LbItemDummyPriv *priv;

	priv = GET_PRIV (dummy);
}

static void
item_dummy_finalize (GObject *object)
{
	LbItemDummyPriv *priv;

	priv = GET_PRIV (object);

#if 0
	if (dummy->name) {
		g_free (dummy->name);
		dummy->name = NULL;
	}
#endif

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
item_dummy_set_property (GObject      *object,
			      guint         property_id,
			      const GValue *value,
			      GParamSpec   *pspec)
{
	LbItemDummyPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
#if 0
	case PROP_NAME:
		if (dummy->name)
			g_free (dummy->name);
		dummy->name = g_value_dup_string (value);
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
item_dummy_get_property (GObject    *object,
			 guint       property_id,
			 GValue     *value,
			 GParamSpec *pspec)
{
	LbItemDummyPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
#if 0
	case PROP_NAME:
		g_value_set_string (value, dummy->name);
		break;
#endif
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

#if 0
const gchar *
lb_item_dummy_get_name (LbItemDummy *item)
{
	LbItemDummyPriv *priv;
	
	g_return_val_if_fail (LB_IS_ITEM_DUMMY (item), NULL);

	priv = GET_PRIV (item);

	return priv->name;
}

void
lb_item_dummy_set_name (LbItemDummy *item, const gchar *name)
{
	LbItemDummyPriv *priv;

	g_return_if_fail (LB_IS_ITEM_DUMMY (item));
	g_return_if_fail (name != NULL);

	priv = GET_PRIV (item);
	
	g_free (priv->name);
	priv->name = g_strdup (name);
	
	g_object_notify (G_OBJECT (item), "name");
}
#endif
