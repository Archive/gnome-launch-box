/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2005 Imendio AB
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/*
 *  lb-item-contact.c
 */

#include "config.h"

#include "lb-item-contact.h"

#define GET_PRIV(obj) (G_TYPE_INSTANCE_GET_PRIVATE ((obj), LB_TYPE_ITEM_CONTACT, LbItemContactPriv))

typedef struct _LbItemContactPriv LbItemContactPriv;
struct _LbItemContactPriv {
	gchar *email;
};

enum {
	PROP_0,
	PROP_EMAIL
};

static void    item_contact_finalize     (GObject            *object);
static void    item_contact_set_property (GObject            *object,
					  guint               property_id,
					  const GValue       *value,
					  GParamSpec         *pspec);
static void    item_contact_get_property (GObject            *object,
					  guint               property_id,
					  GValue             *value,
					  GParamSpec         *pspec);


G_DEFINE_TYPE (LbItemContact, lb_item_contact, LB_TYPE_ITEM);
static LbItemClass *parent_class = NULL;

static void
lb_item_contact_class_init (LbItemContactClass *klass)
{
	GObjectClass  *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize     = item_contact_finalize;
	object_class->set_property = item_contact_set_property;
	object_class->get_property = item_contact_get_property;

	g_object_class_install_property (object_class, PROP_EMAIL,
					 g_param_spec_string ("email",
							      NULL, NULL,
							      "",
							      G_PARAM_READWRITE |
							      G_PARAM_CONSTRUCT));

	g_type_class_add_private (object_class,
				  sizeof (LbItemContactPriv));

}

static void
lb_item_contact_init (LbItemContact *contact)
{
	LbItemContactPriv *priv;

	priv = GET_PRIV (contact);

	priv->email = NULL;
}

static void
item_contact_finalize (GObject *object)
{
	LbItemContactPriv *priv;

	priv = GET_PRIV (object);

	if (priv->email) {
		g_free (priv->email);
		priv->email = NULL;
	}

	G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
item_contact_set_property (GObject      *object,
			   guint         property_id,
			   const GValue *value,
			   GParamSpec   *pspec)
{
	LbItemContactPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
	case PROP_EMAIL:
		if (priv->email) {
			g_free (priv->email);
		}
		priv->email = g_value_dup_string (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

static void
item_contact_get_property (GObject    *object,
			   guint       property_id,
			   GValue     *value,
			   GParamSpec *pspec)
{
	LbItemContactPriv *priv;

	priv = GET_PRIV (object);

	switch (property_id) {
	case PROP_EMAIL:
		g_value_set_string (value, priv->email);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
		break;
	}
}

const gchar *
lb_item_contact_get_email (LbItemContact *item)
{
	LbItemContactPriv *priv;
	
	g_return_val_if_fail (LB_IS_ITEM_CONTACT (item), NULL);

	priv = GET_PRIV (item);

	return priv->email;
}

void
lb_item_contact_set_email (LbItemContact *item, const gchar *email)
{
	LbItemContactPriv *priv;

	g_return_if_fail (LB_IS_ITEM_CONTACT (item));
	g_return_if_fail (email != NULL);

	priv = GET_PRIV (item);
	
	g_free (priv->email);
	priv->email = g_strdup (email);
        
	g_object_notify (G_OBJECT (item), "email");
}
